import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.InputMismatchException;
import java.util.Scanner;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Metodos {

	Scanner t = new Scanner(System.in);

	public void anyadir() {

		Document document = cargarXML();

		Element listaJuegos = document.getDocumentElement();

		// Titulo del Juego
		String textTitulo;
		System.out.println("Introduce un juego:");
		textTitulo = t.nextLine();

		// Genero del Juego
		String textGenero;
		System.out.println("Introduce el genero del juego:");
		textGenero = t.nextLine();

		// Platoforma del Juego
		String textPlataforma;
		System.out.println("Introduce la plataforma del juego:");
		textPlataforma = t.nextLine();

		// Fecha de Lanzamiento del Juego
		String textFechaLanzamiento;
		System.out.println("Introduce la fecha de lanzamiento del juego:");
		textFechaLanzamiento = t.nextLine();

		Element juego = document.createElement("juego");

		Element titulo = document.createElement("titulo");
		titulo.setTextContent(textTitulo);

		Element genero = document.createElement("genero");
		genero.setTextContent(textGenero);

		Element plataforma = document.createElement("plataforma");
		plataforma.setTextContent(textPlataforma);

		Element fechadelanzamiento = document.createElement("fechadelanzamiento");
		fechadelanzamiento.setTextContent(textFechaLanzamiento);

		listaJuegos.appendChild(juego);

		juego.appendChild(titulo);
		juego.appendChild(genero);
		juego.appendChild(plataforma);
		juego.appendChild(fechadelanzamiento);

		cargarDatosFichero(document);
	}

	public void eliminar() {

		// 1. cargar el XML original
		Document document = cargarXML();

		// 2. Obtener el nodo ra�z del objeto de documento a trav�s del objeto de
		// documento

		Element root = document.getDocumentElement();

		// 3. Encuentra el nodo especificado
		// Obtener nodos secundarios a trav�s del nodo ra�z
		NodeList juegoList = root.getElementsByTagName("juego");
		// 4. Obtenga el segundo nodo aqu� por teclado
		System.out.println("Que juego quieres eliminar? (Introduce la posicion)");

		try {

			int juegoPos;
			juegoPos = t.nextInt();
			Node item = juegoList.item(juegoPos);
			if (item != null) {
				// eliminar el nodo
				root.removeChild(item);
				System.out.println("Se ha borrado correctamente el juego");
			} else
				System.out.println("El juego no existe");

		} catch (InputMismatchException ex) {
			System.out.println("Has introducido un String");
		}

		// 3. Exportar nuevamente el XML
		cargarDatosFichero(document);

	}

	public void listar() {
		Document document = cargarXML();
		NodeList listaJuegos = document.getElementsByTagName("listadejuegos").item(0).getChildNodes();
		for (int i = 0; i < listaJuegos.getLength(); i++) {
			Node node = listaJuegos.item(i);
			if (node.getNodeType() == Node.ELEMENT_NODE) { // Los nodos pueden ser de diferentes tipos (como atributo,
															// elemento...) con esto controlo que el nodo que me saca
															// sea un elemento
				Element elemento = (Element) node; // obtener los elementos del nodo

				// imprime elementos
				System.out.println("JUEGO " + "\n");
				System.out.println("Titulo: " + getNodo("titulo", elemento));
				System.out.println("Genero: " + getNodo("genero", elemento));
				System.out.println("Plataforma: " + getNodo("plataforma", elemento));
				System.out.println("Fecha de lanzamiento: " + getNodo("fechadelanzamiento", elemento) + "\n");

			}

		}
	}

	public void salir() {
		System.out.println("* * *Programa Cerrado* * *");
		System.exit(0);
	}

	private Document cargarXML() {
		Path path = Path.of("src/juegos.xml");
		File xml = path.toFile();
		/* PRIMEROS PASOS DE CONFIGURACION */
		DocumentBuilder builder = createBuilder();
		Document document = null;
		try {
			document = builder.parse(xml);
		} catch (IOException | SAXException ex) {
			System.err.println("Error al crear el Document");
			System.err.println(ex.getMessage());
			System.exit(-2);
		}
		return document;

	}

	private DocumentBuilder createBuilder() {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		try {
			builder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException ex) {
			System.err.println("Error al crear el DocumentBuilder");
			System.err.println(ex.getMessage());
			System.exit(-1);
		}
		return builder;
	}

	private String getNodo(String etiqueta, Element elem) {
		NodeList nodo = elem.getElementsByTagName(etiqueta).item(0).getChildNodes();
		Node valorNodo = (Node) nodo.item(0);
		return valorNodo.getNodeValue();// devuelve el valor del nodo
	}

	private void cargarDatosFichero(Document document) {
		Source origen = new DOMSource(document);
		Result result = new StreamResult(new File("src/juegos.xml"));
		Transformer transformer = null;
		try {
			transformer = TransformerFactory.newInstance().newTransformer();
		} catch (TransformerConfigurationException ex) {
			System.err.println("Error al crear el Transfomer");
			System.out.println(ex.getMessage());
			System.exit(-3);
		}

		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");

		try {
			transformer.transform(origen, result); // transforma los datos: de la RAM al fichero
		} catch (TransformerException ex) {
			System.err.println("Error al transformar el origen en el destino");
			System.out.println(ex.getMessage());
			System.exit(-3);
		}
	}

}
