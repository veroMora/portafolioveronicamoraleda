import java.util.Scanner;

public class Main {
	
	public static Scanner t;
	public static Metodos m;
	
	public static void main(String[] args) {
		t = new Scanner(System.in);
		
		System.out.println("* * *Inicio del Programa* * *");

		m = new Metodos();

		String accion;

		do {
			menu();
			accion = t.next().toLowerCase();
			elegirAccion(accion);
		} while (accion != "salir");


	}
	
	private static void elegirAccion(String accion) {

		switch (accion) {
		case "añadir":
			m.anyadir();
			break;
		case "eliminar":
			m.eliminar();
			break;
		case "listar":
			m.listar();
			break;
		case "salir":
			m.salir();
			break;
		default:
			System.out.println("No existe la accion introducida");
		}
	}
	
	private static void menu() {
		System.out.println("Elige entre estas acciones: \n"); 
		System.out.println("-Añadir \n"); 
		System.out.println("-Eliminar \n"); 
		System.out.println("-Listar \n"); 
		System.out.println("-Salir \n"); 

	}

}
