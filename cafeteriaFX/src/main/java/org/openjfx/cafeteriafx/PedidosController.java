package org.openjfx.cafeteriafx;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import modelo.Cafes;

public class PedidosController implements Initializable {
    double dineroTotal = 0;
    
    @FXML
    private AnchorPane anchorPane;
    
    @FXML
    private Button btEliminar;

    @FXML
    private Button btModificar;

    @FXML
    private Button btPedir;

    @FXML
    private Button btRecargar;

    @FXML
    private ComboBox cbTamanio;

    @FXML
    private Label labelSaldo;
     
    @FXML
    private TableColumn colCantidad;

    @FXML
    private TableColumn colPrecio;

    @FXML
    private TableColumn colTamanio;

    @FXML
    private TableColumn colTipo;

    @FXML
    private RadioButton rdbCafeLatte;

    @FXML
    private RadioButton rdbCapuccino;

    @FXML
    private RadioButton rdbCortado;

    @FXML
    private TableView<Cafes> tblPedidos;

    @FXML
    private TextField txtCantidad;
    
    @FXML
    private TextField txtFiltrarPedido;

    @FXML
    private TextField txtSaldo;
    
    @FXML
    private ObservableList<Cafes> cafes;
    
    @FXML
    private ObservableList<Cafes> filtroCafes;
    
    // Función que devuelve el tipo de café
    public String tipoDeCafe() {
        String tipo = null;
            if(rdbCafeLatte.isSelected()) {
                tipo = "Cafe Latte";
            } else if(rdbCapuccino.isSelected()) {
                tipo = "Capuccino";
            } else if(rdbCortado.isSelected()) {
                tipo = "Cortado";
            }   
        return tipo;
    }
    
    // Función que devuelve el tamañio del café
    public String tipoDeTamanio() {
        String tamanio;
        tamanio = cbTamanio.getValue().toString();
        return tamanio;
    }
    
    // Con este action event vamos a añadir los pedidos a la tabla. Además, como tenemos un filtro también se añadirán los pedidos a la tabla en función del filtrado.
    // Todo los errores que se puedan dar con el botón pedir están controlados.
    @FXML
    void agregarPedidos(ActionEvent event) {
        
        try {
            String tipoC = tipoDeCafe();
            String tamanioC = tipoDeTamanio();
        
            int cantidad = Integer.parseInt(txtCantidad.getText());
        
            Cafes c = new Cafes(tipoC,tamanioC,cantidad);
            c.getPrecio();
            
            if((cantidad > 0)) {
                 dineroTotal = Double.parseDouble(labelSaldo.getText());
                 
                 if(dineroTotal >= c.getPrecio()) {
                     String filtroTamTipo = c.getTipo() + " " + c.getTamanio();
                      if(c.getTipo().toLowerCase().contains(txtFiltrarPedido.getText().toLowerCase())) {
                            this.filtroCafes.add(c);
                      } else if(c.getTamanio().toLowerCase().contains(txtFiltrarPedido.getText().toLowerCase())) {
                            this.filtroCafes.add(c);
                      } else if(filtroTamTipo.toLowerCase().contains(txtFiltrarPedido.getText().toLowerCase())){
                            this.filtroCafes.add(c);
                      }
                     this.cafes.add(c);
                     tblPedidos.setItems(cafes);
                     tblPedidos.setItems(filtroCafes);
                     dineroTotal -= c.getPrecio();
                     labelSaldo.setText(dineroTotal + ""); 
                 } else {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error");
                    alert.setContentText("No tienes suficiente dinero");
                    alert.showAndWait();
                 }
                 
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                alert.setContentText("La cifra es menor o igual a 0");
                alert.showAndWait();
            }
           
            
        } catch(NumberFormatException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setContentText("Introduce una cifra");
            alert.showAndWait();
        }   
        
        
    }
    
    // Está controlado para que se pueda eliminar el pedido tanto si está siendo filtrado como si no lo está siendo.
    @FXML
    void eliminarPedidos(ActionEvent event) {
        
        Cafes c = this.tblPedidos.getSelectionModel().getSelectedItem();
        
        if (c == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setContentText("Debes seleccionar un pedido");
            alert.showAndWait();
        } else {
            dineroTotal = Double.parseDouble(labelSaldo.getText()); 
            dineroTotal += c.getPrecio();
            labelSaldo.setText(dineroTotal + ""); 
            
            this.cafes.remove(c);
            this.filtroCafes.remove(c);
            this.tblPedidos.refresh();
            
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Informacion");
            alert.setContentText("Pedido eliminado correctamente");
            alert.showAndWait();
        }
    }
    
    //Filtramos los pedidos por tipo y tamanio individualmente, y también se puede filtrar conjuntamente, por tipo y tamanio (en este orden)
    @FXML
    void filtrarPedidos(KeyEvent event) {
        
        String filtroCafe = this.txtFiltrarPedido.getText();
        
        if(filtroCafe == null || filtroCafe.isEmpty()) {
            this.tblPedidos.setItems(cafes);
        } else {
            this.filtroCafes.clear();
            
            for(Cafes cafe: this.cafes) {
                String filtroTamTipo = cafe.getTipo() + " " + cafe.getTamanio();
                if(cafe.getTipo().toLowerCase().contains(filtroCafe.toLowerCase())) {
                    this.filtroCafes.add(cafe);
                } else if(cafe.getTamanio().toLowerCase().contains(filtroCafe.toLowerCase())) {
                    this.filtroCafes.add(cafe);
                } else if(filtroTamTipo.toLowerCase().contains(filtroCafe.toLowerCase())){
                    this.filtroCafes.add(cafe);
                }
                this.tblPedidos.setItems(filtroCafes);
            }
        }
    }
    
    //Recarga el saldo. Solo se pueden recargar números naturales que son convertidos a decimales. 
    @FXML
    void recargarSaldo(ActionEvent event) {
        try {
            double dinero; 
            dinero = Math.round((Integer.parseInt(txtSaldo.getText()) * 10)) / 10.0;
            dineroTotal += dinero;
            labelSaldo.setText(dineroTotal + ""); 
        } catch(NumberFormatException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setContentText("Mete bien el dinero!");
            alert.showAndWait();
        } 
    }
    
    //Controla que mientras no se seleccione ningún radio button no cambia el método setDisable() que está inicializado como true para que no se pueda 
    //escribir en el text field asignado para cantidad ni el botón asignado para pedir. Con esto evitamos que haya error por los radio button al pedir.
    @FXML
    void seleccionarCafe(ActionEvent event) {
        if (rdbCafeLatte.isSelected() || rdbCapuccino.isSelected() || rdbCortado.isSelected()) {
            txtCantidad.setDisable(false);
            btPedir.setDisable(false);
        }
    }

    // Al inicializar el proyecto se controlan los radio button para que solo se pueda elegir uno, se añaden los valores al combo box y se predetermina el valor,
    // se adjuntan las observable list, se establece que variable adquiere cada columan de la tabla y se convierten en disable el botón pedir y el texto de cantidad.
    @Override
    public void initialize(URL location, ResourceBundle resources) {
       ToggleGroup tg = new ToggleGroup();
       this.rdbCafeLatte.setToggleGroup(tg);
       this.rdbCapuccino.setToggleGroup(tg);
       this.rdbCortado.setToggleGroup(tg);
        
       cbTamanio.getItems().addAll("Grande", "Mediano", "Pequeño");
       cbTamanio.getSelectionModel().select("Pequeño");
       
       cafes = FXCollections.observableArrayList();
       filtroCafes = FXCollections.observableArrayList();
       
       colTipo.setCellValueFactory(new PropertyValueFactory("tipo"));
       colTamanio.setCellValueFactory(new PropertyValueFactory("tamanio"));
       colCantidad.setCellValueFactory(new PropertyValueFactory("cantidad"));
       colPrecio.setCellValueFactory(new PropertyValueFactory("precio"));
       
       
       txtCantidad.setDisable(true);
       btPedir.setDisable(true);
   
    }

    
    

}

