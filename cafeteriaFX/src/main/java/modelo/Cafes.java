package modelo;

public class Cafes {
    
    private String tipo;
    private String tamanio;
    private int cantidad;
    private double precio;  

    public Cafes(String tipo, String tamanio, int cantidad, double precio) {
        this.tipo = tipo;
        this.tamanio = tamanio;
        this.cantidad = cantidad;
        this.precio = precio;
    }

    public Cafes(String tipo, String tamanio, int cantidad) {
        this.tipo = tipo;
        this.tamanio = tamanio;
        this.cantidad = cantidad;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTamanio() {
        return tamanio;
    }

    public void setTamanio(String tamanio) {
        this.tamanio = tamanio;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getPrecio() {
        setPrecio(cantidad, tamanio);
        return precio;
    }
    
    public void setPrecio(int cantidad, String tamanio) {
        double precioTamanio;
         if (tamanio.equalsIgnoreCase("Grande")) {
            precioTamanio = 2;
        } else if (tamanio.equalsIgnoreCase("Mediano")) {
             precioTamanio = 1.5;
        } else precioTamanio = 1;
         
        this.precio = precioTamanio * cantidad;
    }
    
    
    
}
