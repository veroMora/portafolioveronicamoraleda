package Casino;

import java.util.ArrayList;

public class Casino {

	static int banca = 3000;
	
	public static int getBanca() {
		return Casino.banca;
	}

	public static void setBanca(int banca) {
		Casino.banca = banca;
	}

	public static void main(String[] args) throws InterruptedException {
		
		ArrayList<Jugadores> jugador = new ArrayList<Jugadores>();
		
		Jugadores j1 = new Jugadores("Vero");
		Jugadores j2 = new Jugadores("Juan");
		Jugadores j3 = new Jugadores("Jesus");
		Jugadores j4 = new Jugadores("Nines");
		
		jugador.add(j1);
		jugador.add(j2);
		jugador.add(j3);
		jugador.add(j4);
		
		
		
		do {
			System.out.println("HAGAN SUS APUESTAS!...\n");
			Ruleta r = new Ruleta(jugador);
			Thread.sleep(1000);
			r.start();
			r.join();
			System.out.println("\n* * *La bola ha caido en el numero: " + r.getNumAzar() + "* * *");
			System.out.println("\n* * *BANCA: " + getBanca() + "€* * *\n");
			Thread.sleep(5000);
		} while (jugador.size() != 0 && banca > 0);
		System.out.println("Ya no hay jugadores o la banca se ha quedado en numeros rojos");
	}
	
	protected int actualizarBanca(ArrayList<Jugadores> jugador, int pos, int dineroBanca, int numAzar){
		
		if(jugador.get(pos).getApuestaNumero() == numAzar) {
			dineroBanca -= 360;
		} else {
			dineroBanca += 10;
		}
		return dineroBanca;
	}

}
