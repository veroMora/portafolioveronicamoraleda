package Casino;

public class Jugadores {
	private String nombre;
	private int dinero = 300;
	private int apuestaDinero = 10;
	private int apuestaNumero;
	
	public Jugadores(String nombre, int dinero, int apuestaDinero, int apuestaNumero) {
		this.nombre = nombre;
		this.dinero = dinero;
		this.apuestaDinero = apuestaDinero;
		this.apuestaNumero = apuestaNumero;
	}
	
	public Jugadores(String nombre) {
		this.nombre = nombre;
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getDinero() {
		return dinero;
	}
	public void setDinero(int dinero) {
		this.dinero = dinero;
	}
	public int getApuestaDinero() {
		return apuestaDinero;
	}
	public void setApuestaDinero(int apuestaDinero) {
		this.apuestaDinero = apuestaDinero;
	}
	public int getApuestaNumero() {
		return apuestaNumero;
	}
	public void setApuestaNumero(int apuestaNumero) {
		this.apuestaNumero = apuestaNumero;
	}
	
	public void saldoActual(int numRuleta) {
		if(this.apuestaNumero == numRuleta) {
			System.out.println(nombre + " ha acertado el numero " + apuestaNumero + ". Su saldo actual es de: " + (dinero += 360) + "€");
		} else {
			System.out.println(nombre + " ha apostado al numero " + apuestaNumero + ". Fallaste. Su saldo actual es de: " + (dinero -= apuestaDinero) + "€");
		}
	}
	
	
	
}
