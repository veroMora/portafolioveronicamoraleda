package Casino;

import java.util.ArrayList;

public class Ruleta extends Thread {
	ArrayList<Jugadores> jugador;
	Casino c = new Casino();

	private int numAzar = (int) (Math.random() * 36 + 1);

	public Ruleta(ArrayList<Jugadores> jugador) {
		this.jugador = jugador;

	}

	public Ruleta() {

	}

	public int getNumAzar() {
		return numAzar;
	}

	public void setNumAzar(int numAzar) {
		this.numAzar = numAzar;
	}

	@Override
	public void run() {

		for (int i = 0; i < jugador.size(); i++) {
			jugador.get(i).setApuestaNumero((int) (Math.random() * 36 + 1));
			jugador.get(i).saldoActual(this.numAzar);
			int actualizarBanca = c.actualizarBanca(jugador, i, Casino.banca, this.numAzar);
			Casino.banca = actualizarBanca;
			if (jugador.get(i).getDinero() <= 0 && !jugador.isEmpty()) {
				jugador.remove(i);
				i--;
			}
		}

	}

}
