package org.openjfx.pokemon_mongodb;

import com.mongodb.client.MongoDatabase;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Pokemon_MongoDB {

    private static Metodos m;
    private static MongoDatabase database;
    private static String campo;
    private static String valor;
    private static String nombrePokemon;
    private static Scanner t;
    private static BufferedReader in;

    public static void main(String[] args) throws IOException {

        t = new Scanner(System.in);
        in = new BufferedReader(new InputStreamReader(System.in));

        Conexion conexion = new Conexion();

        String url = conexion.getUrl();
        String nombreDB = conexion.getNombreDB();

        database = conexion.connect(url, nombreDB);

        m = new Metodos();

        String resp;
        do {
            int op = 0;
            try {
                menu(op);
            } catch (InputMismatchException ex) {
                System.err.println("Error porque se ha introducido una cadena de caracteres o un double\n");
                t.next();
            }
            System.out.println("Quieres seguir realizando operaciones? S/N");
            resp = t.next();
        } while (!resp.equalsIgnoreCase("N"));

        conexion.disconnect();
    }

    private static void menu(int op) throws IOException {
        System.out.println("Que operacion quieres realizar?\n 1. Buscar un pokemon por el campo que desees\n "
                + "2. Eliminar varios pokemon en funcion del campo que desees\n 3. Insertar un pokemon con un solo campo (name)\n "
                + "4. Actualizar un pokemon en función del campo que desees\n 5. Salir");
        op = t.nextInt();

        switch (op) {
            case 1:
                System.out.println("Introduce el campo por el que quieres filtrar");
                campo = t.next();
                System.out.println("Introduce un valor determinado del campo anterior");
                valor = in.readLine();
                m.encontrar(database, "pokemon", campo, valor);
                break;
            case 2:
                System.out.println("Introduce el campo por el que quieres eliminar");
                campo = t.next();
                System.out.println("Introduce un valor determinado del campo anterior");
                valor = in.readLine();
                m.eliminar(database, "pokemon", campo, valor);
                break;
            case 3:
                System.out.println("Introduce el nombre del pokemon que quieras insertar");
                valor = t.next();
                m.insertar(database, "pokemon", "name", valor);
                break;
            case 4:
                System.out.println("Introduce el campo por el que quieres actualizar");
                campo = t.next();
                System.out.println("Introduce un valor determinado del campo anterior");
                valor = in.readLine();
                System.out.println("Introduce un nuevo nombre al pokemon para actualizar este campo");
                nombrePokemon = t.next();
                m.actualizar(database, "pokemon", campo, valor, nombrePokemon);
                break;
            case 5:
                System.out.println("* * * NO HAS REALIZADO NINGUNA OPERACION * * *\n");
                break;
            default:
                System.err.println("* * * NO EXISTE LA OPERACION INTRODUCIDA * * *\n");
        }
    }
}
