package org.openjfx.pokemon_mongodb;

import com.mongodb.MongoException;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Projections;
import com.mongodb.client.model.Sorts;
import com.mongodb.client.model.Updates;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import java.util.Scanner;
import org.bson.Document;
import org.bson.conversions.Bson;

public class Metodos {

    private Scanner t = new Scanner(System.in);

    public void encontrar(MongoDatabase database, String nombreC, String campo, String valor) {

        System.out.println("Quieres parsear el valor a int? S/N");
        String resp = t.next();

        int valorInt = 0;
        try {
            if (!resp.equalsIgnoreCase("N")) {
                valorInt = Integer.parseInt(valor);
            }
        } catch (NumberFormatException ex) {
            System.err.println("* * * No se ha podido parsear el valor * * *\n");
        }

        MongoCollection<Document> collection = database.getCollection(nombreC);

        Bson projectionFields = Projections.fields(
                Projections.include("pokedex_number", "name", "abilities", 
                        "is_legendary", "generation", "classfication"),
                Projections.excludeId());

        Document findDoc;
        if (valorInt == 0) {
            findDoc = new Document(campo, valor);
        } else {
            findDoc = new Document(campo, valorInt);
        }

        MongoCursor<Document> docs = (MongoCursor<Document>) collection.find(findDoc).projection(projectionFields)
                .sort(Sorts.descending("pokedex_number")).iterator();

        if (docs.hasNext() == false) {
            System.err.println("* * * No se han encontrado pokemon * * *\n");
        } else {
            System.out.println("* * * LISTA DE POKEMON * * *\n");
            while (docs.hasNext()) {
                System.out.println(docs.next().toJson() + "\n");
            }
        }

    }

    public void eliminar(MongoDatabase database, String nombreC, String campo, String valor) {

        System.out.println("Quieres parsear el valor a int? S/N");
        String resp = t.next();

        int valorInt = 0;
        try {
            if (!resp.equalsIgnoreCase("N")) {
                valorInt = Integer.parseInt(valor);
            }
        } catch (NumberFormatException ex) {
            System.err.println("* * * No se ha podido parsear el valor * * *\n");
        }

        MongoCollection<Document> collection = database.getCollection(nombreC);

        Document delDoc;
        if (valorInt == 0) {
            delDoc = new Document(campo, valor);
        } else {
            delDoc = new Document(campo, valorInt);
        }

        try {
            DeleteResult result = collection.deleteMany(delDoc);
            System.out.println("Numero de documentos borrados: " + result.getDeletedCount() + "\n");
        } catch (MongoException ex) {
            System.err.println("* * * No se ha podido borrar ningún pokemon debido a un error * * *\n");
        }

    }

    public void insertar(MongoDatabase database, String nombreC, String campo, String valor) {

        MongoCollection<Document> collection = database.getCollection(nombreC);

        try {
            collection.insertOne(new Document().append(campo, valor));
            System.out.println("* * * Exito al insertar el pokemon * * *\n");
        } catch (MongoException me) {
            System.err.println("* * * Error al insertar el pokemon * * *\n");
        }
    }

    public void actualizar(MongoDatabase database, String nombreC, String campo, String valor, String vPokemon) {

        System.out.println("Quieres parsear el valor a int? S/N");
        String resp = t.next();

        int valorInt = 0;
        try {
            if (!resp.equalsIgnoreCase("N")) {
                valorInt = Integer.parseInt(valor);
            }
        } catch (NumberFormatException ex) {
            System.err.println("* * * No se ha podido parsear el valor * * *\n");
        }

        MongoCollection<Document> collection = database.getCollection(nombreC);

        Document updateDoc;
        if (valorInt == 0) {
            updateDoc = new Document().append(campo, valor);
        } else {
            updateDoc = new Document().append(campo, valorInt);
        }

        Bson updates = Updates.set("name", vPokemon);

        try {
            UpdateResult result = collection.updateOne(updateDoc, updates);
            System.out.println("Numero de documentos modificados: " + result.getModifiedCount() + "\n");

        } catch (MongoException me) {
            System.err.println("* * * Error al actualizar el pokemon * * *\n");
        }
    }
}
