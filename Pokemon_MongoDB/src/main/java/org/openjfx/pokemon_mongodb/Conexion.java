package org.openjfx.pokemon_mongodb;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;

public class Conexion {
    
    private String url = "mongodb+srv://veroMora:veroMora@cluster0.igab9ax.mongodb.net/?retryWrites=true&w=majority";
    private String nombreDB = "pokedex";
    private MongoClient client;

    
    public MongoDatabase connect(String url, String nombreDB) {
        
        client = MongoClients.create(url);
        MongoDatabase database = client.getDatabase(nombreDB);
        System.out.println("* * * CONEXION ESTABLECIDA * * *\n");
        return database;
    }
    
    public void disconnect() {
        client.close();
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getNombreDB() {
        return nombreDB;
    }

    public void setNombreDB(String nombreDB) {
        this.nombreDB = nombreDB;
    }

    public MongoClient getClient() {
        return client;
    }

    public void setClient(MongoClient client) {
        this.client = client;
    }
    
}
