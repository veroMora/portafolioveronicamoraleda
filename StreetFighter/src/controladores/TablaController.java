/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package controladores;

import static controladores.PeleaController.batalla;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import modelo.Batallas;

/**
 * FXML Controller class
 *
 * @author verom
 */
public class TablaController implements Initializable {
    
    @FXML
    private Button btSalirTabla;
    
    @FXML
    private TableColumn colBatalla;
    
    @FXML
    private TableColumn colGanador;
    
    @FXML
    private TableView<Batallas> tblBatallas;
    
    @FXML
    private ObservableList<Batallas> batallas;
    
    @FXML
    void salirJuego(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setHeaderText(null);
        alert.setTitle("Confirmacion");
        alert.setContentText("¿Estas seguro/a de que quieres salir?");
        alert.showAndWait();

        if (alert.getResult().equals(ButtonType.OK)) {
            Platform.exit();
        }
    }
    
    @FXML
    void volverGanador(MouseEvent event) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/vistas/ganador.fxml"));
            Parent root = loader.load();

            PeleaController.stageGanador = (Stage) ((Node) event.getSource()).getScene().getWindow();
            Scene scene = new Scene(root);
            PeleaController.stageGanador.setScene(scene);

            PeleaController.stageGanador.show();

        } catch (IOException ex) {
            Logger.getLogger(streetFighterController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        batallas = FXCollections.observableArrayList();
        
        this.colBatalla.setCellValueFactory(new PropertyValueFactory("batalla"));
        this.colGanador.setCellValueFactory(new PropertyValueFactory("ganador"));
        
        batallas.addAll(PeleaController.batalla);
        tblBatallas.setItems(batallas);
        
        
        
    }    
    
}
