/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package controladores;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 *
 * @author Mañana_pos1
 */
public class EleccionMapasController implements Initializable {

    @FXML
    private AnchorPane anchorPane;

    @FXML
    private ImageView flechaLeft;

    @FXML
    private ImageView flechaRight;

    @FXML
    private ImageView imgVS;
    
    @FXML
    private ImageView imgSelect;
    
    @FXML
    private ImageView imgLuchar;

    @FXML
    public ImageView imgMapa;

    @FXML
    private ImageView pAzul;

    @FXML
    private ImageView pRojo;
    
    @FXML
    public static Stage stagePelea;

    public static int numImg;
    public static String desplazamiento;
  
    public static String personajeAzul;
    public static String personajeRojo;
    
    
     @FXML
    void empezarPelea(MouseEvent event) {
        try {
            FXMLLoader loaderM = new FXMLLoader(getClass().getResource("/vistas/pelea.fxml"));
            Parent root = loaderM.load();

            stagePelea = (Stage) ((Node) event.getSource()).getScene().getWindow();
            Scene scene = new Scene(root);
            stagePelea.setScene(scene);

            stagePelea.show();

        } catch (IOException ex) {
            Logger.getLogger(streetFighterController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    void pasarLeft(MouseEvent event) {
        switch (numImg) {
            case 1:
                numImg = 7;
                getNumImagen(numImg);
                imgMapa.setImage(new Image(getClass().getResourceAsStream(desplazamiento)));
                break;
            case 2:
                mapaEstablecidoIzquierda();
                break;
            case 3:
                mapaEstablecidoIzquierda();
                break;
            case 4:
                mapaEstablecidoIzquierda();
                break;
            case 5:
                mapaEstablecidoIzquierda();
                break;
            case 6:
                mapaEstablecidoIzquierda();
                break;
            case 7:
                mapaEstablecidoIzquierda();
                break;
            default:
                break;
        }
    }

    @FXML
    void pasarRight(MouseEvent event) {
        switch (numImg) {
            case 1:
                mapaEstablecidoDerecha();
                break;
            case 2:
                mapaEstablecidoDerecha();
                break;
            case 3:
                mapaEstablecidoDerecha();
                break;
            case 4:
                mapaEstablecidoDerecha();
                break;
            case 5:
                mapaEstablecidoDerecha();
                break;
            case 6:
                mapaEstablecidoDerecha();
                break;
            case 7:
                numImg = 1;
                getNumImagen(numImg);
                imgMapa.setImage(new Image(getClass().getResourceAsStream(desplazamiento)));
                break;
            default:
                break;
        }
    }

    public void mapaEstablecidoDerecha() {
        numImg++;
        getNumImagen(numImg);
        imgMapa.setImage(new Image(getClass().getResourceAsStream(desplazamiento)));
    }
    
    public void mapaEstablecidoIzquierda() {
        numImg--;
        getNumImagen(numImg);
        imgMapa.setImage(new Image(getClass().getResourceAsStream(desplazamiento)));
    }

    public static String getNumImagen(int numImg) {
        switch (numImg) {

            case 1:
                desplazamiento = "/streetFighterImagenes/escenarioCafe.gif"; 
                break;
            case 2:
                desplazamiento = "/streetFighterImagenes/escenarioDhalsim.gif";
                break;
            case 3:
                desplazamiento = "/streetFighterImagenes/escenarioGill.gif";
                break;
            case 4:
                desplazamiento = "/streetFighterImagenes/escenarioKen.gif";
                break;
            case 5:
                desplazamiento = "/streetFighterImagenes/escenarioRyu.gif";
                break;
            case 6:
                desplazamiento = "/streetFighterImagenes/escenarioTwelve.gif";
                break;
            case 7:
                desplazamiento = "/streetFighterImagenes/escenarioYang.gif";
                break;
        }

        return desplazamiento;
    }
    
     @FXML
    void seleccionarMapa(MouseEvent event) {
        
        flechaRight.setVisible(false);
        flechaLeft.setVisible(false);
        
        flechaRight.setDisable(true);
        flechaLeft.setDisable(true);
        
        imgVS.setVisible(false);
        
        imgSelect.setVisible(false);
        
        imgLuchar.setDisable(false);
        imgLuchar.setImage(new Image(getClass().getResourceAsStream("/streetFighterImagenes/fight!.png")));
        
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) { //INITIALIZE SIEMPRE SE EJECUTA ANTES QUE EL STAGE!
        numImg = 4;
        
        EleccionPersonajesController.getImageAzul(EleccionPersonajesController.azul);
        EleccionPersonajesController.getImageRojo(EleccionPersonajesController.rojo);

        pAzul.setImage(new Image(getClass().getResourceAsStream(EleccionPersonajesController.jugadorAzul)));
        pRojo.setImage(new Image(getClass().getResourceAsStream(EleccionPersonajesController.jugadorRojo)));
        
        imgLuchar.setDisable(true);
    }
    
    public static String getGifAzul(int numAzul) {
        switch (numAzul) {

            case 1:
                personajeAzul = "/streetFighterImagenes/akumaAzul.gif";
                break;
            case 2:
                personajeAzul = "/streetFighterImagenes/blankaAzul.gif";
                break;
            case 3:
                personajeAzul = "/streetFighterImagenes/chunliAzul.gif";
                break;
            case 4:
                personajeAzul = "/streetFighterImagenes/dhalsimAzul.gif";
                break;
            case 5:
                personajeAzul = "/streetFighterImagenes/ehondaAzul.gif";
                break;
            case 6:
                personajeAzul = "/streetFighterImagenes/guileAzul.gif";
                break;
            case 7:
                personajeAzul = "/streetFighterImagenes/kenAzul.gif";
                break;
            case 8:
                personajeAzul = "/streetFighterImagenes/mbisonAzul.gif";
                break;
            case 9:
                personajeAzul = "/streetFighterImagenes/ryuAzul.gif";
                break;
            case 10:
                personajeAzul = "/streetFighterImagenes/zangiefAzul.gif";
                break;

        }
        return personajeAzul;

    }
    
    public static String getGifRojo(int numRojo) {
        switch (numRojo) {

            case 1:
                personajeRojo = "/streetFighterImagenes/akumaRojo.gif";
                break;
            case 2:
                personajeRojo = "/streetFighterImagenes/blankaRojo.gif";
                break;
            case 3:
                personajeRojo = "/streetFighterImagenes/chunliRojo.gif";
                break;
            case 4:
                personajeRojo = "/streetFighterImagenes/dhalsimRojo.gif";
                break;
            case 5:
                personajeRojo = "/streetFighterImagenes/ehondaRojo.gif";
                break;
            case 6:
                personajeRojo = "/streetFighterImagenes/guileRojo.gif";
                break;
            case 7:
                personajeRojo = "/streetFighterImagenes/kenRojo.gif";
                break;
            case 8:
                personajeRojo = "/streetFighterImagenes/mbisonRojo.gif";
                break;
            case 9:
                personajeRojo = "/streetFighterImagenes/ryuRojo.gif";
                break;
            case 10:
                personajeRojo = "/streetFighterImagenes/zangiefRojo.gif";
                break;

        }
        return personajeRojo;

    }

    
}
