package controladores;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class EleccionPersonajesController implements Initializable {

    private boolean pAzulChosen;

    private boolean pRojoChosen;
    
    public static String nombreAzul;
    public static String nombreRojo;

    @FXML
    private ImageView azar;

    @FXML
    private ImageView akumaP1;

    @FXML
    private ImageView blankaP2;

    @FXML
    private ImageView chunLiP3;

    @FXML
    private ImageView dhalsimP4;

    @FXML
    private ImageView eHondaP5;

    @FXML
    private ImageView guileP6;

    @FXML
    private ImageView kenP7;

    @FXML
    private ImageView mBisonP8;

    @FXML
    private ImageView ryuP9;

    @FXML
    private ImageView zangiefP10;

    @FXML
    private ImageView imgContinuar;

    @FXML
    private ImageView imgCharacterSelect;

    @FXML
    private ImageView personajeAzul;

    @FXML
    private ImageView personajeRojo;

    @FXML
    private Stage stageM;

    public static int azul;
    public static int rojo;
    public static String jugadorAzul;
    public static String jugadorRojo;

    @FXML
    void desvisualizarImagen(MouseEvent event) {

        if (!ispAzulChosen()) {
            personajeAzul.setVisible(false);
            personajeAzul.setFitWidth(600);
            personajeAzul.setFitHeight(600);

        } else if (ispAzulChosen() && !ispRojoChosen()) {
            personajeRojo.setVisible(false);
            personajeRojo.setFitWidth(600);
            personajeRojo.setFitHeight(600);
        }
    }

    @FXML
    void elegirMapa(MouseEvent event) {
        try {
            FXMLLoader loaderM = new FXMLLoader(getClass().getResource("/vistas/eleccionMapas.fxml"));
            Parent root = loaderM.load();

            stageM = (Stage) ((Node) event.getSource()).getScene().getWindow();
            Scene scene = new Scene(root);
            stageM.setScene(scene);

            stageM.show();

        } catch (IOException ex) {
            Logger.getLogger(streetFighterController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    void seleccionPersonaje(MouseEvent event) {
        if (!ispAzulChosen()) {
            setpAzulChosen(true);
            
            if (event.getSource() == azar) {
                randomAzul();
            } else {
                personajeAzul(event);
            }
            personajeAzul.setVisible(true);
        } else if (ispAzulChosen() && !ispRojoChosen()) {
            setpRojoChosen(true);
            
            if (event.getSource() == azar) {
                randomRojo();
            } else {
                personajeRojo(event);
            }
            personajeRojo.setVisible(true);

            imgContinuar.setImage(new Image(getClass().getResourceAsStream("/streetFighterImagenes/continuarBlanco.png")));
            imgContinuar.setDisable(false);
            imgCharacterSelect.setVisible(false);
        }

    }

    @FXML
    void visualizarImagen(MouseEvent event) {

        if (!ispAzulChosen()) {
            personajeAzul.setVisible(true);
            personajeAzul(event);
        } else if (ispAzulChosen() && !ispRojoChosen()) {
            personajeRojo.setVisible(true);
            personajeRojo(event);
        }

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        imgContinuar.setDisable(true);
        pAzulChosen = false;
        pRojoChosen = false;
        nombreAzul = null;
        nombreRojo = null;
    }

    public static void getImageAzul(int azul) {
        switch (azul) {

            case 1:
                jugadorAzul = "/streetFighterImagenes/p1Akuma.jpg";
                nombreAzul = "Akuma";
                break;
            case 2:
                jugadorAzul = "/streetFighterImagenes/p2BlankaFlip.jpg";
                nombreAzul = "Blanka";
                break;
            case 3:
                jugadorAzul = "/streetFighterImagenes/p3Chunli.jpg";
                nombreAzul = "Chun-Li";
                break;
            case 4:
                jugadorAzul = "/streetFighterImagenes/p4DhalsimFlip.jpg";
                nombreAzul = "Dhalsim";
                break;
            case 5:
                jugadorAzul = "/streetFighterImagenes/p5EhondaFlip.jpg";
                nombreAzul = "E.Honda";
                break;
            case 6:
                jugadorAzul = "/streetFighterImagenes/p6GuileFlip.jpg";
                nombreAzul = "Guile";
                break;
            case 7:
                jugadorAzul = "/streetFighterImagenes/p7Ken.jpg";
                nombreAzul = "Ken";
                break;
            case 8:
                jugadorAzul = "/streetFighterImagenes/p8MbisonFlip.jpg";
                nombreAzul = "M.Bison";
                break;
            case 9:
                jugadorAzul = "/streetFighterImagenes/p9RyuFlip.jpg";
                nombreAzul = "Ryu";
                break;
            case 10:
                jugadorAzul = "/streetFighterImagenes/p10Zangief.jpg";
                nombreAzul = "Zangief";
                break;

        }

    }

    public static void getImageRojo(int rojo) {
        switch (rojo) {

            case 1:
                jugadorRojo = "/streetFighterImagenes/p1AkumaFlip.jpg";
                nombreRojo = "Akuma";
                break;
            case 2:
                jugadorRojo = "/streetFighterImagenes/p2Blanka.jpg";
                nombreRojo = "Blanka";
                break;
            case 3:
                jugadorRojo = "/streetFighterImagenes/p3ChunliFlip.jpg";
                nombreRojo = "Chun-Li";
                break;
            case 4:
                jugadorRojo = "/streetFighterImagenes/p4Dhalsim.jpg";
                nombreRojo = "Dhalsim";
                break;
            case 5:
                jugadorRojo = "/streetFighterImagenes/p5Ehonda.jpg";
                nombreRojo = "E.Honda";
                break;
            case 6:
                jugadorRojo = "/streetFighterImagenes/p6Guile.jpg";
                nombreRojo = "Guile";
                break;
            case 7:
                jugadorRojo = "/streetFighterImagenes/p7KenFlip.jpg";
                nombreRojo = "Ken";
                break;
            case 8:
                jugadorRojo = "/streetFighterImagenes/p8Mbison.jpg";
                nombreRojo = "M.Bison";
                break;
            case 9:
                jugadorRojo = "/streetFighterImagenes/p9Ryu.jpg";
                nombreRojo = "Ryu";
                break;
            case 10:
                jugadorRojo = "/streetFighterImagenes/p10ZangiefFlip.jpg";
                nombreRojo = "Zangief";
                break;

        }

    }

    public boolean ispAzulChosen() {
        return pAzulChosen;
    }

    public void setpAzulChosen(boolean pAzulChosen) {
        this.pAzulChosen = pAzulChosen;
    }

    public boolean ispRojoChosen() {
        return pRojoChosen;
    }

    public void setpRojoChosen(boolean pRojoChosen) {
        this.pRojoChosen = pRojoChosen;
    }

    void personajeAzul(MouseEvent event) {
        if (event.getSource() == akumaP1) {
            personajeAzul.setImage(new Image(getClass().getResourceAsStream("/streetFighterImagenes/akuma3RB.png")));
            personajeAzul.setFitWidth(645);
            personajeAzul.setFitHeight(645);
            azul = 1;
            System.out.println(azul);
        } else if (event.getSource() == blankaP2) {
            personajeAzul.setImage(new Image(getClass().getResourceAsStream("/streetFighterImagenes/blanka.png")));
            azul = 2;
        } else if (event.getSource() == chunLiP3) {
            personajeAzul.setImage(new Image(getClass().getResourceAsStream("/streetFighterImagenes/chunli.png")));
            azul = 3;
        } else if (event.getSource() == dhalsimP4) {
            personajeAzul.setImage(new Image(getClass().getResourceAsStream("/streetFighterImagenes/dhalsim.png")));
            azul = 4;
        } else if (event.getSource() == eHondaP5) {
            personajeAzul.setImage(new Image(getClass().getResourceAsStream("/streetFighterImagenes/ehonda.png")));
            personajeAzul.setFitWidth(720);
            personajeAzul.setFitHeight(715);
            azul = 5;
        } else if (event.getSource() == guileP6) {
            personajeAzul.setImage(new Image(getClass().getResourceAsStream("/streetFighterImagenes/guileFlip.png")));
            azul = 6;
        } else if (event.getSource() == kenP7) {
            personajeAzul.setImage(new Image(getClass().getResourceAsStream("/streetFighterImagenes/ken.png")));
            azul = 7;
        } else if (event.getSource() == mBisonP8) {
            personajeAzul.setImage(new Image(getClass().getResourceAsStream("/streetFighterImagenes/mBison.png")));
            azul = 8;
        } else if (event.getSource() == ryuP9) {
            personajeAzul.setImage(new Image(getClass().getResourceAsStream("/streetFighterImagenes/ryu.png")));
            personajeAzul.setFitWidth(720);
            personajeAzul.setFitHeight(720);
            azul = 9;
        } else {
            personajeAzul.setImage(new Image(getClass().getResourceAsStream("/streetFighterImagenes/zangiefR.png")));
            azul = 10;
        }
    }

    void personajeRojo(MouseEvent event) {
        if (event.getSource() == akumaP1) {
            personajeRojo.setImage(new Image(getClass().getResourceAsStream("/streetFighterImagenes/akumaFlip.png")));
            personajeRojo.setFitWidth(645);
            personajeRojo.setFitHeight(645);
            rojo = 1;
            System.out.println(rojo);
        } else if (event.getSource() == blankaP2) {
            personajeRojo.setImage(new Image(getClass().getResourceAsStream("/streetFighterImagenes/blankaFlip.png")));
            rojo = 2;
        } else if (event.getSource() == chunLiP3) {
            personajeRojo.setImage(new Image(getClass().getResourceAsStream("/streetFighterImagenes/chunliFlip.png")));
            rojo = 3;
        } else if (event.getSource() == dhalsimP4) {
            personajeRojo.setImage(new Image(getClass().getResourceAsStream("/streetFighterImagenes/dhalsimFlip.png")));
            rojo = 4;
        } else if (event.getSource().equals(eHondaP5)) {
            personajeRojo.setImage(new Image(getClass().getResourceAsStream("/streetFighterImagenes/ehondaFlip.png")));
            personajeRojo.setFitWidth(720);
            personajeRojo.setFitHeight(715);
            rojo = 5;
        } else if (event.getSource() == guileP6) {
            personajeRojo.setImage(new Image(getClass().getResourceAsStream("/streetFighterImagenes/guile.png")));
            rojo = 6;
        } else if (event.getSource() == kenP7) {
            personajeRojo.setImage(new Image(getClass().getResourceAsStream("/streetFighterImagenes/kenFlip.png")));
            rojo = 7;
        } else if (event.getSource() == mBisonP8) {
            personajeRojo.setImage(new Image(getClass().getResourceAsStream("/streetFighterImagenes/mBisonFlip.png")));
            rojo = 8;
        } else if (event.getSource() == ryuP9) {
            personajeRojo.setImage(new Image(getClass().getResourceAsStream("/streetFighterImagenes/ryuFlip.png")));
            personajeRojo.setFitWidth(720);
            personajeRojo.setFitHeight(720);
            rojo = 9;
        } else {
            personajeRojo.setImage(new Image(getClass().getResourceAsStream("/streetFighterImagenes/zangiefRFlip.png")));
            rojo = 10;
        }
    }

    private void randomAzul() {
        int numAzulAzar = (int) (Math.random() * 10 + 1);
        System.out.println(numAzulAzar);
        switch (numAzulAzar) {
            case 1:
                personajeAzul.setImage(new Image(getClass().getResourceAsStream("/streetFighterImagenes/akuma3RB.png")));
                personajeAzul.setFitWidth(645);
                personajeAzul.setFitHeight(645);
                azul = 1;
                break;
            case 2:
                personajeAzul.setImage(new Image(getClass().getResourceAsStream("/streetFighterImagenes/blanka.png")));
                azul = 2;
                break;
            case 3:
                personajeAzul.setImage(new Image(getClass().getResourceAsStream("/streetFighterImagenes/chunli.png")));
                azul = 3;
                break;
            case 4:
                personajeAzul.setImage(new Image(getClass().getResourceAsStream("/streetFighterImagenes/dhalsim.png")));
                azul = 4;
                break;
            case 5:
                personajeAzul.setImage(new Image(getClass().getResourceAsStream("/streetFighterImagenes/ehonda.png")));
                personajeAzul.setFitWidth(720);
                personajeAzul.setFitHeight(715);
                azul = 5;
                break;
            case 6:
                personajeAzul.setImage(new Image(getClass().getResourceAsStream("/streetFighterImagenes/guileFlip.png")));
                azul = 6;
                break;
            case 7:
                personajeAzul.setImage(new Image(getClass().getResourceAsStream("/streetFighterImagenes/ken.png")));
                azul = 7;
                break;
            case 8:
                personajeAzul.setImage(new Image(getClass().getResourceAsStream("/streetFighterImagenes/mBison.png")));
                azul = 8;
                break;
            case 9:
                personajeAzul.setImage(new Image(getClass().getResourceAsStream("/streetFighterImagenes/ryu.png")));
                personajeAzul.setFitWidth(720);
                personajeAzul.setFitHeight(720);
                azul = 9;
                break;
            case 10:
                personajeAzul.setImage(new Image(getClass().getResourceAsStream("/streetFighterImagenes/zangiefR.png")));
                azul = 10;
                break;
            default:
                break;
        }
    }

    private void randomRojo() {
        int numRojoAzar = (int) (Math.random() * 10 + 1);
        System.out.println(numRojoAzar);
        switch (numRojoAzar) {
            case 1:
                personajeRojo.setImage(new Image(getClass().getResourceAsStream("/streetFighterImagenes/akumaFlip.png")));
                personajeRojo.setFitWidth(645);
                personajeRojo.setFitHeight(645);
                rojo = 1;
                System.out.println(rojo);
                break;
            case 2:
                personajeRojo.setImage(new Image(getClass().getResourceAsStream("/streetFighterImagenes/blankaFlip.png")));
                rojo = 2;
                break;
            case 3:
                personajeRojo.setImage(new Image(getClass().getResourceAsStream("/streetFighterImagenes/chunliFlip.png")));
                rojo = 3;
                break;
            case 4:
                personajeRojo.setImage(new Image(getClass().getResourceAsStream("/streetFighterImagenes/dhalsimFlip.png")));
                rojo = 4;
                break;
            case 5:
                personajeRojo.setImage(new Image(getClass().getResourceAsStream("/streetFighterImagenes/ehondaFlip.png")));
                personajeRojo.setFitWidth(720);
                personajeRojo.setFitHeight(715);
                rojo = 5;
                break;
            case 6:
                personajeRojo.setImage(new Image(getClass().getResourceAsStream("/streetFighterImagenes/guile.png")));
                rojo = 6;
                break;
            case 7:
                personajeRojo.setImage(new Image(getClass().getResourceAsStream("/streetFighterImagenes/kenFlip.png")));
                rojo = 7;
                break;
            case 8:
                personajeRojo.setImage(new Image(getClass().getResourceAsStream("/streetFighterImagenes/mBisonFlip.png")));
                rojo = 8;
                break;
            case 9:
                personajeRojo.setImage(new Image(getClass().getResourceAsStream("/streetFighterImagenes/ryuFlip.png")));
                personajeRojo.setFitWidth(720);
                personajeRojo.setFitHeight(720);
                rojo = 9;
                break;
            case 10:
                personajeRojo.setImage(new Image(getClass().getResourceAsStream("/streetFighterImagenes/zangiefRFlip.png")));
                rojo = 10;
                break;
            default:
                break;
        }
    }

}
