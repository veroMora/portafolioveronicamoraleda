/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package controladores;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author veromose
 */
public class GanadorController implements Initializable {

    @FXML
    private Button btBatallas;

    @FXML
    private Button btCombate;

    @FXML
    private Button btRevancha;

    @FXML
    private Button btSalirGanador;

    @FXML
    private ImageView ganador;

    @FXML
    private ImageView jugadorID;

    @FXML
    private Stage stageTabla;

    @FXML
    void nuevosPersonajes(ActionEvent event) throws IOException {

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/vistas/eleccionPersonajes.fxml"));
            Parent root = loader.load();

            streetFighterController.stageP = (Stage) ((Node) event.getSource()).getScene().getWindow();
            Scene scene = new Scene(root);
            streetFighterController.stageP.setScene(scene);

            streetFighterController.stageP.show();

        } catch (IOException ex) {
            Logger.getLogger(streetFighterController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    void revanchaPelea(ActionEvent event) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/vistas/pelea.fxml"));
            Parent root = loader.load();

            EleccionMapasController.stagePelea = (Stage) ((Node) event.getSource()).getScene().getWindow();
            Scene scene = new Scene(root);
            EleccionMapasController.stagePelea.setScene(scene);

            EleccionMapasController.stagePelea.show();

        } catch (IOException ex) {
            Logger.getLogger(streetFighterController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    void salirJuego(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setHeaderText(null);
        alert.setTitle("Confirmacion");
        alert.setContentText("¿Estas seguro/a de que quieres salir?");
        alert.showAndWait();

        if (alert.getResult().equals(ButtonType.OK)) {
            Platform.exit();
        }
    }

    @FXML
    void tablaBatallas(ActionEvent event) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/vistas/tabla.fxml"));
            Parent root = loader.load();

            stageTabla = (Stage) ((Node) event.getSource()).getScene().getWindow();
            Scene scene = new Scene(root);
            stageTabla.setScene(scene);

            stageTabla.show();

        } catch (IOException ex) {
            Logger.getLogger(streetFighterController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        PeleaController.rutaImagenGanador(PeleaController.numGanador);

        ganador.setImage(new Image(getClass().getResourceAsStream(PeleaController.jugadorGanador)));
        jugadorID.setImage(new Image(getClass().getResourceAsStream(PeleaController.idJugador)));
        jugadorID.setX(PeleaController.xGanador);
        jugadorID.setY(PeleaController.yGanador);
    }

}
