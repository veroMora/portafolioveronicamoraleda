package controladores;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class streetFighterController implements Initializable {

    @FXML
    public static Stage stageP;

    @FXML
    private ImageView imgStart;

    @FXML
    void startJuego(MouseEvent event) {
try {
            FXMLLoader loaderM = new FXMLLoader(getClass().getResource("/vistas/eleccionPersonajes.fxml"));
            Parent root = loaderM.load();
            
            stageP = (Stage) ((Node)event.getSource()).getScene().getWindow();
            Scene scene = new Scene(root);
            stageP.setScene(scene);
            stageP.show();
            
            
        } catch (IOException ex) {
            Logger.getLogger(streetFighterController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }

    public void setStage(Stage stageInicio) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
