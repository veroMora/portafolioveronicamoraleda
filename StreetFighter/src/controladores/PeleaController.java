/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package controladores;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import modelo.Batallas;
import modelo.Personajes;

/**
 * FXML Controller class
 *
 * @author veromose
 */
public class PeleaController implements Initializable {

    private String gifAzul;
    private String gifRojo;
    
    public static ArrayList<Batallas> batalla = new ArrayList<>();

    //String nombreAzul = getNombreAzul(EleccionPersonajesController.azul);
    //String nombreRojo = getNombreRojo(EleccionPersonajesController.rojo);
    Personajes pAzul = new Personajes(150.0);
    Personajes pRojo = new Personajes(150.0);
    private int contAzul = 0;
    private int contRojo = 0;
    private String nomGanador;
    int ataque;
    long time;
    int xAzul;
    int yAzul;
    int wAzul;
    int hAzul;

    int xRojo;
    int yRojo;
    int wRojo;
    int hRojo;

    public static String jugadorGanador;
    public static int numGanador;
    public static int xGanador;
    public static int yGanador;
    public static String idJugador;

    private Random azar = new Random();

    @FXML
    private AnchorPane panelMapa;

    @FXML
    public ImageView mapaSeleccionado;

    @FXML
    private ImageView j1;

    @FXML
    private ImageView j2;

    @FXML
    private ImageView ataqueGifAzul;

    @FXML
    private ImageView ataqueGifRojo;

    @FXML
    private ImageView KO;

    @FXML
    private Button btPunyoAzul;

    @FXML
    private Button btPatadaAzul;

    @FXML
    private Button btEspecialAzul;

    @FXML
    private Button btPunyoRojo;

    @FXML
    private Button btPatadaRojo;

    @FXML
    private Button btEspecialRojo;

    @FXML
    private ProgressBar barraVidaAzul;

    @FXML
    private ProgressBar barraVidaRojo;

    @FXML
    private ProgressIndicator indicadorEspecialAzul;

    @FXML
    private ProgressIndicator indicadorEspecialRojo;

    @FXML
    public static Stage stageGanador;

    void cambiarVentana(Stage stage) {
        try {

            FXMLLoader loaderF = new FXMLLoader(getClass().getResource("/vistas/ganador.fxml"));

            Parent root = loaderF.load();

            GanadorController gC = loaderF.getController();
            Scene scene = new Scene(root);
            stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setScene(scene);
            stage.show();

            EleccionMapasController.stagePelea.close();

        } catch (IOException ex) {
            Logger.getLogger(streetFighterController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    void ataquesAzules() {
        posicionamientoGif(ataqueGifAzul, j1, this.xAzul, this.yAzul, this.wAzul, this.hAzul);
        timer(ataqueGifAzul, j1, this.gifAzul, this.time);
    }

    @FXML
    void accionAzul(ActionEvent event) {

        if (event.getSource() == btPunyoAzul) {
            getGifAtaquePunyoAzul(EleccionPersonajesController.azul);
            ataquesAzules();

            ataque = ataquePuñetazo();
            contAzul++;

        } else if (event.getSource() == btPatadaAzul) {
            getGifAtaquePatadaAzul(EleccionPersonajesController.azul);
            ataquesAzules();

            ataque = ataquePatada();
            contAzul += 3;

        } else {
            getGifAtaqueEspecialAzul(EleccionPersonajesController.azul);
            ataquesAzules();

            ataque = ataqueEspecial();
            contAzul = 0;

        }

        pRojo.perderVida(ataque);

        double specialValueAzul = contAzul / 10.0;
        indicadorEspecialAzul.setProgress(specialValueAzul);

        double valueRojo = pRojo.getVida() / 150.0;
        barraVidaRojo.setProgress(valueRojo);

        if (pRojo.getVida() <= 0.0) {
            KO.setImage(new Image(getClass().getResourceAsStream("/streetFighterImagenes/ko.png")));
            numGanador = EleccionPersonajesController.azul;
            idJugador = "/streetFighterImagenes/jugador1.png";
            nomGanador = EleccionPersonajesController.nombreAzul;
            btPunyoAzul.setDisable(true);
            btPatadaAzul.setDisable(true);
            btEspecialAzul.setDisable(true);
            
            Batallas b = new Batallas(EleccionPersonajesController.nombreAzul, EleccionPersonajesController.nombreRojo, nomGanador);
            b.getBatalla();
            batalla.add(b);
            delay(time, () -> cambiarVentana(stageGanador));
        } else {
            delay(time, () -> atacarRojo());
            btPunyoAzul.setDisable(true);
            btPatadaAzul.setDisable(true);
            btEspecialAzul.setDisable(true);
        }

    }

    void ataquesRojos() {
        posicionamientoGif(ataqueGifRojo, j2, this.xRojo, this.yRojo, this.wRojo, this.hRojo);
        timer(ataqueGifRojo, j2, this.gifRojo, this.time);
    }

    @FXML
    void accionRojo(ActionEvent event) {

        if (event.getSource() == btPunyoRojo) {
            getGifAtaquePunyoRojo(EleccionPersonajesController.rojo);
            ataquesRojos();

            ataque = ataquePuñetazo();
            contRojo++;

        } else if (event.getSource() == btPatadaRojo) {
            getGifAtaquePatadaRojo(EleccionPersonajesController.rojo);
            ataquesRojos();

            ataque = ataquePatada();
            contRojo += 3;

        } else {
            getGifAtaqueEspecialRojo(EleccionPersonajesController.rojo);
            ataquesRojos();

            ataque = ataqueEspecial();
            contRojo = 0;

        }
       
        pAzul.perderVida(ataque);

        double specialValueRojo = contRojo / 10.0;
        indicadorEspecialRojo.setProgress(specialValueRojo);

        double valueAzul = pAzul.getVida() / 150.0;
        barraVidaAzul.setProgress(valueAzul);

        if (pAzul.getVida() <= 0.0) {
            KO.setImage(new Image(getClass().getResourceAsStream("/streetFighterImagenes/ko.png")));
            numGanador = EleccionPersonajesController.rojo;
            idJugador = "/streetFighterImagenes/jugador2.png";
            nomGanador = EleccionPersonajesController.nombreRojo;
            btPunyoRojo.setDisable(true);
            btPatadaRojo.setDisable(true);
            btEspecialRojo.setDisable(true);
            
            Batallas b = new Batallas(EleccionPersonajesController.nombreAzul, EleccionPersonajesController.nombreRojo, nomGanador);
            b.getBatalla();
            batalla.add(b);
            delay(time, () -> cambiarVentana(stageGanador));
        } else {

            delay(time, () -> atacarAzul());
            btPunyoRojo.setDisable(true);
            btPatadaRojo.setDisable(true);
            btEspecialRojo.setDisable(true);
        }
    }

    private void posicionamientoGif(ImageView imgGif, ImageView jugador, int x, int y, int w, int h) {
        jugador.setVisible(false);

        imgGif.setFitWidth(w);
        imgGif.setFitHeight(h);
        imgGif.setX(x);
        imgGif.setY(y);

    }

    public static void delay(long millis, Runnable continuation) {
        Task<Void> sleeper = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                try {
                    Thread.sleep(millis);
                } catch (InterruptedException e) {
                }
                return null;
            }
        };
        sleeper.setOnSucceeded(event -> continuation.run());
        new Thread(sleeper).start();
    }

    private void timer(ImageView imgGif, ImageView jugador, String rutaImg, long time) {

        Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {

                imgGif.setVisible(false);
                jugador.setVisible(true);
                timer.cancel();
            }

        };

        imgGif.setImage(new Image(getClass().getResourceAsStream(rutaImg)));
        imgGif.setVisible(true);

        timer.schedule(task, time);
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        EleccionMapasController.getNumImagen(EleccionMapasController.numImg);
        System.out.println(EleccionMapasController.desplazamiento);
        mapaSeleccionado.setImage(new Image(getClass().getResourceAsStream(EleccionMapasController.desplazamiento)));

        EleccionMapasController.getGifAzul(EleccionPersonajesController.azul);
        EleccionMapasController.getGifRojo(EleccionPersonajesController.rojo);

        j1.setImage(new Image(getClass().getResourceAsStream(EleccionMapasController.personajeAzul)));
        j2.setImage(new Image(getClass().getResourceAsStream(EleccionMapasController.personajeRojo)));

        int azarJugador = (int) (Math.random() * 2 + 1);
        System.out.println("Empieza el jugador: " + azarJugador);

        if (azarJugador == 1) {
            btPunyoRojo.setDisable(true);
        } else {
            btPunyoAzul.setDisable(true);
        }

        btPatadaAzul.setDisable(true);
        btEspecialAzul.setDisable(true);
        btPatadaRojo.setDisable(true);
        btEspecialRojo.setDisable(true);

        barraVidaAzul.setProgress(1.0);
        barraVidaRojo.setProgress(1.0);

        indicadorEspecialAzul.setProgress(0.0);
        indicadorEspecialRojo.setProgress(0.0);

    }

    public void atacarRojo() {
        System.out.println("Contador Rojo: " + contRojo);
        if (contRojo >= 10) {
            btEspecialRojo.setDisable(false);

        } else {
            if (esPatada()) {
                btPatadaRojo.setDisable(false);

            } else {
                btPunyoRojo.setDisable(false);

            }
        }
    }

    public void atacarAzul() {
        System.out.println("Contador Azul: " + contAzul);
        if (contAzul >= 10) {
            btEspecialAzul.setDisable(false);

        } else {
            if (esPatada()) {
                btPatadaAzul.setDisable(false);

            } else {
                btPunyoAzul.setDisable(false);

            }
        }
    }

    private int ataquePuñetazo() {

        int ataquePuñetazo = (int) (Math.random() * 10 + 1);
        System.out.println("Daño ataque puño: " + ataquePuñetazo);
        return ataquePuñetazo;
    }

    private int ataquePatada() {
        int ataquePatada = (int) (Math.random() * (25 - 10 + 1) + 10);
        System.out.println("Daño ataque patada: " + ataquePatada);
        return ataquePatada;
    }

    private int ataqueEspecial() {
        int ataqueEspecial = (int) (Math.random() * (50 - 30 + 1) + 30);
        System.out.println("Daño ataque especial: " + ataqueEspecial);
        return ataqueEspecial;
    }

    private boolean esPatada() {
        int valor = azar.nextInt(100) + 1;
        System.out.println(valor);
        /*
		 * Entre 1 y 100 hay veinte valores que son
		 * múltiplos de 5.
		 * Es decir, hay un 20% de posibilidades
		 * de obtener un valor múltiplo de 5.
         */

        return valor % 5 == 0;
    }

    public String getGifAtaqueEspecialAzul(int numAzul) {
        switch (numAzul) {

            case 1:
                gifAzul = "/streetFighterImagenes/akumaAtaqueEspecialAzul.gif";
                time = 5390L;
                xAzul = 110;
                yAzul = 7;
                wAzul = 670;
                hAzul = 710;
                break;
            case 2:
                gifAzul = "/streetFighterImagenes/blankaAtaqueEspecialAzul.gif";
                time = 1980L;
                xAzul = 190;
                yAzul = 325;
                wAzul = 590;
                hAzul = 375;
                break;
            case 3:
                gifAzul = "/streetFighterImagenes/chunliAtaqueEspecialAzul.gif";
                time = 4380L;
                xAzul = 90;
                yAzul = 250;
                wAzul = 754;
                hAzul = 450;
                break;
            case 4:
                gifAzul = "/streetFighterImagenes/dhalsimAtaqueEspecialAzul.gif";
                time = 6700L;
                xAzul = 220;
                yAzul = 240;
                wAzul = 627;
                hAzul = 475;
                break;
            case 5:
                gifAzul = "/streetFighterImagenes/ehondaAtaqueEspecialAzul.gif";
                time = 600L;
                xAzul = 260;
                yAzul = 365;
                wAzul = 614;
                hAzul = 340;
                break;
            case 6:
                gifAzul = "/streetFighterImagenes/guileAtaqueEspecialAzul.gif";
                time = 4100L;
                xAzul = 280;
                yAzul = 400;
                wAzul = 852;
                hAzul = 295;
                break;
            case 7:
                gifAzul = "/streetFighterImagenes/kenAtaqueEspecialAzul.gif";
                time = 1620L;
                xAzul = 210;
                yAzul = 10;
                wAzul = 660;
                hAzul = 710;
                break;
            case 8:
                gifAzul = "/streetFighterImagenes/mbisonAtaqueEspecialAzul.gif";
                time = 3600L;
                xAzul = 200;
                yAzul = 275;
                wAzul = 978;
                hAzul = 425;
                break;
            case 9:
                gifAzul = "/streetFighterImagenes/ryuAtaqueEspecialAzul.gif";
                time = 4300L;
                xAzul = 90;
                yAzul = 320;
                wAzul = 1109;
                hAzul = 400;
                break;
            case 10:
                gifAzul = "/streetFighterImagenes/zangiefAtaqueEspecialAzul.gif";
                time = 7920L;
                xAzul = 28;
                yAzul = 10;
                wAzul = 1230;
                hAzul = 710;
                break;

        }
        return gifAzul;

    }

    public String getGifAtaquePunyoAzul(int numAzul) {
        switch (numAzul) {

            case 1:
                gifAzul = "/streetFighterImagenes/akumaAtaquePunyoAzul.gif";
                time = 1260L;
                xAzul = 300;
                yAzul = 295;
                wAzul = 468;
                hAzul = 425;
                break;
            case 2:
                gifAzul = "/streetFighterImagenes/blankaAtaquePunyoAzul.gif";
                time = 800L;
                xAzul = 310;
                yAzul = 370;
                wAzul = 467;
                hAzul = 330;
                break;
            case 3:
                gifAzul = "/streetFighterImagenes/chunliAtaquePunyoAzul.gif";
                time = 1070L;
                xAzul = 215;
                yAzul = 360;
                wAzul = 832;
                hAzul = 340;
                break;
            case 4:
                gifAzul = "/streetFighterImagenes/dhalsimAtaquePunyoAzul.gif";
                time = 1700L;
                xAzul = 287;
                yAzul = 367;
                wAzul = 727;
                hAzul = 335;
                break;
            case 5:
                gifAzul = "/streetFighterImagenes/ehondaAtaquePunyoAzul.gif";
                time = 1850L;
                xAzul = 175;
                yAzul = 250;
                wAzul = 977;
                hAzul = 450;
                break;
            case 6:
                gifAzul = "/streetFighterImagenes/guileAtaquePunyoAzul.gif";
                time = 1900L;
                xAzul = 280;
                yAzul = 400;
                wAzul = 780;
                hAzul = 295;
                break;
            case 7:
                gifAzul = "/streetFighterImagenes/kenAtaquePunyoAzul.gif";
                time = 1800L;
                xAzul = 315;
                yAzul = 385;
                wAzul = 727;
                hAzul = 320;
                break;
            case 8:
                gifAzul = "/streetFighterImagenes/mbisonAtaquePunyoAzul.gif";
                time = 580L;
                xAzul = 235;
                yAzul = 275;
                wAzul = 787;
                hAzul = 425;
                break;
            case 9:
                gifAzul = "/streetFighterImagenes/ryuAtaquePunyoAzul.gif";
                time = 1040L;
                xAzul = 267;
                yAzul = 377;
                wAzul = 780;
                hAzul = 335;
                break;
            case 10:
                gifAzul = "/streetFighterImagenes/zangiefAtaquePunyoAzul.gif";
                time = 2280L;
                xAzul = 130;
                yAzul = 400;
                wAzul = 700;
                hAzul = 700;
                break;

        }
        return gifAzul;

    }

    public String getGifAtaquePatadaAzul(int numAzul) {
        switch (numAzul) {

            case 1:
                gifAzul = "/streetFighterImagenes/akumaAtaquePatadaAzul.gif";
                time = 900L;
                xAzul = 315;
                yAzul = 390;
                wAzul = 405;
                hAzul = 320;
                break;
            case 2:
                gifAzul = "/streetFighterImagenes/blankaAtaquePatadaAzul.gif";
                time = 2650L;
                xAzul = 210;
                yAzul = 130;
                wAzul = 868;
                hAzul = 575;
                break;
            case 3:
                gifAzul = "/streetFighterImagenes/chunliAtaquePatadaAzul.gif";
                time = 3180L;
                xAzul = 110;
                yAzul = 20;
                wAzul = 1120;
                hAzul = 710;
                break;
            case 4:
                gifAzul = "/streetFighterImagenes/dhalsimAtaquePatadaAzul.gif";
                time = 2400L;
                xAzul = 200;
                yAzul = 220;
                wAzul = 767;
                hAzul = 500;
                break;
            case 5:
                gifAzul = "/streetFighterImagenes/ehondaAtaquePatadaAzul.gif";
                time = 2400L;
                xAzul = 280;
                yAzul = 83;
                wAzul = 753;
                hAzul = 630;
                break;
            case 6:
                gifAzul = "/streetFighterImagenes/guileAtaquePatadaAzul.gif";
                time = 3020L;
                xAzul = 300;
                yAzul = 150;
                wAzul = 634;
                hAzul = 550;
                break;
            case 7:
                gifAzul = "/streetFighterImagenes/kenAtaquePatadaAzul.gif";
                time = 3150L;
                xAzul = 160;
                yAzul = 35;
                wAzul = 781;
                hAzul = 665;
                break;
            case 8:
                gifAzul = "/streetFighterImagenes/mbisonAtaquePatadaAzul.gif";
                time = 1080L;
                xAzul = 170;
                yAzul = 115;
                wAzul = 947;
                hAzul = 600;
                break;
            case 9:
                gifAzul = "/streetFighterImagenes/ryuAtaquePatadaAzul.gif";
                time = 4000L;
                xAzul = 260;
                yAzul = 165;
                wAzul = 798;
                hAzul = 555;
                break;
            case 10:
                gifAzul = "/streetFighterImagenes/zangiefAtaquePatadaAzul.gif";
                time = 3510L;
                xAzul = 230;
                yAzul = 310;
                wAzul = 564;
                hAzul = 400;
                break;

        }
        return gifAzul;

    }

    public String getGifAtaqueEspecialRojo(int numRojo) {
        switch (numRojo) {

            case 1:
                gifRojo = "/streetFighterImagenes/akumaAtaqueEspecialRojo.gif";
                time = 5390L;
                xRojo = 470;
                yRojo = 10;
                wRojo = 670;
                hRojo = 710;
                break;
            case 2:
                gifRojo = "/streetFighterImagenes/blankaAtaqueEspecialRojo.gif";
                time = 1980L;
                xRojo = 510;
                yRojo = 325;
                wRojo = 590;
                hRojo = 375;
                break;
            case 3:
                gifRojo = "/streetFighterImagenes/chunliAtaqueEspecialRojo.gif";
                time = 4380L;
                xRojo = 390;
                yRojo = 250;
                wRojo = 754;
                hRojo = 450;
                break;
            case 4:
                gifRojo = "/streetFighterImagenes/dhalsimAtaqueEspecialRojo.gif";
                time = 6700L;
                xRojo = 450;
                yRojo = 240;
                wRojo = 627;
                hRojo = 475;
                break;
            case 5:
                gifRojo = "/streetFighterImagenes/ehondaAtaqueEspecialRojo.gif";
                time = 600L;
                xRojo = 465;
                yRojo = 365;
                wRojo = 614;
                hRojo = 340;
                break;
            case 6:
                gifRojo = "/streetFighterImagenes/guileAtaqueEspecialRojo.gif";
                time = 4100L;
                xRojo = 110;
                yRojo = 400;
                wRojo = 852;
                hRojo = 295;
                break;
            case 7:
                gifRojo = "/streetFighterImagenes/kenAtaqueEspecialRojo.gif";
                time = 1620L;
                xRojo = 340;
                yRojo = 10;
                wRojo = 660;
                hRojo = 710;
                break;
            case 8:
                gifRojo = "/streetFighterImagenes/mbisonAtaqueEspecialRojo.gif";
                time = 3600L;
                xRojo = 170;
                yRojo = 275;
                wRojo = 978;
                hRojo = 425;
                break;
            case 9:
                gifRojo = "/streetFighterImagenes/ryuAtaqueEspecialRojo.gif";
                time = 4300L;
                xRojo = 5;
                yRojo = 320;
                wRojo = 1109;
                hRojo = 400;
                break;
            case 10:
                gifRojo = "/streetFighterImagenes/zangiefAtaqueEspecialRojo.gif";
                time = 7920L;
                xRojo = 1;
                yRojo = 10;
                wRojo = 1230;
                hRojo = 710;
                break;

        }
        return gifRojo;

    }

    public String getGifAtaquePunyoRojo(int numRojo) {
        switch (numRojo) {

            case 1:
                gifRojo = "/streetFighterImagenes/akumaAtaquePunyoRojo.gif";
                time = 1260L;
                xRojo = 425;
                yRojo = 295;
                wRojo = 468;
                hRojo = 425;
                break;
            case 2:
                gifRojo = "/streetFighterImagenes/blankaAtaquePunyoRojo.gif";
                time = 800L;
                xRojo = 490;
                yRojo = 370;
                wRojo = 467;
                hRojo = 330;
                break;
            case 3:
                gifRojo = "/streetFighterImagenes/chunliAtaquePunyoRojo.gif";
                time = 1070L;
                xRojo = 205;
                yRojo = 360;
                wRojo = 832;
                hRojo = 340;
                break;
            case 4:
                gifRojo = "/streetFighterImagenes/dhalsimAtaquePunyoRojo.gif";
                time = 1700L;
                xRojo = 272;
                yRojo = 367;
                wRojo = 727;
                hRojo = 335;
                break;
            case 5:
                gifRojo = "/streetFighterImagenes/ehondaAtaquePunyoRojo.gif";
                time = 1850L;
                xRojo = 190;
                yRojo = 250;
                wRojo = 977;
                hRojo = 450;
                break;
            case 6:
                gifRojo = "/streetFighterImagenes/guileAtaquePunyoRojo.gif";
                time = 1900L;
                xRojo = 180;
                yRojo = 400;
                wRojo = 780;
                hRojo = 295;
                break;
            case 7:
                gifRojo = "/streetFighterImagenes/kenAtaquePunyoRojo.gif";
                time = 1800L;
                xRojo = 180;
                yRojo = 385;
                wRojo = 727;
                hRojo = 320;
                break;
            case 8:
                gifRojo = "/streetFighterImagenes/mbisonAtaquePunyoRojo.gif";
                time = 580L;
                xRojo = 315;
                yRojo = 275;
                wRojo = 787;
                hRojo = 425;
                break;
            case 9:
                gifRojo = "/streetFighterImagenes/ryuAtaquePunyoRojo.gif";
                time = 1040L;
                xRojo = 157;
                yRojo = 377;
                wRojo = 780;
                hRojo = 335;
                break;
            case 10:
                gifRojo = "/streetFighterImagenes/zangiefAtaquePunyoRojo.gif";
                time = 2280L;
                xRojo = 470;
                yRojo = 400;
                wRojo = 700;
                hRojo = 700;
                break;

        }
        return gifRojo;

    }

    public String getGifAtaquePatadaRojo(int numRojo) {
        switch (numRojo) {

            case 1:
                gifRojo = "/streetFighterImagenes/akumaAtaquePatadaRojo.gif";
                time = 900L;
                xRojo = 490;
                yRojo = 390;
                wRojo = 405;
                hRojo = 320;
                break;
            case 2:
                gifRojo = "/streetFighterImagenes/blankaAtaquePatadaRojo.gif";
                time = 2650L;
                xRojo = 210;
                yRojo = 135;
                wRojo = 868;
                hRojo = 575;
                break;
            case 3:
                gifRojo = "/streetFighterImagenes/chunliAtaquePatadaRojo.gif";
                time = 3180L;
                xRojo = 20;
                yRojo = 20;
                wRojo = 1120;
                hRojo = 710;
                break;
            case 4:
                gifRojo = "/streetFighterImagenes/dhalsimAtaquePatadaRojo.gif";
                time = 2400L;
                xRojo = 330;
                yRojo = 220;
                wRojo = 767;
                hRojo = 500;
                break;
            case 5:
                gifRojo = "/streetFighterImagenes/ehondaAtaquePatadaRojo.gif";
                time = 2400L;
                xRojo = 305;
                yRojo = 83;
                wRojo = 753;
                hRojo = 630;
                break;
            case 6:
                gifRojo = "/streetFighterImagenes/guileAtaquePatadaRojo.gif";
                time = 3020L;
                xRojo = 307;
                yRojo = 150;
                wRojo = 634;
                hRojo = 550;
                break;
            case 7:
                gifRojo = "/streetFighterImagenes/kenAtaquePatadaRojo.gif";
                time = 3150L;
                xRojo = 270;
                yRojo = 35;
                wRojo = 781;
                hRojo = 665;
                break;
            case 8:
                gifRojo = "/streetFighterImagenes/mbisonAtaquePatadaRojo.gif";
                time = 1080L;
                xRojo = 210;
                yRojo = 115;
                wRojo = 947;
                hRojo = 600;
                break;
            case 9:
                gifRojo = "/streetFighterImagenes/ryuAtaquePatadaRojo.gif";
                time = 4000L;
                xRojo = 155;
                yRojo = 165;
                wRojo = 798;
                hRojo = 555;
                break;
            case 10:
                gifRojo = "/streetFighterImagenes/zangiefAtaquePatadaRojo.gif";
                time = 3510L;
                xRojo = 500;
                yRojo = 310;
                wRojo = 564;
                hRojo = 400;
                break;

        }
        return gifRojo;

    }

    public static void rutaImagenGanador(int numGanador) {
        switch (numGanador) {

            case 1:
                jugadorGanador = "/streetFighterImagenes/akumaResult.jpg";
                xGanador = 800;
                yGanador = 290;
                break;
            case 2:
                jugadorGanador = "/streetFighterImagenes/blankaResult.jpg";
                xGanador = 800;
                yGanador = 290;
                break;
            case 3:
                jugadorGanador = "/streetFighterImagenes/chunliResult.jpg";
                xGanador = 800;
                yGanador = 290;
                break;
            case 4:
                jugadorGanador = "/streetFighterImagenes/dhalsimResult.jpg";
                xGanador = 800;
                yGanador = 290;
                break;
            case 5:
                jugadorGanador = "/streetFighterImagenes/ehondaResult.jpg";
                xGanador = 800;
                yGanador = 500;
                break;
            case 6:
                jugadorGanador = "/streetFighterImagenes/guileResult.jpg";
                xGanador = 210;
                yGanador = 290;
                break;
            case 7:
                jugadorGanador = "/streetFighterImagenes/kenResult.jpg";
                xGanador = 210;
                yGanador = 450;
                break;
            case 8:
                jugadorGanador = "/streetFighterImagenes/mbisonResult.jpg";
                xGanador = 210;
                yGanador = 290;
                break;
            case 9:
                jugadorGanador = "/streetFighterImagenes/ryuResult.jpg";
                xGanador = 800;
                yGanador = 290;
                break;
            case 10:
                jugadorGanador = "/streetFighterImagenes/zangiefResult.jpg";
                xGanador = 800;
                yGanador = 450;
                break;

        }
    }
}
