/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo;

/**
 *
 * @author verom
 */
public class Batallas {
    
    private String nomJ1;
    private String nomJ2;
    private String batalla;
    private String ganador;

    public Batallas(String nomJ1, String nomJ2, String batalla, String ganador) {
        this.nomJ1 = nomJ1;
        this.nomJ2 = nomJ2;
        this.batalla = batalla;
        this.ganador = ganador;
    }
    
    public Batallas(String nomJ1, String nomJ2, String ganador) {
        this.nomJ1 = nomJ1;
        this.nomJ2 = nomJ2;
        this.ganador = ganador;
    }

    public String getNomJ1() {
        return nomJ1;
    }

    public void setNomJ1(String nomJ1) {
        this.nomJ1 = nomJ1;
    }

    public String getNomJ2() {
        return nomJ2;
    }

    public void setNomJ2(String nomJ2) {
        this.nomJ2 = nomJ2;
    }

    public String getBatalla() {
        setBatalla(nomJ1, nomJ2);
        return batalla;
    }

    public void setBatalla(String nomJ1, String nomJ2) {
        this.batalla = nomJ1 + " VS " + nomJ2;
    }

    public String getGanador() {
        return ganador;
    }

    public void setGanador(String ganador) {
        this.ganador = ganador;
    }
    
    
    
    
    
}
