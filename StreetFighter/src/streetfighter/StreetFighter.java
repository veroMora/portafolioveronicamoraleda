
package streetfighter;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class StreetFighter extends Application {
    
    @Override
    public void start(Stage stageInicio) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/vistas/streetFighter.fxml"));
        
        Scene scene = new Scene(root);
        
        stageInicio.setScene(scene);
        stageInicio.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
    
}
