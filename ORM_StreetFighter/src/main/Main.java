package main;

import java.util.InputMismatchException;
import java.util.Scanner;
import org.hibernate.Session;
import controlador.AtaqueController;
import controlador.EscenarioController;
import controlador.PersonajeController;
import controlador.TorneoController;
import utilidades.Utilidades;

public class Main {

	private static Scanner t;

	public static void main(String[] args) {

		t = new Scanner(System.in);

		Session session = Utilidades.getSessionFactory().openSession();

		AtaqueController ac = new AtaqueController(session);
		EscenarioController ec = new EscenarioController(session);
		PersonajeController pc = new PersonajeController(session);
		TorneoController tc = new TorneoController(session);

		int numeroT = 0;
		int operacion = 0;
		String resp = null;

		do {

			numeroT = menu();
			menuTablaSeleccionada(numeroT);
			if (numeroT >= 1 && numeroT <= 4) {
				try {
					operacion = t.nextInt();
					switch (numeroT) {
					case 1:
						operacionSeleccionadaAtaque(operacion, ac);
						break;
					case 2:
						operacionSeleccionadaEscenario(operacion, ec);
						break;
					case 3:
						operacionSeleccionadaPersonaje(operacion, pc);
						break;
					case 4:
						operacionSeleccionadaTorneo(operacion, tc);
						break;
					}
				} catch (InputMismatchException ex) {
					System.err.println("Error porque se ha introducido una cadena de caracteres o un double.\n");
					t.next();
				}

			}
			System.out.println("Quieres seguir realizando operaciones?S/N");
			resp = t.next();

		} while (!resp.equalsIgnoreCase("N"));

		session.close();
	}

	private static int menu() {

		System.out.println("\nA que tabla quieres acceder?\n 1. Ataque\n 2. Escenario\n 3. Personaje\n 4. Torneo");
		int numTabla = 4;
		try {
			numTabla = t.nextInt();
			return numTabla;
		} catch (InputMismatchException ex) {
			System.err.println(
					"Error porque se ha introducido una cadena de caracteres o un double. Por defecto accederas a la tabla torneo.\n");
			t.next();
		}
		return numTabla;

	}

	private static void menuTablaSeleccionada(int numT) {

		String nomTabla = null;

		switch (numT) {
		case 1:
			nomTabla = "ATAQUE";
			break;
		case 2:
			nomTabla = "ESCENARIO";
			break;
		case 3:
			nomTabla = "PERSONAJE";
			break;
		case 4:
			nomTabla = "TORNEO";
			break;
		default:
			System.err.println("* * * NO EXISTE LA TABLA INTRODUCIDA * * *");
		}

		if (numT >= 1 && numT <= 4) {
			System.out.println("* * * MENU DE LA TABLA " + nomTabla + " * * *\n");
			System.out.println("Que operacion quieres realizar en la tabla " + nomTabla.toLowerCase()
					+ "?\n 1. Obtener una fila\n 2. Crear una fila nueva\n 3. Eliminar una fila\n"
					+ " 4. Actualizar un campo de una fila\n 5. Realizar una consulta\n 6. Salir");
		} else
			System.err.println(
					"Si quieres realizar modificaciones o consultas en las tablas INTRODUCE UN NUMERO QUE ESTE ENTRE EL RANGO 1-4\n");

	}

	private static void operacionSeleccionadaAtaque(int op, AtaqueController ac) {

		int id = 0;

		switch (op) {
		case 1:
			System.out.println("Introduce el ID del ataque para obtener la fila deseada");
			id = t.nextInt();
			ac.obtenerByID(id);
			break;
		case 2:
			ac.crear();
			break;
		case 3:
			System.out.println("Introduce el ID del ataque para eliminar la fila deseada");
			id = t.nextInt();
			ac.eliminar(id);
			break;
		case 4:
			System.out.println("Introduce el ID del ataque para actualizar la fila deseada");
			id = t.nextInt();
			ac.update(id);
			break;
		case 5:
			ac.consultarCantidadPersonajesAtaque();
			break;
		case 6:
			System.out.println("* * * NO HAS REALIZADO NINGUNA OPERACION * * *\n");
			break;
		default: 
			System.err.println("* * * NO EXISTE LA OPERACION INTRODUCIDA * * *");
		}

	}

	private static void operacionSeleccionadaEscenario(int op, EscenarioController ec) {

		int id = 0;

		switch (op) {
		case 1:
			System.out.println("Introduce el ID del escenario para obtener la fila deseada");
			id = t.nextInt();
			ec.obtenerByID(id);
			break;
		case 2:
			ec.crear();
			break;
		case 3:
			System.out.println("Introduce el ID del escenario para eliminar la fila deseada");
			id = t.nextInt();
			ec.eliminar(id);
			break;
		case 4:
			System.out.println("Introduce el ID del escenario para actualizar la fila deseada");
			id = t.nextInt();
			ec.update(id);
			break;
		case 5:
			ec.consultarNumeroEscenarios();
			break;
		case 6:
			System.out.println("* * * NO HAS REALIZADO NINGUNA OPERACION * * *\n");
			break;
		default: 
			System.err.println("* * * NO EXISTE LA OPERACION INTRODUCIDA * * *\n");
		}

	}

	private static void operacionSeleccionadaPersonaje(int op, PersonajeController pc) {

		String nomP = null;

		switch (op) {
		case 1:
			System.out.println("Introduce el nombre del personaje para obtener la fila deseada");
			nomP = t.next();
			pc.obtenerPersonaje(nomP);
			break;
		case 2:
			pc.crear();
			break;
		case 3:
			System.out.println("Introduce el nombre del personaje para eliminar la fila deseada");
			nomP = t.next();
			pc.eliminar(nomP);
			break;
		case 4:
			System.out.println("Introduce el nombre del personaje para actualizar la fila deseada");
			nomP = t.next();
			pc.update(nomP);
			break;
		case 5:
			pc.consultarListadoPersonajes();
			break;
		case 6:
			System.out.println("* * * NO HAS REALIZADO NINGUNA OPERACION * * *\n");
			break;
		default: 
			System.err.println("* * * NO EXISTE LA OPERACION INTRODUCIDA * * *\n");
		}

	}

	private static void operacionSeleccionadaTorneo(int op, TorneoController tc) {

		int id = 0;

		switch (op) {
		case 1:
			System.out.println("Introduce el ID del torneo para obtener la fila deseada");
			id = t.nextInt();
			tc.obtenerByID(id);
			break;
		case 2:
			tc.crear();
			break;
		case 3:
			System.out.println("Introduce el ID del torneo para eliminar la fila deseada");
			id = t.nextInt();
			tc.eliminar(id);
			break;
		case 4:
			System.out.println("Introduce el ID del torneo para actualizar la fila deseada");
			id = t.nextInt();
			tc.update(id);
			break;
		case 5:
			tc.consultarCantidadPersonajesTorneo();
			break;
		case 6:
			System.out.println("* * * NO HAS REALIZADO NINGUNA OPERACION * * *\n");
			break;
		default: 
			System.err.println("* * * NO EXISTE LA OPERACION INTRODUCIDA * * *\n");
		}

	}

	/*
	 * private static String primeraLetraMayus(String nomP, String primerLetra,
	 * String restoLetra) { nomP = t.next().toLowerCase(); primerLetra =
	 * nomP.substring(0, 1).toUpperCase(); restoLetra = nomP.substring(1,
	 * nomP.length()); nomP = primerLetra + restoLetra;
	 * 
	 * return nomP; }
	 */

}
