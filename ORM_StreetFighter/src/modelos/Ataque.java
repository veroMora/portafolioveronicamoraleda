package modelos;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name = "ataque")
public class Ataque {
	
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idAtaque")
	private int idAtaque;
	
	@Column(name = "dagno")
	private int dagno;
	
	@Column(name = "nombreAtaque")
	private String nombreAtaque;
	
	@ManyToMany
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JoinTable(name = "ataque_personaje",
	joinColumns = {@JoinColumn (name = "idAtaque")}, inverseJoinColumns = {@JoinColumn (name = "nomPersonaje")})
	private List <Personaje> personajes;
	
	public Ataque() {
		
	}

	public int getIdAtaque() {
		return idAtaque;
	}

	public void setIdAtaque(int idAtaque) {
		this.idAtaque = idAtaque;
	}

	public int getDagno() {
		return dagno;
	}

	public void setDagno(int dagno) {
		this.dagno = dagno;
	}

	public String getNombreAtaque() {
		return nombreAtaque;
	}

	public void setNombreAtaque(String nombreAtaque) {
		this.nombreAtaque = nombreAtaque;
	}

	public List<Personaje> getPersonajes() {
		return personajes;
	}

	public void setPersonajes(List<Personaje> personajes) {
		this.personajes = personajes;
	}

	@Override
	public String toString() {
		return "Ataque [idAtaque=" + idAtaque + ", dagno=" + dagno + ", nombreAtaque=" + nombreAtaque + "]";
	}
	
	

}
