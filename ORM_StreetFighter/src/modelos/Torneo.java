package modelos;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "torneo")
public class Torneo {

	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idTorneo")
	private int idTorneo;
	
	@Column(name = "nomTorneo")
	private String nomTorneo;
	
	@OneToMany(mappedBy = "torneo")
	//@OnDelete(action = OnDeleteAction.CASCADE)
	private List<Personaje> personajes;
	
	public Torneo() {
		
	}

	public int getIdTorneo() {
		return idTorneo;
	}

	public void setIdTorneo(int idTorneo) {
		this.idTorneo = idTorneo;
	}

	public String getNomTorneo() {
		return nomTorneo;
	}

	public void setNomTorneo(String nomTorneo) {
		this.nomTorneo = nomTorneo;
	}

	@Override
	public String toString() {
		return "Torneo [idTorneo=" + idTorneo + ", nomTorneo=" + nomTorneo + "]";
	}
	
	
	
}
