package modelos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name = "escenario")
public class Escenario {

	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY) // Para crear valores de forma automatica incrementandose (auto-incremento)
	@Column(name = "idEscenario")
	private int idEscenario;
	
	@Column(name = "nomEscenario")
	private String nomEscenario;
	
	@Column(name = "paisUbicado")
	private String paisUbicado;
	
	@OneToOne
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JoinColumn(name = "nomPersonaje") // Para hacer la foreign key. nomPersonaje es el nombre de la foreign key
	private Personaje personaje;
	
	public Escenario() {
		
		
	}

	public int getIdEscenario() {
		return idEscenario;
	}

	public void setIdEscenario(int idEscenario) {
		this.idEscenario = idEscenario;
	}

	public String getNomEscenario() {
		return nomEscenario;
	}

	public void setNomEscenario(String nomEscenario) {
		this.nomEscenario = nomEscenario;
	}

	public String getPaisUbicado() {
		return paisUbicado;
	}

	public void setPaisUbicado(String paisUbicado) {
		this.paisUbicado = paisUbicado;
	}

	public Personaje getPersonaje() {
		return personaje;
	}

	public void setPersonaje(Personaje personaje) {
		this.personaje = personaje;
	}

	@Override
	public String toString() {
		return "Escenario [idEscenario=" + idEscenario + ", nomEscenario=" + nomEscenario + ", paisUbicado="
				+ paisUbicado + ", nomPersonaje=" + personaje.getNomPersonaje() + "]";
	}
	
	

	
	
	
}
