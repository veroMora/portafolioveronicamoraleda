package modelos;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name = "personaje")
public class Personaje {
	
	@Id 
	@Column(name = "nomPersonaje", length = 20)
	private String nomPersonaje;
	
	@Column(name = "ocupacion")
	private String ocupacion;
	
	@Column(name = "estiloLucha")
	private String estiloLucha;
	
	@Column(name = "habilidad")
	private String habilidad;
	
	@ManyToMany(mappedBy = "personajes")
	@OnDelete(action = OnDeleteAction.CASCADE)
	private List <Ataque> ataques;
	
	@OneToOne(mappedBy = "personaje")
	private Escenario escenario;
	
	@ManyToOne
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JoinColumn(name = "idTorneo") // Que columna en la tabla Personaje tiene la FK
	private Torneo torneo;
	
	public Personaje() {
		
	}

	public String getNomPersonaje() {
		return nomPersonaje;
	}

	public void setNomPersonaje(String nomPersonaje) {
		this.nomPersonaje = nomPersonaje;
	}

	public String getOcupacion() {
		return ocupacion;
	}

	public void setOcupacion(String ocupacion) {
		this.ocupacion = ocupacion;
	}

	public String getEstiloLucha() {
		return estiloLucha;
	}

	public void setEstiloLucha(String estiloLucha) {
		this.estiloLucha = estiloLucha;
	}

	public String getHabilidad() {
		return habilidad;
	}

	public void setHabilidad(String habilidad) {
		this.habilidad = habilidad;
	}

	public Torneo getTorneo() {
		return torneo;
	}

	public void setTorneo(Torneo torneo) {
		this.torneo = torneo;
	}

	public List<Ataque> getAtaques() {
		return ataques;
	}

	public void setAtaques(List<Ataque> ataques) {
		this.ataques = ataques;
	}

	public Escenario getEscenario() {
		return escenario;
	}

	public void setEscenario(Escenario escenario) {
		this.escenario = escenario;
	}

	
	@Override
	public String toString() {
		return "Personaje [nomPersonaje=" + nomPersonaje + ", ocupacion=" + ocupacion + ", estiloLucha=" + estiloLucha
				+ ", habilidad=" + habilidad + ", idTorneo=" + torneo.getIdTorneo() + "]";
	}
	
	
}
