package controlador;

import java.util.List;
import java.util.Scanner;
import org.hibernate.Session;
import org.hibernate.Transaction;
import dao.Repository;
import dao.TorneoDAO;
import modelos.Torneo;

public class TorneoController implements Repository<Torneo>, TorneoDAO {

	Scanner td = new Scanner(System.in);

	Session session = null;

	public TorneoController(Session session) {
		super();
		this.session = session;
	}

	@Override
	public void obtenerByID(int id) {

		Torneo t = new Torneo();
		Transaction transaction = null;

		try {
			
			transaction = session.beginTransaction();

			t = session.load(Torneo.class, id);
			
			String str = t.toString();
			
			session.getTransaction().commit();

			System.out.println("* * * ID del torneo buscado con exito * * *\n");

			System.out.println(str + "\n");

		} catch (Exception ex) {
			System.err.println("* * * ID del torneo no encontrado * * *\n");
			
			if (transaction != null) {
				transaction.rollback();
			}
		}

	}

	@Override
	public void crear() {

		Torneo t = new Torneo();

		Transaction transaction = null;
		transaction = session.beginTransaction();

		System.out.println("Introduce el nombre del torneo");
		t.setNomTorneo(td.next());

		session.save(t);

		session.getTransaction().commit();

		System.out.println("* * * Fila creada con exito * * *\n");

	}

	@Override
	public void eliminar(int id) {

		Torneo t = new Torneo();
		Transaction transaction = null;

		try {

			transaction = session.beginTransaction();

			t = session.load(Torneo.class, id);

			session.delete(t);

			System.out.println("* * * ID del torneo buscado con exito * * *");

			session.getTransaction().commit();

			System.out.println("* * * Fila eliminada con exito * * *\n");

		} catch (Exception ex) {
			System.err.println("* * * ID del torneo no encontrado * * *\n");
			
			if (transaction != null) {
				transaction.rollback();
			}
		}

	}

	@Override
	public void update(int id) {

		Torneo t = new Torneo();
		Transaction transaction = null;

		try {
			
			transaction = session.beginTransaction();

			t = session.get(Torneo.class, id);

			if (t != null) {
				System.out.println("Actualiza el nombre del torneo");
				t.setNomTorneo(td.next());
			}

			session.update(t);

			System.out.println("* * * ID del torneo buscado con exito * * *");

			session.getTransaction().commit();

			System.out.println("* * * Fila actualizada con exito * * *\n");

		} catch (IllegalArgumentException ex) {
			System.err.println("* * * ID del torneo no encontrado * * *\n");
			
			if (transaction != null) {
				transaction.rollback();
			}
		}

	}

	@Override
	public void consultarCantidadPersonajesTorneo() {

		System.out.println("Obten la cantidad de personajes que hay por torneo.\n");

		List<Object[]> torneos = session.createNativeQuery(
				"SELECT nomTorneo, COUNT(nomPersonaje)" + " FROM TORNEO NATURAL JOIN PERSONAJE GROUP BY idTorneo")
				.list();

		System.out.println("* * * CANTIDAD DE PERSONAJES POR TORNEO * * *\n");
		for (Object[] torneo : torneos) {
			String nombreT = (String) torneo[0];
			System.out.println("Nombre del torneo: " + nombreT + "; Cantidad de personajes: " + torneo[1] + "\n");
		}
		if (torneos.isEmpty()) {
			System.err.println("* * * NO SE HA OBTENIDO NINGUN RESULTADO * * *\n");
		}
		System.out.println("* * * * * * * * * * * * * * * * * * * * * * * * * *\n");

	}
}
