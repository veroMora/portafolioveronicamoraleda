package controlador;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

import org.hibernate.Session;
import org.hibernate.Transaction;

import dao.EscenarioDAO;
import dao.Repository;
import modelos.Ataque;
import modelos.Escenario;
import modelos.Personaje;

public class EscenarioController implements Repository<Escenario>, EscenarioDAO {

	Scanner t = new Scanner(System.in);

	Session session = null;

	public EscenarioController(Session session) {
		super();
		this.session = session;
	}

	@Override
	public void obtenerByID(int id) {

		Escenario e = new Escenario();
		Transaction transaction = null;

		try {
			
			transaction = session.beginTransaction();

			e = session.load(Escenario.class, id);
			
			String str = e.toString();
			
			session.getTransaction().commit();
			
			System.out.println("* * * ID del escenario buscado con exito * * *\n");
			
			System.out.println(str + "\n");

		} catch (Exception ex) {
			System.err.println("* * * ID del escenario no encontrado * * *\n");
			
			if (transaction != null) {
				transaction.rollback();
			}
		}

	}

	@Override
	public void crear() {

		Escenario e = new Escenario();
		Transaction transaction = null;

		try {
			
			transaction = session.beginTransaction();

			System.out.println("Introduce el nombre del escenario");
			e.setNomEscenario(t.next());

			System.out.println("Introduce el pais donde se encuentra el escenario");
			e.setPaisUbicado(t.next());

			Personaje p = new Personaje();

			System.out.println("Introduce el nombre del personaje al que pertenece el escenario");
			String nomP = t.next();

			p = session.load(Personaje.class, nomP);
			
			e.setPersonaje(p);

			session.save(e);

			session.getTransaction().commit();

			System.out.println("* * * Fila creada con exito * * *\n");

		} catch (Exception ex) {
			System.err.println("* * * El escenario no ha podido ser creado. El personaje no existe * * *\n");
			
			if (transaction != null) {
				transaction.rollback();
			}
		}

	}

	@Override
	public void eliminar(int id) {

		Escenario e = new Escenario();
		Transaction transaction = null;

		try {
			
			transaction = session.beginTransaction();

			e = session.load(Escenario.class, id);

			session.delete(e);
			
			System.out.println("* * * ID del escenario buscado con exito * * *");

			session.getTransaction().commit();

			System.out.println("* * * Fila eliminada con exito * * *\n");

		} catch (Exception ex) {
			System.err.println("* * * ID del escenario no encontrado * * *\n");
			
			if (transaction != null) {
				transaction.rollback();
			}
		}

	}

	@Override
	public void update(int id) {

		Escenario e = new Escenario();
		Transaction transaction = null;

		try {
			
			transaction = session.beginTransaction();

			e = session.load(Escenario.class, id);
			
			if (e != null) {
				System.out.println("Actualiza el nombre del escenario");
				e.setNomEscenario(t.next());
				
				System.out.println("Actualiza el pais donde esta situado el escenario");
				e.setPaisUbicado(t.next());
			}
						
			session.update(e);
			
			System.out.println("* * *ID del escenario buscado con exito * * *");

			session.getTransaction().commit();

			System.out.println("* * * Fila actualizada con exito * * *\n");

		} catch (Exception ex) {
			System.err.println("* * * ID del escenario no encontrado * * *\n");
			
			if (transaction != null) {
				transaction.rollback();
			}
		}

	}

	@Override
	public void consultarNumeroEscenarios() {
		
		System.out.println("Introduce el numero de escenarios que quieres obtener, ordenados por el pais (DESCENDIENTE).");
		int limite = 1;
		try {
			limite = t.nextInt();
		} catch (InputMismatchException ex) {
			System.err.println(
					"Error porque se ha introducido una cadena de caracteres o un double. Por defecto se establecera el numero 1.\n");
			t.next();
		}
		
		List<Object[]> escenarios = session.createNativeQuery("SELECT CONCAT(nomEscenario,'(',UPPER(paisUbicado),')'), nomPersonaje"
				+ " FROM ESCENARIO ORDER BY paisUbicado DESC, nomPersonaje LIMIT ?").setParameter(1, limite).list();

		System.out.println("* * * LISTADO DE LOS ESCENARIOS Y LOS PROPIETARIOS DE LOS MISMOS * * *\n");
		for (Object[] escenario : escenarios) {
			String nombreE = (String) escenario[0];
			String nombreP = (String) escenario[1];
			System.out.println("Nombre y pais del escenario: " + nombreE + "; Propietario del escenario: "
					+ nombreP + "\n");
		}
		if (escenarios.isEmpty()) {
			System.err.println("* * * NO SE HA OBTENIDO NINGUN RESULTADO * * *\n");
		}
		System.out.println("* * * * * * * * * * * * * * * * * * * * * * * * * *\n");
	}
		
}
