package controlador;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

import org.hibernate.Session;
import org.hibernate.Transaction;

import dao.AtaqueDAO;
import dao.Repository;
import modelos.Ataque;
import modelos.Personaje;

public class AtaqueController implements Repository<Ataque>, AtaqueDAO {

	Scanner t = new Scanner(System.in);

	Session session = null;

	public AtaqueController(Session session) {
		super();
		this.session = session;
	}

	@Override
	public void obtenerByID(int id) {

		Ataque a = new Ataque();
		Transaction transaction = null;

		try {
			
			transaction = session.beginTransaction();

			a = session.load(Ataque.class, id);
			
			String str = a.toString();

			session.getTransaction().commit();
			
			System.out.println("* * * ID del ataque buscado con exito * * *\n");
			
			System.out.println(str + "\n");

		} catch (Exception ex) {
			System.err.println("* * * ID del ataque no encontrado * * *\n");
			
			if (transaction != null) {
				transaction.rollback();
			}
		}

	}

	@Override
	public void crear() {

		Ataque a = new Ataque();
		Personaje p = new Personaje();
		List<Personaje> personajes = new ArrayList<Personaje>();
		Transaction transaction = null;

		try {
			
			transaction = session.beginTransaction();

			System.out.println("Introduce el daño del ataque");
			a.setDagno(t.nextInt());

			System.out.println("Introduce el nombre del ataque");
			a.setNombreAtaque(t.next());

			String resp = null;

			do {
				System.out.println("Introduce el nombre de un personaje que tenga este ataque");
				String nomPer = t.next();

				p = session.load(Personaje.class, nomPer);
				personajes.add(p);

				System.out.println("Hay mas personajes con este ataque?S/N");
				resp = t.next();
			} while (!resp.equalsIgnoreCase("N"));

			a.setPersonajes(personajes);

			session.save(a);

			session.getTransaction().commit();

			System.out.println("* * *Fila creada con exito * * *\n");

		} catch (InputMismatchException ex) {
			System.err.println("Error porque se ha introducido una cadena de caracteres o un double en el daño del ataque.\n");
			t.next();
			
			if (transaction != null) {
				transaction.rollback();
			}

		} catch (Exception ex) {
			System.err.println("* * * El ataque no ha podido ser creado. El personaje no existe * * *\n");
			
			if (transaction != null) {
				transaction.rollback();
			}
		}

	}

	@Override
	public void eliminar(int id) {

		Ataque a = new Ataque();
		Transaction transaction = null;

		try {
			
			transaction = session.beginTransaction();

			a = session.load(Ataque.class, id);

			session.delete(a);
			
			System.out.println("* * * ID del ataque buscado con exito * * *");

			session.getTransaction().commit();

			System.out.println("* * * Fila eliminada con exito * * *\n");

		} catch (Exception ex) {
			System.err.println("* * * ID del ataque no encontrado * * *\n");
			
			if (transaction != null) {
				transaction.rollback();
			}
		}

	}

	@Override
	public void update(int id) {

		Ataque a = new Ataque();
		Transaction transaction = null;

		try {
			
			transaction = session.beginTransaction();

			a = session.load(Ataque.class, id);
			
			if (a != null) {
				System.out.println("Actualiza el daño del ataque");
				a.setDagno(t.nextInt());

				System.out.println("Actualiza el nombre del ataque");
				a.setNombreAtaque(t.next());
			}
			
			session.update(a);
			
			System.out.println("* * * ID del ataque buscado con exito * * *");

			session.getTransaction().commit();

			System.out.println("* * * Fila actualizada con exito * * *\n");

		} catch (Exception ex) {
			System.err.println("* * * ID del ataque no encontrado * * *\n");
			
			if (transaction != null) {
				transaction.rollback();
			}
		}

	}

	@Override
	public void consultarCantidadPersonajesAtaque() {

		System.out.println("Introduce el numero de la consulta que quieres realizar");
		System.out.println(" 1. Obtener la cantidad de personajes, para cada ataque.");
		System.out.println(" 2. Obtener la cantidad de personajes, de un ataque en concreto.");

		int numCon;
		try {
			numCon = t.nextInt();
			while (numCon > 2 || numCon <= 0) {
				System.err.println("La operacion introducida no existe. Introduce un numero entre 1-2.");
				numCon = t.nextInt();
			}

		} catch (InputMismatchException ex) {
			System.err.println(
					"Error porque se ha introducido una cadena de caracteres o un double. Por defecto se elegira la primera consulta.\n");
			numCon = 1;
			t.next();
		}

		if (numCon == 1) {

			List<Object[]> ataques = session.createNativeQuery("SELECT nombreAtaque, COUNT(nomPersonaje)"
					+ " FROM ATAQUE NATURAL JOIN ATAQUE_PERSONAJE GROUP BY idAtaque").list();

			System.out.println("* * * CANTIDAD DE PERSONAJES PARA CADA ATAQUE * * *\n");
			for (Object[] ataque : ataques) {
				String nombreA = (String) ataque[0];
				System.out.println("Nombre del ataque: " + nombreA + "; Cantidad de personajes con el ataque: "
						+ ataque[1] + "\n");
			}
			if (ataques.isEmpty()) {
				System.err.println("* * * NO SE HA OBTENIDO NINGUN RESULTADO * * *\n");
			}
			System.out.println("* * * * * * * * * * * * * * * * * * * * * * * * * *\n");

		} else {

			System.out.println("Selecciona el ataque concreto introduciendo su id");
			int idAtaque = 1;
			try {
				idAtaque = t.nextInt();
			} catch (InputMismatchException ex) {
				System.err.println(
						"Error porque se ha introducido una cadena de caracteres o un double. Por defecto se quedara el id del ataque en 1.\n");
				t.next();
			}

			System.out.println("* * * CANTIDAD DE PERSONAJES DE UN ATAQUE EN CONCRETO * * *\n");
			Object[] ataque = (Object[]) session
					.createNativeQuery("SELECT nombreAtaque, COUNT(nomPersonaje)"
							+ " FROM ATAQUE NATURAL JOIN ATAQUE_PERSONAJE WHERE idAtaque = ? GROUP BY idAtaque")
					.setParameter(1, idAtaque).uniqueResult();

			try {
				String nombreA = (String) ataque[0];
				System.out.println("Nombre del ataque: " + nombreA + "; Cantidad de personajes con el ataque: "
						+ ataque[1] + "\n");
			} catch (NullPointerException ex) {
				System.err.println("No se pueden cargar los datos porque el idAtaque es null (NO EXISTE)\n");
			}
			System.out.println("* * * * * * * * * * * * * * * * * * * * * * * * * *\n");

		}

	}
}
