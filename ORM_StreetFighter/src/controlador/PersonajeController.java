package controlador;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

import org.hibernate.Session;
import org.hibernate.Transaction;

import dao.PersonajeDAO;
import modelos.Ataque;
import modelos.Escenario;
import modelos.Personaje;
import modelos.Torneo;

public class PersonajeController implements PersonajeDAO {

	Scanner t = new Scanner(System.in);

	Session session = null;

	public PersonajeController(Session session) {
		super();
		this.session = session;
	}

	@Override
	public void obtenerPersonaje(String nomPersonaje) {

		Personaje p = new Personaje();
		Transaction transaction = null;

		try {
			
			transaction = session.beginTransaction();

			p = session.load(Personaje.class, nomPersonaje);
			
			String str = p.toString();

			session.getTransaction().commit();
			
			System.out.println("* * * Nombre del personaje buscado con exito * * *\n");
			
			System.out.println(str + "\n");

		} catch (Exception ex) {
			System.err.println("* * * Nombre del personaje no encontrado * * *\n");
			
			if (transaction != null) {
				transaction.rollback();
			}
		}

	}

	@Override
	public void crear() {

		Personaje p = new Personaje();
		Torneo tor = new Torneo();
		Escenario e = new Escenario();
		Transaction transaction = null;
		
		try {
			
			transaction = session.beginTransaction();

			System.out.println("Introduce el nombre del personaje");
			String nombrePer = t.next();

			p.setNomPersonaje(nombrePer);

			System.out.println("Introduce la ocupacion de " + nombrePer.toUpperCase());
			p.setOcupacion(t.next());

			System.out.println("Introduce el estilo de lucha de " + nombrePer.toUpperCase());
			p.setEstiloLucha(t.next());

			System.out.println("Introduce la habilidad de " + nombrePer.toUpperCase());
			p.setHabilidad(t.next());

			System.out.println("Introduce el id del torneo en el que participa " + nombrePer.toUpperCase());
			int idTorneo = t.nextInt();
			
			tor = session.load(Torneo.class, idTorneo);
			
			p.setTorneo(tor);
			
			session.save(p);

			session.getTransaction().commit();

			System.out.println("* * * Fila creada con exito * * *\n");
			
		} catch (InputMismatchException ex) {
			System.err.println("Error porque se ha introducido una cadena de caracteres o un double en el id del torneo.\n");
			t.next();
			
			if (transaction != null) {
				transaction.rollback();
			}
			
		} catch (Exception ex) {
			System.err.println("* * * El personaje no ha podido ser creado. El torneo no existe * * *\n");
			
			if (transaction != null) {
				transaction.rollback();
			}
		}

	}

	@Override
	public void eliminar(String nomPersonaje) {

		Personaje p = new Personaje();
		Transaction transaction = null;

		try {
			
			transaction = session.beginTransaction();

			p = session.load(Personaje.class, nomPersonaje);
			
			session.delete(p);
			
			System.out.println("* * * Nombre del personaje buscado con exito * * *");

			session.getTransaction().commit();

			System.out.println("* * * Fila eliminada con exito * * *\n");

		} catch (Exception ex) {
			System.err.println("* * * Nombre del personaje no encontrado * * *\n");
			
			if (transaction != null) {
				transaction.rollback();
			}
		}

	}

	@Override
	public void update(String nomPersonaje) {

		Personaje p = new Personaje();
		Transaction transaction = null;

		try {
			
			transaction = session.beginTransaction();

			p = session.load(Personaje.class, nomPersonaje);

			if (p != null) {
				System.out.println("Actualiza la ocupacion de " + nomPersonaje);
				p.setOcupacion(t.next());

				System.out.println("Actualiza el estilo de lucha de " + nomPersonaje);
				p.setEstiloLucha(t.next());

				System.out.println("Actualiza la habilidad de " + nomPersonaje);
				p.setHabilidad(t.next());
			}
			
			session.update(p);
			
			System.out.println("* * *Nombre del personaje buscado con exito * * *");

			session.getTransaction().commit();
			
			System.out.println("* * * Fila actualizada con exito * * *\n");

		} catch (Exception ex) {
			System.err.println("* * * Nombre del personaje no encontrado * * *\n");
			
			if (transaction != null) {
				transaction.rollback();
			}
		}

	}

	@Override
	public void consultarListadoPersonajes() {
		
		System.out.println("Introduce el numero de la consulta que quieres realizar");
		System.out.println(" 1. Obtener un listado de los personajes donde se muestra la media del daño de sus ataques, y la fuerza de los mismos.");
		System.out.println(" 2. Obtener un listado de los personajes que empiezan por vocal.");

		int numCon;
		try {
			numCon = t.nextInt();
			while (numCon > 2 || numCon <= 0) {
				System.err.println("La operacion introducida no existe. Introduce un numero entre 1-2.");
				numCon = t.nextInt();
			}

		} catch (InputMismatchException ex) {
			System.err.println(
					"Error porque se ha introducido una cadena de caracteres o un double. Por defecto se elegira la primera consulta.\n");
			numCon = 1;
			t.next();
		}

		if (numCon == 1) {

			List<Object[]> personajes = session.createNativeQuery("SELECT personaje.nomPersonaje, AVG(dagno), IF (AVG(dagno) < 30, 'Reducida', 'Elevada')"
					+ " FROM PERSONAJE NATURAL JOIN ATAQUE_PERSONAJE NATURAL JOIN ATAQUE"
					+ " GROUP BY personaje.nomPersonaje ORDER BY personaje.nomPersonaje, AVG(dagno) DESC").list();

			System.out.println("* * * LISTADO DE LOS PERSONAJES, MEDIA DEL DAÑO DEL ATAQUE Y LA FUERZA * * *\n");
			for (Object[] personaje : personajes) {
				String nombreP = (String) personaje[0];
				String fuerza = (String) personaje[2];
				System.out.println("Nombre del personaje: " + nombreP + "; Media del daño de sus ataques: "
						+ personaje[1] + "; Fuerza: " + fuerza + "\n");
			}
			if (personajes.isEmpty()) {
				System.err.println("* * * NO SE HA OBTENIDO NINGUN RESULTADO * * *\n");
			}
			System.out.println("* * * * * * * * * * * * * * * * * * * * * * * * * *\n");

		} else {

			System.out.println("* * * LISTADO DE LOS PERSONAJES CUYOS NOMBRES EMPIEZAN POR VOCAL * * *\n");
			List<Personaje> personajes = session.createNativeQuery("SELECT *"
					+ " FROM PERSONAJE NATURAL JOIN TORNEO WHERE LEFT(nomPersonaje,1) IN ('A','E','I','O','U')", Personaje.class).list();

			for (Personaje personaje : personajes) {
				String nomP = personaje.getNomPersonaje();
				String estiloL = personaje.getEstiloLucha();
				String hab = personaje.getHabilidad();
				String ocup = personaje.getOcupacion();
				String nomT = personaje.getTorneo().getNomTorneo();

				System.out.println("Nombre del personaje: " + nomP + "; Estilo de lucha: "
						+ estiloL + "; Habilidad: " + hab + "; Ocupacion: " + ocup + "; Nombre del torneo: " + nomT + "\n");
			}
			if (personajes.isEmpty()) {
				System.err.println("* * * NO SE HA OBTENIDO NINGUN RESULTADO * * *\n");
			}
			System.out.println("* * * * * * * * * * * * * * * * * * * * * * * * * *\n");

		}
		
	}
}
