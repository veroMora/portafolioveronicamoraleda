package dao;

public interface Repository <T>{
	
	public void obtenerByID(int id);
	public void crear();
	public void eliminar(int id);
	public void update(int id);
	
}
