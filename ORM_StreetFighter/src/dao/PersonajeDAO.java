package dao;

import modelos.Personaje;

public interface PersonajeDAO {
	
	public void obtenerPersonaje(String nomPersonaje);
	public void crear();
	public void eliminar(String nomPersonaje);
	public void update(String nomPersonaje);
	public void consultarListadoPersonajes();
	
}
