const mongoose = require('mongoose');

const Schema = mongoose.Schema

const RankingSchema = new Schema({

    nombre: String,
    nickname: String,
    correo: String,
    telefono: String,
    password: String,
    compassword: String,
    record: String,
    timep: String,
    tiempoweb: Number,
    recordweb: Number
})

module.exports = mongoose.model('usuarios',RankingSchema);