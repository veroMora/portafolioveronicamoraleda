var express = require('express');
var router = express.Router();
var RankingController = require('../controllers/RankingController')

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('Esta es la página del ranking');
});

router.get("/verRanking",RankingController.verRanking);
router.get("/usuarios",RankingController.usuarios);
router.get("/buscarUsuario/:nickname/:password",RankingController.buscarUsuario);

module.exports = router;
