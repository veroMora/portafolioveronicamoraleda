const Ranking = require("../models/Ranking")

exports.verRanking = (req, res) => {
    Ranking.find({record:{$ne: '0:0:0'}}).sort({recordweb: 1, _id: 1}).limit(10).then(ranking => {
        res.json({ranking})
    })
}

exports.usuarios = (req, res) => {
    Ranking.find().then((ranking) => {
        res.json({ranking})
    }).catch(() => res.json({error:"Los usuarios no han podido ser encontrados"}))
}

exports.buscarUsuario = (req, res) => {
    Ranking.findOne(req.params)
    .then((ranking) => {
        res.json({ranking})
        console.log("Usuario encontrada con éxito")
    }).catch(() => res.json({error:"El usuario no ha podido ser encontrado"}))
}