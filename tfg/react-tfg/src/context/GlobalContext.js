import React, {useState, createContext} from 'react';
import EnglishMessages from '../language/en-UK.json';
import SpanishMessages from '../language/es-ES.json';
import {IntlProvider} from 'react-intl';

export const GlobalContext = createContext();

const GlobalProvider = ({children}) => {
	let localeByDefault;
	let messagesByDefault;
	const lang = localStorage.getItem('lang');

	if(lang){
		localeByDefault = lang

		if(lang === 'es-ES'){
			messagesByDefault = SpanishMessages;
		} else if(lang === 'en-UK'){
			messagesByDefault = EnglishMessages;
		} else {
			localeByDefault = 'es-ES'
			messagesByDefault = SpanishMessages
		}
	}

	const [messages, setMessages] = useState(messagesByDefault);
	const [locale, setLocale] = useState(localeByDefault);

	const changeLanguage = (language) => {
		switch (language){
			case 'es-ES':
				setMessages(SpanishMessages);
				setLocale('es-ES');
				localStorage.setItem('lang', 'es-ES');
				break;
			case 'en-UK':
				setMessages(EnglishMessages);
				setLocale('en-UK');
				localStorage.setItem('lang', 'en-UK');
				break;
			default:
				setMessages(SpanishMessages);
				setLocale('es-ES');
				localStorage.setItem('lang', 'es-ES');
		}
	}

	let userByDefault;
	const username = localStorage.getItem('username')
	let passwordByDefault;
	const password = localStorage.getItem('password')

	if(username && password){
		userByDefault = username;
		passwordByDefault = password;
	}

	const [userAuth, setUserAuth] = useState(userByDefault);
    const [passwordAuth, setPasswordAuth] = useState(passwordByDefault);

	const changeLogin = (user, password) => {
        setUserAuth(user);
        setPasswordAuth(password);
		localStorage.setItem('username', user);
		localStorage.setItem('password', password);
    }

	return (
		<GlobalContext.Provider value={{changeLanguage, changeLogin, lang, userAuth, passwordAuth}}>
			<IntlProvider locale={locale} messages={messages}>
				{children}
			</IntlProvider>
		</GlobalContext.Provider>
	);
}
 
export {GlobalProvider};