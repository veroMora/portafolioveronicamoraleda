import './App.css';
import BarraNav from './components/BarraNav';
import Inicio from './components/Inicio';
import Ranking from './components/Ranking';
import Reglas from './components/Reglas';
import Pie from './components/Pie';
import Error from './components/Error';
import { BrowserRouter, Route, Routes } from "react-router-dom";
import React from "react";
import { GlobalProvider } from './context/GlobalContext';
import PrivateRoute from './components/PrivateRoute';



function App() {
  return (
    <>
      <GlobalProvider>
          <BarraNav/>
          <BrowserRouter>
            <Routes>
              <Route path="/" element={<Inicio/>}></Route>
              <Route path="/ranking" element={
                <PrivateRoute>
                    <Ranking/>
                </PrivateRoute>
              }/>
              <Route path="/rules" element={<Reglas/>}></Route>
              <Route path="*" element={<Error/>}></Route>
            </Routes>
          </BrowserRouter>
        </GlobalProvider>
    </>
  );
}

export default App;