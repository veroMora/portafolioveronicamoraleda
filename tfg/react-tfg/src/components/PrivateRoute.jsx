import { useContext, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { GlobalContext } from "../context/GlobalContext";

export default function PrivateRoute({children}) {

    const {userAuth} = useContext(GlobalContext);

    const navigate = useNavigate();

    useEffect(() => {
        if(userAuth === null || userAuth === undefined) {
            navigate('/');
        }
    }, []);
    
    return children;
}