import { useState, useContext } from 'react';
import {FormattedMessage} from 'react-intl';
import styled, {css} from "styled-components";
import axios from 'axios';
import Modal from './Modal';
import { GlobalContext } from '../context/GlobalContext';

export default function LogIn() {

    const {userAuth, changeLogin, lang} = useContext(GlobalContext);

    let usuarioMongo;

    const [login, setLogin] = useState(false);
    const [user, setUser] = useState({campo: '', valido: null});
    const [password, setPassword] = useState({campo: '', valido: null});
    const [mensajeError, setMensajeError] = useState('false');

    const handleChangeUser = (e) => {
        setUser({...user, campo: e.target.value});
    }

    const validacionUser = () => {
        if(user.campo == '') {
            setUser({...user, valido: 'false'});
        } else setUser({...user, valido: 'true'});
    }

    const handleChangePassword = (e) => {
        setPassword({...password, campo: e.target.value});
    }

    const validacionPassword = () => {
        if(password.campo == '') {
            setPassword({...password, valido: 'false'});
        } else setPassword({...password, valido: 'true'});
    }

    const onSubmit = async(e) => {

        e.preventDefault();

        try {
            const response = await axios.get(`http://localhost:3001/ranking/buscarUsuario/${user.campo}/${password.campo}`)
            console.log(response.data.ranking)
            usuarioMongo = response.data.ranking
        } catch(error) {
            console.log(error)
        }
        
        if(usuarioMongo != null) {
            console.log('Usuario encontrado')
            console.log(user.campo)
            changeLogin(user.campo, password.campo)
            setMensajeError('false');
            setUser({...user, campo: '',valido: 'false'});
            setPassword({...password, campo: '', valido: 'false'});
            setLogin(!login);
        } else {
            console.log('Usuario no encontrado')
            console.log(user.campo)
            setMensajeError('true');
            setUser({...user, campo: '',valido: 'false'});
            setPassword({...password, campo: '', valido: 'false'});
            
        }

    };

    let leftIn;
    let leftOut;
    
    if (lang == 'es-ES') {
        leftIn = '22px';
        leftOut = '12px';
        
    } else if (lang == 'en-UK') {
        leftIn = '50px';
        leftOut = '40px';
    }

    return (
        
        <div className='divLogIn'>
            <div>

                {
                    userAuth ? (
                        <span className='spanLogIn' style={{right: 10}}>
                            <a href='#' onClick={() => {changeLogin('', ''); window.location.href = `http://localhost:3000/`}}>
                            <p className='textLog' style={{marginLeft: leftOut}}><FormattedMessage id="menu.logout" defaultMessage="Cerrar sesión"></FormattedMessage></p>
                            <svg className="svgLogout" xmlns="http://www.w3.org/2000/svg" width="30px" height="30px" viewBox="0 0 24 24" fill="none">
                                <path d="M12 12H19M19 12L16 15M19 12L16 9" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round"/>
                                <path d="M19 6V5C19 3.89543 18.1046 3 17 3H7C5.89543 3 5 3.89543 5 5V19C5 20.1046 5.89543 21 7 21H17C18.1046 21 19 20.1046 19 19V18" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round"/>
                            </svg>
                            </a>
                        </span>
                    ): (
                        <span className='spanLogIn' style={{right: 10}}>
                            <a href='#' onClick={() => {setLogin(!login); console.log(userAuth)}}>
                            <p className='textLog' style={{marginLeft: leftIn}}><FormattedMessage id="menu.login" defaultMessage="Identifícate"></FormattedMessage></p>
                            <svg className="svgLogin" xmlns="http://www.w3.org/2000/svg" width="30px" height="30px" viewBox="0 0 24 24" fill="none">
                                <path d="M19 12H12M12 12L15 15M12 12L15 9" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round"/>
                                <path d="M19 6V5C19 3.89543 18.1046 3 17 3H7C5.89543 3 5 3.89543 5 5V19C5 20.1046 5.89543 21 7 21H17C18.1046 21 19 20.1046 19 19V18" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round"/>
                            </svg>
                            </a>
                        </span>
                    )
                }
                

                <Modal state={login} changeState={setLogin} changeError={setMensajeError}>
                    <Form onSubmit={(e) => onSubmit(e)} className='formLogIn' validoUser={user.valido} validoPassword={password.valido}>
                        <p><FormattedMessage id="message.form" defaultMessage="Si ya tienes una cuenta por favor inicia sesión aquí"></FormattedMessage></p>
                        <label htmlFor="username"><FormattedMessage id="label.user" defaultMessage="Usuario"></FormattedMessage></label>
                        <input type="text" placeholder="Enter Username" value={user.campo} onChange={handleChangeUser} onKeyUp={validacionUser} onBlur={validacionUser} required={true}/>
                        <svg className='svgUser' xmlns="http://www.w3.org/2000/svg" height="15px" viewBox="0 0 512 512">
                            <path d="M256 48a208 208 0 1 1 0 416 208 208 0 1 1 0-416zm0 464A256 256 0 1 0 256 0a256 256 0 1 0 0 512zM369 209c9.4-9.4 9.4-24.6 0-33.9s-24.6-9.4-33.9 0l-111 111-47-47c-9.4-9.4-24.6-9.4-33.9 0s-9.4 24.6 0 33.9l64 64c9.4 9.4 24.6 9.4 33.9 0L369 209z"/>
                        </svg>
                        <label htmlFor="password"><FormattedMessage id="label.password" defaultMessage="Contraseña"></FormattedMessage></label>
                        <input type="password" placeholder="Enter Password" value={password.campo} onChange={handleChangePassword} onKeyUp={validacionPassword} onBlur={validacionPassword} required={true}/>
                        <svg className='svgPassword' xmlns="http://www.w3.org/2000/svg" height="15px" viewBox="0 0 512 512">
                            <path d="M256 48a208 208 0 1 1 0 416 208 208 0 1 1 0-416zm0 464A256 256 0 1 0 256 0a256 256 0 1 0 0 512zM369 209c9.4-9.4 9.4-24.6 0-33.9s-24.6-9.4-33.9 0l-111 111-47-47c-9.4-9.4-24.6-9.4-33.9 0s-9.4 24.6 0 33.9l64 64c9.4 9.4 24.6 9.4 33.9 0L369 209z"/>
                        </svg>
                        <MensajeError mensajeError={mensajeError}>
                            <p>
                                <svg xmlns="http://www.w3.org/2000/svg" height="14px" viewBox="0 0 512 512">
                                    <path d="M256 32c14.2 0 27.3 7.5 34.5 19.8l216 368c7.3 12.4 7.3 27.7 .2 40.1S486.3 480 472 480H40c-14.3 0-27.6-7.7-34.7-20.1s-7-27.8 .2-40.1l216-368C228.7 39.5 241.8 32 256 32zm0 128c-13.3 0-24 10.7-24 24V296c0 13.3 10.7 24 24 24s24-10.7 24-24V184c0-13.3-10.7-24-24-24zm32 224a32 32 0 1 0 -64 0 32 32 0 1 0 64 0z"/>
                                </svg>
                                <b>Error:</b> <FormattedMessage id="message.error" defaultMessage="usuario y/o contraseña incorrectos"></FormattedMessage></p>
                        </MensajeError>
                        <input type="submit" value="Log In"/>
                    </Form>
                </Modal>
            </div>
        </div>
    )
}

const colores = {
    borde: "#0075FF",
    exito: "#1ED12D"
}

const Form = styled.form`
    position: relative;
    display: flex;
	flex-direction: column;
	align-items: flex-start;
    text-align: center;
    width: 100%;
    z-index: 1;

	p {
		font-size: 15px;
		margin-bottom: 20px;
        color: #FFF;
	}

	label {
		margin: 0;
        padding: 0;
        font-weight: bold;
        color: #FFF;
        display: block;
	}

    input {
        margin-bottom: 20px;
        cursor: pointer;
    }

    input[type="text"], input[type="password"] {
        width: 90%;
        border: none;
        border-bottom: 1px solid #fff;
        background: transparent;
        outline: none;
        height: 30px;
        right: 30px;
        padding-right: 25px;
        color: #fff;
        font-size: 15px;
    }

    input[type="text"] {

        ${props => props.validoUser === 'true' && css`
        border-bottom: 1px solid ${colores.exito} !important;
      `}

      ${props => props.validoUser === 'false' && css`
        border-bottom: 1px solid #FFF;
      `}

    }

    input[type="password"] {

        ${props => props.validoPassword === 'true' && css`
        border-bottom: 1px solid ${colores.exito} !important;
      `}

      ${props => props.validoPassword === 'false' && css`
        border-bottom: 1px solid #FFF;
      `}

    }

    input[type="text"]:focus, input[type="password"]:focus {
        border-bottom: 1px solid ${colores.borde};
    }

      input[type="submit"] {
        width: 100%;
        border: none;
        outline: none;
        height: 40px;
        background: #b80f22;
        color: #fff;
        font-size: 18px;
        border-radius: 10px;
        transition: .3s ease all;
      }

      input[type="submit"]:hover {
        background: #ffc107;
        color: #000;
      }

      a {
        width: 100%;
        text-decoration: none;
        font-size: 12px;
        line-height: 20px;
        color: darkgrey;
        top: 350px;
      }

      a:hover {
        color: #fff;
      }

      a:visited {
        color: darkgrey;
      }

      .svgUser, .svgPassword {
        position: absolute;
        right: 2px;
        fill: ${colores.exito};
        opacity: 0; 
      }

      .svgUser {
        top: 104px;

        ${props => props.validoUser === 'true' && css`
        opacity: 1;
      `}

      ${props => props.validoUser === 'false' && css`
        opacity: 0;
      `}
      }

      .svgPassword {
        top: 178px;

        ${props => props.validoPassword === 'true' && css`
        opacity: 1;
      `}

      ${props => props.validoPassword === 'false' && css`
        opacity: 0;
      `}
        
      }

`;

const MensajeError = styled.div`
    position: relative;
    height: 30px;
    line-height: 30px;
    background: #F66060;
    border-radius: 3px;
    width: 100%;
    margin-bottom: 20px;
    display: none;

    p {
        margin: 0;
        color: #262626;
        font-size: 12px;
    }

    b {
        margin-left: 7px;
    }

    svg {
        position: relative;
        top: 2px;
        fill: #262626;
        opacity: 1;
    }

    ${props => props.mensajeError === 'true' && css`
        display: block;
  `}

  ${props => props.mensajeError === 'false' && css`
        display: none;
  `}

`;