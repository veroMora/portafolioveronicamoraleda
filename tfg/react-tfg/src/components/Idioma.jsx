import React, {useState, useContext} from "react";
import { GlobalContext } from "../context/GlobalContext";

export default function Idioma() {

    const {changeLanguage, lang} = useContext(GlobalContext);

    const [display, setDisplay] = useState(0);

    let visualizacion
    let radiusES
    let radiusEN

    if (display==1){
        visualizacion="flex"
        radiusES="0 0 15px 15px"
        radiusEN="15px 15px 0 0"
        
    } else{
        visualizacion="none"
    }

    let es = (
        <>
                <span>ES</span>
                <svg width="27" height="27" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <rect y="5.71387" width="40" height="28.5714" rx="4" fill="white"/>
                    <mask id="mask0_145_2216" mask-type="luminance" maskUnits="userSpaceOnUse" x="0" y="5" width="40" height="30">
                    <rect y="5.71387" width="40" height="28.5714" rx="4" fill="white"/>
                    </mask>
                    <g mask="url(#mask0_145_2216)">
                    <path fillRule="evenodd" clipRule="evenodd" d="M0 13.3329H40V5.71387H0V13.3329Z" fill="#DD172C"/>
                    <path fillRule="evenodd" clipRule="evenodd" d="M0 34.286H40V26.667H0V34.286Z" fill="#DD172C"/>
                    <path fillRule="evenodd" clipRule="evenodd" d="M0 26.6663H40V13.333H0V26.6663Z" fill="#FFD133"/>
                    <path fillRule="evenodd" clipRule="evenodd" d="M10.4766 19.0479H12.3813V20.0002H10.4766V19.0479Z" fill="#FFEDB1"/>
                    <path d="M8.87919 19.4844C8.8468 19.0957 9.15352 18.7624 9.54355 18.7624H11.4073C11.7974 18.7624 12.1041 19.0957 12.0717 19.4844L11.874 21.8564C11.8134 22.5838 11.2053 23.1433 10.4754 23.1433C9.74553 23.1433 9.13747 22.5838 9.07686 21.8564L8.87919 19.4844Z" stroke="#A41517" strokeWidth="1.33333"/>
                    <path fillRule="evenodd" clipRule="evenodd" d="M8.57227 20H12.3818V20.9524H11.4294L10.477 22.8571L9.52465 20.9524H8.57227V20Z" fill="#A41517"/>
                    <rect x="5.71484" y="17.1426" width="1.90476" height="6.66667" rx="0.952381" fill="#A41517"/>
                    <rect x="13.334" y="17.1426" width="1.90476" height="6.66667" rx="0.952381" fill="#A41517"/>
                    <path fillRule="evenodd" clipRule="evenodd" d="M8.57227 16.7621C8.57227 15.9205 9.2545 15.2383 10.0961 15.2383H10.858C11.6996 15.2383 12.3818 15.9205 12.3818 16.7621C12.3818 16.9725 12.2112 17.143 12.0008 17.143H8.95322C8.74282 17.143 8.57227 16.9725 8.57227 16.7621Z" fill="#A41517"/>
                    </g>
                </svg>
        </>
    );

    let en = (
        <>
                <span className="idiomaBoton">EN</span>
                <svg width="27" height="27" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <rect y="5.71387" width="40" height="28.5714" rx="4" fill="white"/>
                    <mask id="mask0_145_2470" mask-type="luminance" maskUnits="userSpaceOnUse" x="0" y="5" width="40" height="30">
                    <rect y="5.71387" width="40" height="28.5714" rx="4" fill="white"/>
                    </mask>
                    <g mask="url(#mask0_145_2470)">
                    <rect y="5.71387" width="40" height="28.5714" fill="#0A17A7"/>
                    <path fillRule="evenodd" clipRule="evenodd" d="M0 24.762H15.2381V36.1905H24.7619V24.762H40V15.2381H24.7619V3.80957H15.2381V15.2381H0V24.762Z" fill="white"/>
                    <rect x="-1.83203" y="2.97656" width="55.2381" height="3.80952" transform="rotate(34 -1.83203 2.97656)" fill="white"/>
                    <rect x="-3.96289" y="33.8652" width="55.2381" height="3.80952" transform="rotate(-34 -3.96289 33.8652)" fill="white"/>
                    <path d="M26.668 14.7606L44.7613 2.85742" stroke="#DB1F35" strokeWidth="1.33333" strokeLinecap="round"/>
                    <path d="M28.5898 25.2822L44.8096 36.2148" stroke="#DB1F35" strokeWidth="1.33333" strokeLinecap="round"/>
                    <path d="M11.4362 14.7292L-5.48242 3.32715" stroke="#DB1F35" strokeWidth="1.33333" strokeLinecap="round"/>
                    <path d="M13.2712 25.1494L-5.48242 37.5859" stroke="#DB1F35" strokeWidth="1.33333" strokeLinecap="round"/>
                    <path fillRule="evenodd" clipRule="evenodd" d="M0 22.8567H17.1429V34.2853H22.8571V22.8567H40V17.1424H22.8571V5.71387H17.1429V17.1424H0V22.8567Z" fill="#E6273E"/>
                    </g>
                </svg>
        </>
    );

    let buttonLang
    let buttonChange
    let handleLang

    if (lang == 'es-ES') {
        buttonLang = es
        buttonChange = en
        handleLang = 'en-UK'
    } else if (lang == 'en-UK') {
        buttonLang = en
        buttonChange = es
        handleLang = 'es-ES'
    } else {
        buttonLang = es
        buttonChange = en
        handleLang = 'en-UK'
    }

    return (
      <>
        <div onMouseLeave={()=>setDisplay(0)} className='divLanguage'>
            <button onClick={()=>setDisplay(1)} style={{borderRadius: (radiusEN), marginTop: 13.5}} className="languageButton" >
                {buttonLang}
            </button>
            <button style={{display: (visualizacion), borderRadius: (radiusES), zIndex: 2}} className="buttonChangeLang" onClick={() => {changeLanguage(handleLang);setDisplay(0)}}>
                {buttonChange}
            </button>
        </div>
      </>
    );
  }