import { useState, useEffect } from 'react';
import axios from 'axios';
import Pie from './Pie';
import {FormattedMessage} from 'react-intl';

export default function Ranking() {

    const [ranking, setRanking] = useState([]);
    
    useEffect(() => {
        axios.get('http://localhost:3001/ranking/verRanking')
            .then(response => {
                setRanking(response.data.ranking);
            });
    }, []);

    /*function SecondsToString(seconds) {
        var hour = Math.floor(seconds.value / 3600);
        console.log(seconds.value)
        hour = (hour < 10)? '0' + hour : hour;
        var minute = Math.floor((seconds.value / 60) % 60);
        minute = (minute < 10)? '0' + minute : minute;
        var second = seconds.value % 60;
        second = (second < 10)? '0' + second : second;
        return hour + ':' + minute + ':' + second;
    }*/

    let listRanking = <tr><td><FormattedMessage id="table.empty" defaultMessage="No ha participado nadie en el juego hasta el momento"></FormattedMessage></td></tr>;
    if (ranking.length > 0) {
        
        listRanking = ranking.map((datos) => (
            <tr key={datos._id}>
                <td>{datos.nickname}</td>
                <td>{datos.record}</td>
                <td>{datos.tiempop}</td>
            </tr>
        ));
    }

   let table = (
        <>
            <table className='contentTable'>
                <thead>
                    <tr>
                        <th><FormattedMessage id="col.user" defaultMessage="Usuario"></FormattedMessage></th>
                        <th><FormattedMessage id="col.record" defaultMessage="Récord"></FormattedMessage></th>
                        <th><FormattedMessage id="col.time" defaultMessage="Tiempo actual"></FormattedMessage></th>
                    </tr>
                </thead>
                <tbody>
                    {listRanking}
                </tbody>
            </table>
        </>)

    return (
        <>
           <div className="divRanking">
                <div className='divTopTen'>
                    <p className='titleTopTen'><FormattedMessage id="table.title" defaultMessage="TOP 10: MEJORES AVENTUREROS"></FormattedMessage></p>
                    <div className='divTable'>
                        {table}
                    </div>
                </div>
                <Pie/>
           </div>
        </>
    )
}