import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexion {
	private Connection  connection;
	private String url = "jdbc:mysql://localhost:3306/tienda";
	private String usuario = "root";
	private String contrasenya = "ITEP";
	
	public Conexion(String url, String usuario, String contrasenya) {
		super();
		this.url = url;
		this.usuario = usuario;
		this.contrasenya = contrasenya;
	}
	
	public Connection getConnection() {
		return connection;
	}
	public void setConnection(Connection connection) {
		this.connection = connection;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getContrasenya() {
		return contrasenya;
	}
	public void setContrasenya(String contrasenya) {
		this.contrasenya = contrasenya;
	}
	
	public void establecerConexion() throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.cj.jdbc.Driver");
		connection = DriverManager.getConnection(url,usuario,contrasenya);
	}
	
	public void cerrarConexion() throws SQLException {
		connection.close();
	}
}
