import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class Metodos {

	Scanner t = new Scanner(System.in);

	protected PreparedStatement crearEstado(Connection connection, String query) {
		try {
			return connection.prepareStatement(query);
		} catch (SQLException ex) {
			System.out.println("Error al crear el Statement");
			System.exit(-2);
		}
		return null;
	}

	protected void insertarDataProducto(Connection connection) {

		String query;
		query = "INSERT INTO producto (nombre,precio,codigo_fabricante) VALUES (?,?,?)";

		System.out.println("Introduce el nombre del producto");
		String nombre = t.next();
		System.out.println("\nIntroduce el precio del producto");
		double precio = t.nextDouble();
		System.out.println("\nIntroduce el codigo del fabricante");
		int codFabricante = t.nextInt();

		PreparedStatement pstm = crearEstado(connection, query);

		try {
			pstm.setString(1, nombre);
			pstm.setDouble(2, precio);
			pstm.setInt(3, codFabricante);

			int valor = pstm.executeUpdate();

			if (valor == 1) {
				System.out.println("\n* * *Se ha a�adido correctamente la fila* * *");
			}

			pstm.close();
		} catch (SQLException ex) {
			System.out.println("Error al insertar datos");
			System.out.println(ex.getMessage());
		}
	}

	protected void insertarDataFabricante(Connection connection) {

		String query;
		query = "INSERT INTO fabricante (nombre) VALUES (?)";

		System.out.println("Introduce el nombre del fabricante");
		String nombre = t.next();

		PreparedStatement pstm = crearEstado(connection, query);

		try {
			pstm.setString(1, nombre);

			int valor = pstm.executeUpdate();

			if (valor == 1) {
				System.out.println("\n* * *Se ha a�adido correctamente la fila* * *");
			}

			pstm.close();
		} catch (SQLException ex) {
			System.out.println("Error al insertar datos");
			System.out.println(ex.getMessage());
		}
	}
	
	protected void selectProductosFabricante(Connection connection) {
		String query;
		query = "SELECT producto.nombre, precio	FROM fabricante JOIN producto ON fabricante.codigo = producto.codigo_fabricante WHERE fabricante.nombre = ? ";

		System.out.println("Introduce el nombre del fabricante del producto que quieres conseguir");
		String fabricante = t.next();

		PreparedStatement pstm = crearEstado(connection, query);
		ResultSet result = null;
		try {
			 pstm.setString(1, fabricante);
			 result = pstm.executeQuery();
			 int cont = 0;
			 while (result.next()) {
				 cont++;
				 System.out.println("PRODUCTO " + fabricante + " "+ cont + ": \n");
					String nombre = result.getString(1);
					System.out.println("Nombre del Producto: " + nombre);
					
					double precio = result.getDouble(2);
				    System.out.println("Precio del Producto: " + precio + " euros \n");
				   
				}
			 if (cont == 0) {
				 System.out.println("\n* * *No se ha obtenido ningun resultado en la consulta* * *");
			 }
			 pstm.close();
		} catch (SQLException e) {
	    	System.out.println("Error al realizar la consulta de datos");
		}
	}
	
	protected void selectProductosBaratos(Connection connection) {
		String query;
		query = "SELECT nombre, precio FROM producto ORDER BY precio LIMIT ? ";

		System.out.println("Introduce la cantidad de productos mas baratos que quieres ver");
		int cant = t.nextInt();
		
		PreparedStatement pstm = crearEstado(connection, query);
		ResultSet result = null;
		try {
			 pstm.setInt(1, cant);
			 result = pstm.executeQuery();
			 int cont = 0 ;
			 while (result.next()) {
				 cont++;
				 System.out.println("PRODUCTO "+ cont + ": \n");
					String nombre = result.getString(1);
					System.out.println("Nombre del Producto: " + nombre);
					
					double precio = result.getDouble(2);
				    System.out.println("Precio del Producto: " + precio + " euros\n");
				    
				}
			 if (cont == 0) {
				 System.out.println("\n* * *No se ha obtenido ningun resultado en la consulta* * *");
			 }
			 pstm.close();
		} catch (SQLException e) {
	    	System.out.println("Error al realizar la consulta de datos");
		}
	}
	
	
	protected void updateDataProducto(Connection connection) {
		String query = "UPDATE producto SET precio = ? WHERE codigo = ?";
		
		System.out.println("Introduce el codigo del producto que quieres modificar");
		int codigoP = t.nextInt();
		
		System.out.println("\nIntroduce el nuevo precio del producto");
		double precio = t.nextDouble();
		
		PreparedStatement pstm = crearEstado(connection, query);
		try {
	    	pstm.setDouble(1, precio);
	    	pstm.setInt(2, codigoP);
	    	
	    	int valor = pstm.executeUpdate();

			if (valor == 1) {
				System.out.println("\n* * *Se ha actualizado correctamente la tabla* * *");
			}
	    	pstm.close();
		} catch (SQLException ex) {
	    	System.out.println("Error al actualizar los datos de la tabla");
	    	System.out.println(ex.getMessage());

		}
	}


	protected void eliminarProducto(Connection connection) {

		String query;
		query = "DELETE FROM producto WHERE codigo = ?";

		System.out.println("Introduce el codigo del producto que quieres eliminar");
		int codigo = t.nextInt();

		PreparedStatement pstm = crearEstado(connection, query);

		try {
			pstm.setInt(1, codigo);

			int valor = pstm.executeUpdate();

			if (valor == 1) {
				System.out.println("\n* * *Se ha eliminado correctamente la fila seleccionada* * *");
			} else {
				System.out.println("\n* * *La fila seleccionada no existe* * *");
			}

			pstm.close();
		} catch (SQLException ex) {
			System.out.println("Error al eliminar la fila");
			System.out.println(ex.getMessage());
		}
	}



	protected void salir() {
		System.out.println("* * *Programa Cerrado* * *");
		System.exit(0);
	}

}
