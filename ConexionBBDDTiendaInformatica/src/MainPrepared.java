import java.sql.Connection;
import java.sql.SQLException;
import java.util.Scanner;

public class MainPrepared {
	
	public static Scanner t;
	public static Metodos m;

	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		//Creamos la configuracion de la conexion: url, usuario, contrese�a
		Conexion con = new Conexion("jdbc:mysql://localhost:3306/tienda","root","ITEP");
		con.establecerConexion(); // Establecer conexion
		
		//Creamos la conexion
		Connection connection = con.getConnection();
		
		t = new Scanner(System.in);
		
		System.out.println("* * *Inicio del Programa* * *");

		m = new Metodos();

		int accion;

		do {
			menu();
			accion = t.nextInt();
			elegirAccion(accion, connection);
		} while (accion != 7);
		
		con.cerrarConexion();

	}
	
	private static void elegirAccion(int accion, Connection connection) {

		switch (accion) {
		case 1:
			m.insertarDataProducto(connection);
			break;
		case 2:
			m.insertarDataFabricante(connection);
			break;
		case 3:
			m.selectProductosFabricante(connection);
			break;
		case 4:
			m.selectProductosBaratos(connection);
			break;
		case 5:
			m.updateDataProducto(connection);
			break;
		case 6:
			m.eliminarProducto(connection);
			break;
		case 7:
			m.salir();
			break;
		default:
			System.out.println("No existe la accion introducida");
		}
	}
	
	private static void menu() {
		System.out.println("\nRealiza estas acciones/tareas: \n"); 
		System.out.println("1-Insertar productos\n"); 
		System.out.println("2-Insertar fabricantes\n"); 
		System.out.println("3-Recupera los productos del fabricante Asus\n"); 
		System.out.println("4-Recupera los cinco productos mas baratos\n"); 
		System.out.println("5-Edita el precio del producto con ID: 1 a 101.00 euros\n"); 
		System.out.println("6-Elimina el producto 2\n"); 
		System.out.println("7-Salir \n"); 

	}

}
