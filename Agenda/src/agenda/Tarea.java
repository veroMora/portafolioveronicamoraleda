package agenda;

import java.io.Serializable;
import java.util.Date;

public class Tarea implements Serializable {
    
    private Persona p;
    private String descTarea;
    private Date fecha = new Date();

    public Tarea(Persona p, String descTarea, Date fecha) {
        this.p = p;
        this.descTarea = descTarea;
        this.fecha = fecha;
    }

    public Tarea() {
    }

    public Persona getP() {
        return p;
    }

    public void setP(Persona p) {
        this.p = p;
    }

    public String getDescTarea() {
        return descTarea;
    }

    public void setDescTarea(String descTarea) {
        this.descTarea = descTarea;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Override
    public String toString() {
        return "Tarea{" + "p=" + p + ", descTarea=" + descTarea + ", fecha=" + fecha + '}';
    }
    
}
