package agenda;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class Cliente {

    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {

        Socket sc = new Socket("localhost", 5000);

        DataInputStream in = new DataInputStream(sc.getInputStream());
        DataOutputStream out = new DataOutputStream(sc.getOutputStream());

        ObjectOutputStream outObjeto = new ObjectOutputStream(sc.getOutputStream());
        ObjectInputStream inObjeto = new ObjectInputStream(sc.getInputStream());

        ClienteHilo hilo = new ClienteHilo(in, out, outObjeto, inObjeto);
        hilo.start();
        hilo.join();
        
        outObjeto.close();
        inObjeto.close();
        sc.close();
    }

}
