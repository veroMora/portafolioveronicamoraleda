package agenda;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ClienteHilo extends Thread {

    Scanner t = new Scanner(System.in);

    private DataInputStream in;
    private DataOutputStream out;
    private ObjectOutputStream outObjeto;
    private ObjectInputStream inObjeto;

    public ClienteHilo(DataInputStream in, DataOutputStream out, ObjectOutputStream outObjeto, ObjectInputStream inObjeto) {
        this.in = in;
        this.out = out;
        this.outObjeto = outObjeto;
        this.inObjeto = inObjeto;
    }

    @Override
    public void run() {

        try {
            // Recibo el mensaje del menu del servidor
            String menu = in.readUTF();
            // Se imprime el menu en el cliente
            System.out.println(menu);

            int operacion;
            try {
                operacion = t.nextInt();
                while (operacion > 2 || operacion <= 0) {
                    System.err.println("La operacion introducida no existe. Introduce un numero entre 1-2.");
                    operacion = t.nextInt();
                }

            } catch (InputMismatchException ex) {
                System.err.println("Error porque se ha introducido una cadena de caracteres o un double. Por defecto se elegira la operacion 1. Ver las tareas de la agenda.");
                operacion = 1;
                t.next();
            }
            out.writeInt(operacion);

            if (operacion == 1) {
                System.out.println("* * * TAREAS ALMACENADAS * * *");
                ArrayList<Tarea> tareasAl = (ArrayList) inObjeto.readObject();
                leerArray(tareasAl);
            } else {
                String str = in.readUTF();
                System.out.println(str);

                outObjeto.writeObject(menuRegistro());
            }

        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(ClienteHilo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void leerArray(ArrayList<Tarea> tareasAlmacenadas) {

        if (tareasAlmacenadas.isEmpty()) {
            System.err.println("No hay tareas almacenadas en la agenda.");
        } else {
            System.out.println("Las tareas de la agenda almacenadas en el server son:\n");

            for (int i = 0; i < tareasAlmacenadas.size(); i++) {
                System.out.println("* * * TAREA " + (i + 1) + " * * * ");
                System.out.println("Nombre: " + tareasAlmacenadas.get(i).getP().getNombre());
                System.out.println("Apellido: " + tareasAlmacenadas.get(i).getP().getApellido());
                System.out.println("Edad: " + tareasAlmacenadas.get(i).getP().getEdad());
                System.out.println("Descripcion de la tarea: " + tareasAlmacenadas.get(i).getDescTarea());
                System.out.println("Fecha del registro: " + tareasAlmacenadas.get(i).getFecha());
                System.out.println("* * * * * * * * * *\n");
            }
        }

    }

    private Tarea menuRegistro() {

        System.out.println("Introduce el nombre");
        String nombre = t.next();

        System.out.println("Introduce el apellido");
        String apellido = t.next();

        int edad = 0;
        try {
            System.out.println("Introduce la edad");
            edad = t.nextInt();
        } catch (InputMismatchException ex) {
            System.err.println("Error porque se ha introducido una cadena de caracteres o un double. Por defecto se quedara la edad en 0");
            t.next();
        }

        Persona p = new Persona(nombre, apellido, edad);

        Tarea tareaR = new Tarea();

        tareaR.setP(p);

        System.out.println("Introduce la descripcion de la tarea");
        tareaR.setDescTarea(t.next());

        Date fechaActual = new Date();
        tareaR.setFecha(fechaActual);

        return tareaR;
    }
}
