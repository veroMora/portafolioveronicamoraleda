package agenda;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Servidor {

    public static void main(String[] args) throws IOException, ClassNotFoundException {

        ArrayList<Tarea> tareas = new ArrayList<>();

        ServerSocket servidor = new ServerSocket(5000);
        Socket sc;

        while (true) {
            System.out.println("Esperando al cliente...");
            sc = servidor.accept();
            System.out.println("Cliente conectado a la AGENDA del servidor");

            DataInputStream in = new DataInputStream(sc.getInputStream());
            DataOutputStream out = new DataOutputStream(sc.getOutputStream());

            ObjectOutputStream outObjeto = new ObjectOutputStream(sc.getOutputStream());
            ObjectInputStream inObjeto = new ObjectInputStream(sc.getInputStream());

            // Envio un menu al cliente para pedirle la operacion que quiere realizar
            menu(out);

            // Servidor lee la operacion que quiere el cliente (numEntero)
            int op = in.readInt();
            System.out.println("Operacion escogida por el cliente: " + op);

            if (op == 1) {
                outObjeto.writeObject(tareas);
            } else {
                out.writeUTF("* * * REGISTRA LA TAREA * * *");
                Tarea tarea = (Tarea) inObjeto.readObject();
                tareas.add(tarea);
            }
            outObjeto.close();
            inObjeto.close();
            sc.close();
        }

    }

    private static void menu(DataOutputStream out) throws IOException {
        out.writeUTF("Introduce el numero de la operacion que quieres realizar\n1. Ver las tareas de la agenda.\n2. Registar tarea.");

    }

}
