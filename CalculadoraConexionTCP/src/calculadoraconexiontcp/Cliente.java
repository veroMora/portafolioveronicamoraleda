package calculadoraconexiontcp;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Cliente {

    static Scanner t; 
    
    public static void main(String[] args) throws IOException {
        t = new Scanner(System.in);

        final String HOST = "localhost";
        final int PUERTO = 5000;
        DataInputStream in;
        DataOutputStream out;

        // Creo el socket para conectarme con el servidor
        Socket sc = new Socket(HOST, PUERTO);
        in = new DataInputStream(sc.getInputStream());
        out = new DataOutputStream(sc.getOutputStream());

        // Recibo el mensaje del menu del servidor
        String menu = in.readUTF();

        // Se imprime el menu en el cliente
        System.out.println(menu);

        // Cliente envia un numero indicando la operacion a realizar
        int operacion = 0;
        try {
            operacion = t.nextInt();
            while (operacion > 4 || operacion <= 0) {
                System.err.println("La operacion introducida no existe. Introduce un numero entre 1-4.");
                operacion = t.nextInt();
            }
   
        } catch (InputMismatchException ex) {
            System.err.println("Error porque se ha introducido una cadena de caracteres o un double. Por defecto se elegira la operacion 1 (SUMA).");
            operacion = 1;
            t.next();
        }
        out.writeInt(operacion);
        
        // Cliente recibe el mensaje del servidor para que introduzca un numero entero
        String mensajeNumero = in.readUTF();
        System.out.println(mensajeNumero);

        // Cliente envia un numero entero
        int num1 = 0;
        controlExcepciones(out, num1); 
        
        // Cliente recibe el mensaje del servidor para que introduzca OTRO numero entero
        String mensajeNumero2 = in.readUTF();
        System.out.println(mensajeNumero2);
        
        // Cliente envia OTRO numero entero
        int num2 = 0;
        controlExcepciones(out, num2);  

        // Recibo el resultado de la operacion realizado por el servidor
        String resultado = in.readUTF();

        System.out.println("RESULTADO DE LA OPERACION: " + resultado);
    }
    
    private static void controlExcepciones(DataOutputStream out, int num) throws IOException {
         try {
            num = t.nextInt();
        } catch (InputMismatchException e) {
            System.err.println("Error porque se ha introducido una cadena de caracteres o un double. Por defecto se elegira el numero 5.");
            num = 5;
            t.next();
        }
        out.writeInt(num);
    }
}
