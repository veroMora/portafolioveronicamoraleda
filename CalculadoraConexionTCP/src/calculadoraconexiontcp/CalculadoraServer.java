package calculadoraconexiontcp;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.DecimalFormat;
import java.util.Scanner;

public class CalculadoraServer {

    public static void main(String[] args) throws IOException {
        Scanner t = new Scanner(System.in);

        ServerSocket servidor = null;
        Socket sc = null;
        DataInputStream in;
        DataOutputStream out;

        final int PUERTO = 5000;
        servidor = new ServerSocket(5000);
        System.out.println("Servidor abierto");

        sc = servidor.accept();
        System.out.println("Cliente conectado a la CALCULADORA del servidor");
        in = new DataInputStream(sc.getInputStream());
        out = new DataOutputStream(sc.getOutputStream());

        // Envio un menu al cliente para pedirle la operacion que quiere realizar
        menu(out);

        // Servidor lee la operacion que quiere el cliente (numEntero)
        int operacion = in.readInt();
        System.out.println("Operacion escogida por el cliente: " + operacion);

        //Envio al cliente un mensaje para que introduzca un numero
        out.writeUTF("Introduce un numero entero");

        // Servidor lee el numero entero introducido por el cliente
        int numero1 = in.readInt();
        System.out.println("Primer numero escogido por el cliente: " + numero1);

        //Envio al cliente un mensaje para que introduzca OTRO numero
        out.writeUTF("Introduce otro numero entero");

        // Servidor lee el segundo numero entero introducido por el cliente
        int numero2 = in.readInt();
        System.out.println("Segundo numero escogido por el cliente: " + numero2);

        // Servidor envia el resultado de la operacion seleccionada por el cliente.
        out.writeUTF(operaciones(operacion, numero1, numero2));

        sc.close();
        System.out.println("Cliente desconectado");

    }

    private static void menu(DataOutputStream out) throws IOException {
        out.writeUTF("Introduce el numero de la operacion que quieres realizar\n1 SUMA; 2 RESTA; 3 MULTIPLICACION; 4 DIVISION");

    }

    private static String operaciones(int op, int num1, int num2) {
        double resultado = 0;
        String caracter = null;
        String error = null;

        switch (op) {
            case 1:
                resultado = num1 + num2;
                caracter = " + ";
                break;
            case 2:
                resultado = num1 - num2;
                caracter = " - ";
                break;
            case 3:
                resultado = num1 * num2;
                caracter = " * ";
                break;
            case 4:
                caracter = " / ";
                resultado = (double) num1 / num2;
        }
        
        return num1 + caracter + num2 + " = " + resultado;

    }

}
