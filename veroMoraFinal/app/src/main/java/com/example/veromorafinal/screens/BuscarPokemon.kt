package com.example.veromorafinal.screens

import android.annotation.SuppressLint
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.example.veromorafinal.R
import com.example.veromorafinal.navigation.AppScreens
import com.example.veromorafinal.uiViewModel.ViewModel

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun BuscarPokemon(navController: NavHostController, ViewModel: ViewModel) {

    val nombrePokemon: String by ViewModel.nombrePokemon.observeAsState(initial = "")
    val isButtonEnable: Boolean by ViewModel.isButtonEnable.observeAsState (initial = false)

    Scaffold(topBar = {
        TopAppBar(
            backgroundColor = Color(0xFF212121),
            contentColor = Color.White
        ) {
            Icon(
                imageVector = Icons.Default.ArrowBack,
                contentDescription = "Volver a la pantalla principal",
                modifier = Modifier.clickable { navController.popBackStack() }
            )
            Spacer(modifier = Modifier.width(107.dp))
            
            Text(
                text = "Buscar un Pokémon",
                color = Color.White
            )
        }
    }) {
            Column(
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier
                    .fillMaxSize()
                    .background(Color(0xFF212121))
                    .padding(top = 45.dp, bottom = 10.dp)

            ) {
                Image(
                    painter = painterResource(R.drawable.pokemon),
                    contentDescription = "Logo de Pokemon",
                    modifier = Modifier.size(220.dp)
                )

                OutlinedTextField(
                    value = nombrePokemon,
                    onValueChange = { ViewModel.onCompletedField (nomP = it) },
                    label = { Text("Introduce el nombre del Pokémon") },
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(start = 25.dp, end = 25.dp),
                    singleLine = true,
                    keyboardOptions = KeyboardOptions(
                        autoCorrect = true,
                        imeAction = ImeAction.Next
                    ),
                    shape = RoundedCornerShape(12.dp),
                    colors = TextFieldDefaults.textFieldColors(
                        textColor = Color(0xFFF1F1F1),
                        backgroundColor = Color(0xFF003c8f),
                        cursorColor = Color(0xFFF1F1F1),
                        focusedIndicatorColor = Color(0xFFF1C232),
                        unfocusedIndicatorColor = Color(0xFFF1C232),
                        //leadingIconColor = Color.White,
                        focusedLabelColor = Color(0xFFF1F1F1),
                        unfocusedLabelColor = Color(0xFFF1F1F1)
                    )
                )

                Spacer(modifier = Modifier.size(35.dp))

                Button(
                    onClick = {
                        navController.navigate(route = AppScreens.Pokemon.ruta + "/${nombrePokemon}")

                    },
                    enabled = isButtonEnable,
                    elevation = ButtonDefaults.elevation(10.dp),
                    shape = RoundedCornerShape(20.dp),
                    contentPadding = PaddingValues(0.dp),
                    colors = ButtonDefaults.buttonColors(
                        backgroundColor = Color(0xFFb2ebf2),
                        contentColor = Color(0xFF212121),
                        disabledBackgroundColor = Color(0xFF000051),
                        disabledContentColor = Color(0xFFA3A3A3)
                    ),
                    modifier = Modifier
                        //.border(2.dp, Color(0xFF000000), RoundedCornerShape(20.dp))
                        .size(width = 150.dp, height = 50.dp),
                ){
                    Text(text = "Pokémon")
                }

                Image(
                    painter = painterResource(R.drawable.legendary),
                    contentDescription = "Logo de Pokemon",
                    modifier = Modifier.size(450.dp),
                    contentScale = ContentScale.Crop
                )


            }

    }
}