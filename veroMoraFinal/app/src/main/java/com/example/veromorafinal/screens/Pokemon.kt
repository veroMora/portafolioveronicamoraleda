package com.example.veromorafinal.screens

import android.annotation.SuppressLint
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import coil.compose.AsyncImage
import com.example.veromorafinal.data.CajaTipos
import com.example.veromorafinal.data.Colores
import com.example.veromorafinal.data.Stats
import com.example.veromorafinal.data.toInt
import com.google.firebase.firestore.FirebaseFirestore

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun Pokemon(navController: NavController, nombreP: String?) {

    val db = FirebaseFirestore.getInstance()
    var nombreColeccion = "pokedex"
    val campoBuscado = "name"

    var imagenPokemon by remember { mutableStateOf("") }
    var hp by remember { mutableStateOf("") }
    var attack by remember { mutableStateOf("") }
    var defense by remember { mutableStateOf("") }
    var speed by remember { mutableStateOf("") }
    var type1 by remember { mutableStateOf("") }
    var type2 by remember { mutableStateOf("") }
    var altura by remember { mutableStateOf("") }
    var peso by remember { mutableStateOf("") }

    var error by remember { mutableStateOf("") }

    db.collection(nombreColeccion)
        .whereEqualTo(campoBuscado,nombreP?.let { it })
        .get()
        .addOnSuccessListener { resultadoPokemon ->
            for (dato in resultadoPokemon) {
                imagenPokemon = dato.get("image").toString()
                hp = dato.get("hp").toString()
                attack = dato.get("attack").toString()
                defense = dato.get("defense").toString()
                speed = dato.get("speed").toString()
                type1 = dato.get("type1").toString()
                type2 = dato.get("type2").toString()
                altura = dato.get("height_m").toString()
                peso = dato.get("weight_kg").toString()
            }

        }
        .addOnFailureListener {
            error = "No se ha podido obtener los datos de los Pokemon"
        }

    val colorTipo1 = Colores(type1)
    val colorTipo2 = Colores(type2)
    val hpInt = toInt(hp)
    val attackInt = toInt(attack)
    val defenseInt = toInt(defense)
    val speedInt = toInt(speed)
    val statHP = "HP"
    val statATK = "ATK"
    val statDEF = "DEF"
    val statSPD = "SPD"

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(Color(0xFF212121))
            .padding(10.dp)
            .verticalScroll(rememberScrollState()),
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        Box(
            modifier = Modifier
                .height(250.dp)
                .fillMaxWidth()
                .background(color = Color(0xFFb2ebf2), shape = RoundedCornerShape(10.dp))
        ) {
            Icon(
                imageVector = Icons.Default.ArrowBack,
                contentDescription = "Volver a la pantalla principal",
                modifier = Modifier.clickable { navController.popBackStack() }
            )
            
            AsyncImage(
                model = imagenPokemon,
                contentDescription = null,
                modifier = Modifier.fillMaxSize()
            )
        }

        Spacer(modifier = Modifier.size(30.dp))

        nombreP?.let {
            Text(
                text = it.uppercase(),
                textAlign = TextAlign.Center,
                color = Color.White,
                fontWeight = FontWeight.Bold,
                fontSize = 50.sp
            )
        }

        Spacer(modifier = Modifier.size(10.dp))

        Text(
            text = "Peso: ${peso}KG - Altura: ${altura}M",
            fontFamily = FontFamily.Monospace,
            textAlign = TextAlign.Center,
            color = Color.White,
        )

        CajaTipos(type1 = type1, type2 = type2, colorTipo1 = colorTipo1, colorTipo2 = colorTipo2)

        Spacer(modifier = Modifier.size(20.dp))

        Text(
            text = "STATS",
            fontFamily = FontFamily.Monospace,
            textAlign = TextAlign.Center,
            color = Color.White,
            fontWeight = FontWeight.Bold,
            fontSize = 25.sp
        )

        Spacer(modifier = Modifier.size(20.dp))

        Stats(hpInt,statHP)

        Spacer(modifier = Modifier.size(45.dp))

        Stats(attackInt,statATK)

        Spacer(modifier = Modifier.size(45.dp))

        Stats(defenseInt,statDEF)

        Spacer(modifier = Modifier.size(45.dp))

        Stats(speedInt,statSPD)
        
    }
}





