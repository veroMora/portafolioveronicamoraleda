package com.example.veromorafinal.uiViewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ViewModel: ViewModel() {

    private val _nombrePokemon = MutableLiveData<String>()
    val nombrePokemon : LiveData<String> = _nombrePokemon

    private val _isButtonEnable = MutableLiveData<Boolean>()
    val isButtonEnable: LiveData<Boolean> = _isButtonEnable

    fun onCompletedField(nomP: String) {
        _nombrePokemon.value = nomP
        _isButtonEnable.value = enableButton(nomP)
    }
    fun enableButton(alias: String) = alias.isNotEmpty()

    fun cleanTextField() {
        _nombrePokemon.value = ""
    }
    fun cleanFieldTrue(nomP: String) {
        _isButtonEnable.value = enableButton(nomP)
    }
}