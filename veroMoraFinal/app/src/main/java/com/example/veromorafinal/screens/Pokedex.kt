package com.example.veromorafinal.screens

import android.annotation.SuppressLint
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import coil.compose.AsyncImage
import coil.compose.rememberAsyncImagePainter
import coil.compose.rememberImagePainter
import com.example.veromorafinal.R
import com.example.veromorafinal.navigation.AppScreens
import com.google.firebase.firestore.FirebaseFirestore

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun Pokedex(navController: NavHostController) {

    val db = FirebaseFirestore.getInstance()
    var nombreColeccion = "pokedex"
    var nombrePokemon by remember { mutableStateOf(listOf<String>()) }
    var imagenPokemon by remember { mutableStateOf(listOf<String>()) }
    var error by remember { mutableStateOf("") }

    db.collection(nombreColeccion)
        .get()
        .addOnSuccessListener { resultadoPokemon ->
            for (dato in resultadoPokemon) {
                nombrePokemon += dato.get("name").toString()
                imagenPokemon += dato.get("image").toString()
            }

        }
        .addOnFailureListener {
            error = "No se ha podido obtener los datos de los Pokemon"
        }

    Scaffold(topBar = {
        TopAppBar(
            backgroundColor = Color(0xFF212121),
            contentColor = Color.White
        ) {
            Icon(
                imageVector = Icons.Default.ArrowBack,
                contentDescription = "Volver a la pantalla principal",
                modifier = Modifier.clickable { navController.popBackStack() }
            )
            Spacer(modifier = Modifier.width(97.dp))
            Image(
                painter = painterResource(R.drawable.pokemon),
                contentDescription = "Logo de Pokemon",
            )
        }
    }) {
            //Text(text = ((nombrePokemon.size/2)).toString())
        Column(
            modifier = Modifier
                .fillMaxSize()
                .background(color = Color(0xFF212121))
        ) {
            LazyVerticalGrid(
                columns = GridCells.Adaptive(150.dp),
                modifier = Modifier
                    .padding(start = 7.5.dp, end = 7.5.dp, bottom = 15.dp)
                    .background(color = Color(0xFF212121))

            ) {
                items(nombrePokemon.size/2) { item ->
                    MediaListItem(
                        name = nombrePokemon[item],
                        urlImagen = imagenPokemon[item],
                        navController = navController
                    )

                }

            }
        }

    }
}

@Composable
fun MediaListItem(
    name: String,
    urlImagen: String,
    navController: NavController
) {
    Column(
        modifier = Modifier
            .padding(start = 7.5.dp, end = 7.5.dp, top = 15.dp)
            .clickable { navController.navigate(route = AppScreens.Pokemon.ruta + "/${name}") }
    ) {
        Box(
            modifier = Modifier
                .height(200.dp)
                .fillMaxWidth()
                .background(color = Color(0xFFb2ebf2), shape = RoundedCornerShape(10.dp)),
            contentAlignment = Alignment.BottomCenter
        ) {
            AsyncImage(
                model = urlImagen,
                contentDescription = null,
                modifier = Modifier.fillMaxSize()
            )
            Text(
                text = name,
                fontWeight = FontWeight.Bold,
                color = Color(0xFF212121)
            )
        }

        /*Box(
            contentAlignment = Alignment.Center,
            modifier = Modifier
                .fillMaxWidth()
                .background(MaterialTheme.colors.secondary)
                .padding(16.dp)
        ) {
            Text(
                text = name,
                style = MaterialTheme.typography.h6
            )
        }*/
    }
}