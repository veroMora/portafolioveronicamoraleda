package com.example.veromorafinal.screens

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.ButtonDefaults.elevation
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import com.example.veromorafinal.R

@Composable
fun MenuPokemon(navController: NavHostController) {

        Column(
            modifier = Modifier
                .fillMaxSize()
                .background(Color(0xFF212121))
                .padding(10.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {

            Image(
                painter = painterResource(R.drawable.pikapoke),
                contentDescription = "Logo de Pokemon",
                modifier = Modifier.size(350.dp),
            )

            Boton(
                navController = navController,
                ruta = "Pokedex",
                nombreBoton = "Pokédex",
                icono = Icons.Default.List
            )

            Spacer(modifier = Modifier.size(30.dp))

            Boton(
                navController = navController,
                ruta = "BuscarPokemon",
                nombreBoton = "Buscar",
                icono = Icons.Default.Search
            )

            Image(
                painter = painterResource(R.drawable.bulbasaurcharmandersquirtle),
                contentDescription = "Pokemon",
                modifier = Modifier.size(450.dp),
            )

        }

}

@Composable
fun Boton(
    navController: NavHostController,
    ruta: String,
    nombreBoton: String,
    icono: ImageVector
) {

    Button(
        onClick = {
            navController.navigate(ruta)
        },
        elevation = elevation(10.dp),
        shape = RoundedCornerShape(20.dp),
        contentPadding = PaddingValues(0.dp),
        colors = ButtonDefaults.buttonColors(
            backgroundColor = Color(0xFFb2ebf2),
            //003c8f //01579B //002071
            contentColor = Color(0xFF212121)
        ),
        modifier = Modifier
            //.border(2.dp, Color(0xFF000000), RoundedCornerShape(20.dp))
            .size(width = 150.dp, height = 50.dp),
    )
    {
        Icon(imageVector = icono, contentDescription = null)
        Spacer(modifier = Modifier.width(8.dp))
        Text(text = nombreBoton)
    }
}

