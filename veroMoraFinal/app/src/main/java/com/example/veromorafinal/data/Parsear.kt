package com.example.veromorafinal.data

fun toInt(s: String): Int {
    var valorInt = 0
    try {
       valorInt = s.toInt()
    } catch (ex: NumberFormatException) {
        println("The given string is non-numeric")
    }
    return valorInt
}