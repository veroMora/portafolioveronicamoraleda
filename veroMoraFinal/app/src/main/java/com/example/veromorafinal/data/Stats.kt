package com.example.veromorafinal.data

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.LinearProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp

@Composable
fun Stats(number: Int, stat: String) {
    Box(
        modifier = Modifier
            .height(35.dp)
            .fillMaxWidth()
            .background(color = Color.Gray, shape = RoundedCornerShape(20.dp)),
    ) {
        LinearProgressIndicator(
            progress = number.div(200.0f),
            modifier = Modifier
                .fillMaxSize()
                .clip(shape = RoundedCornerShape(20.dp)),
            backgroundColor = Color(0xFF7986cb),
            color = Color(0xFFc5cae9),
        )
        Text(
            text = stat,
            modifier = Modifier.padding(start = 10.dp, top = 5.dp),
            fontWeight = FontWeight.Bold
        )
    }
}