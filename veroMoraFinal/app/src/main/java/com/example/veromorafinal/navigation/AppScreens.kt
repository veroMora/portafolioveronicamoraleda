package com.example.veromorafinal.navigation

sealed class AppScreens(val ruta: String) {
    object MenuPokemon: AppScreens("MenuPokemon")
    object BuscarPokemon: AppScreens("BuscarPokemon")
    object Pokedex: AppScreens("Pokedex")
    object Pokemon: AppScreens("Pokemon")

}
