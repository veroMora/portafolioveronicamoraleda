package com.example.veromorafinal.data

import androidx.compose.ui.graphics.Color

fun Colores(tipoGeneral: String): Color {
    val color: Color = when (tipoGeneral) {
        "grass" -> Color(0xFF77cc55)
        "fighting" -> Color(0xFFbb5544)
        "bug" -> Color(0xFFacbd24)
        "electric" -> Color(0xFFffcc33)
        "rock" -> Color(0xFFbbaa66)
        "steel" -> Color(0xFFaaaabb)
        "ground" -> Color(0xFFddbb55)
        "ice" -> Color(0xFF66ccff)
        "normal" -> Color(0xFFaaaa99)
        "fire" -> Color(0xFFff4422)
        "water" -> Color(0xFF3399ff)
        "poison" -> Color(0xFFaa5599)
        "flying" -> Color(0xFF8899ff)
        "psychic" -> Color(0xFFff5599)
        "ghost" -> Color(0xFF6666bb)
        "dragon" -> Color(0xFF7766ee)
        "dark" -> Color(0xFF775544)
        "fairy" -> Color(0xFFee99ee)
        else -> Color(0xFF000000)
    }
    return color
}