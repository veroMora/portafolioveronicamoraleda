package com.example.veromorafinal.data

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp

@Composable
fun CajaTipos(type1: String, type2: String, colorTipo1: Color, colorTipo2: Color) {
    if (type1 == "") {

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 20.dp)
                .wrapContentWidth(align = Alignment.CenterHorizontally),
        ) {
            Box(
                contentAlignment = Alignment.Center,
                modifier = Modifier
                    .height(35.dp)
                    .width(110.dp)
                    .background(color = colorTipo2, shape = RoundedCornerShape(20.dp))
            ) {
                Text(
                    text = type2.uppercase(),
                    color = Color.White
                )
            }
        }

    } else if (type2 == "") {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 20.dp)
                .wrapContentWidth(align = Alignment.CenterHorizontally),
        ) {
            Box(
                contentAlignment = Alignment.Center,
                modifier = Modifier
                    .height(35.dp)
                    .width(110.dp)
                    .background(color = colorTipo1, shape = RoundedCornerShape(20.dp))
            ) {
                Text(
                    text = type1.uppercase(),
                    fontWeight = FontWeight.Bold,
                    color = Color.White
                )
            }
        }
    } else if (type1 != "" && type2 != "") {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 20.dp)
                .wrapContentWidth(align = Alignment.CenterHorizontally),
        ) {
            Box(
                contentAlignment = Alignment.Center,
                modifier = Modifier
                    .height(35.dp)
                    .width(110.dp)
                    .background(color = colorTipo1, shape = RoundedCornerShape(20.dp))
            ) {
                Text(
                    text = type1.uppercase(),
                    fontWeight = FontWeight.Bold,
                    color = Color.White
                )
            }

            Spacer(modifier = Modifier.width(50.dp))

            Box(
                contentAlignment = Alignment.Center,
                modifier = Modifier
                    .height(35.dp)
                    .width(110.dp)
                    .background(color = colorTipo2, shape = RoundedCornerShape(20.dp))
            ) {
                Text(
                    text = type2.uppercase(),
                    fontWeight = FontWeight.Bold,
                    color = Color.White
                )
            }
        }
    }
}