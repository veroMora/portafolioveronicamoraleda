package com.example.veromorafinal.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.example.veromorafinal.screens.*
import com.example.veromorafinal.uiViewModel.ViewModel

@Composable
fun AppNavigation() {
    val navigationController = rememberNavController()
    NavHost(navController = navigationController, startDestination = AppScreens.MenuPokemon.ruta)
    {
        composable(AppScreens.MenuPokemon.ruta) { MenuPokemon(navigationController) }
        composable(AppScreens.BuscarPokemon.ruta) { BuscarPokemon(navigationController, ViewModel()) }
        composable(AppScreens.Pokedex.ruta) { Pokedex(navigationController) }
        composable(AppScreens.Pokemon.ruta + "/{nombreP}",
            arguments = listOf(navArgument(name = "nombreP") {
                type = NavType.StringType
            })) {
            Pokemon(navigationController, it.arguments?.getString("nombreP"))
        }
    }
}