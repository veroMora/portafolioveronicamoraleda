package modelos;

public class Bebida {
    
    private String idBebida;
    private String nomBebida;
    private String urlImagenBebida;

    public Bebida(String idBebida, String nomBebida, String urlImagenBebida) {
        this.idBebida = idBebida;
        this.nomBebida = nomBebida;
        this.urlImagenBebida = urlImagenBebida;
    }

    public String getIdBebida() {
        return idBebida;
    }

    public void setIdBebida(String idBebida) {
        this.idBebida = idBebida;
    }

    public String getNomBebida() {
        return nomBebida;
    }

    public void setNomBebida(String nomBebida) {
        this.nomBebida = nomBebida;
    }

    public String getUrlImagenBebida() {
        return urlImagenBebida;
    }

    public void setUrlImagenBebida(String urlImagenBebida) {
        this.urlImagenBebida = urlImagenBebida;
    }
    
}
