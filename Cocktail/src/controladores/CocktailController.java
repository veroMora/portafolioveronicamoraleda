package controladores;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import modelos.Bebida;
import modelos.Categoria;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Mañana_pos1
 */
public class CocktailController implements Initializable {

    @FXML
    private Button btBorrar;

    @FXML
    private Button btCategoria;

    @FXML
    private Button btRandom;

    @FXML
    private ChoiceBox<Categoria> choiceCategory;

    @FXML
    private TableColumn colDrink;

    @FXML
    private TableColumn colID;

    @FXML
    private ImageView imagenBebida;

    @FXML
    private ImageView imagenLetras;

    @FXML
    private ImageView imagenPrincipal;

    @FXML
    private Label labelBebida;

    @FXML
    private Label labelNom;

    @FXML
    private ObservableList<Bebida> bebidas;

    @FXML
    private TableView<Bebida> tblBebidas;

    private ObservableList<Categoria> listCategory;

    private void jsonCategorias() throws IOException {
        String url = "https://www.thecocktaildb.com/api/json/v1/1/list.php?c=list";
        InputStream inputStream;
        inputStream = new URL(url).openStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String response = bufferedReader.readLine();

        JSONObject jsonObject = new JSONObject(response);

        JSONArray jsonArray = jsonObject.getJSONArray("drinks");

        String nomCategoria;

        for (int i = 0; i < jsonArray.length(); i++) {

            JSONObject o = jsonArray.getJSONObject(i);

            nomCategoria = (String) o.get("strCategory");
            listCategory.add(new Categoria(nomCategoria));
        }

    }

    private void asociarElementos() {
        choiceCategory.setItems(listCategory);
    }

    private void instancias() {
        listCategory = FXCollections.observableArrayList();
        bebidas = FXCollections.observableArrayList();

        colID.setCellValueFactory(new PropertyValueFactory("idBebida"));
        colDrink.setCellValueFactory(new PropertyValueFactory("nomBebida"));
    }

    @FXML
    void mostrarBebidas(ActionEvent event) {

        tblBebidas.getItems().removeAll(bebidas);

        if (choiceCategory.getSelectionModel().getSelectedIndex() >= 0) {
            String url = "https://www.thecocktaildb.com/api/json/v1/1/filter.php?c=" + choiceCategory.getSelectionModel().getSelectedItem().getNombre();

            InputStream inputStream = null;
            try {
                inputStream = new URL(url).openStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String response = null;
            try {
                response = bufferedReader.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            JSONObject jsonObject = new JSONObject(response);

            JSONArray jsonArray = jsonObject.getJSONArray("drinks");

            String idB;
            String nomB;
            String urlIB;
            for (int i = 0; i < jsonArray.length(); i++) {

                JSONObject o = jsonArray.getJSONObject(i);

                nomB = (String) o.get("strDrink");
                idB = (String) o.get("idDrink");
                urlIB = (String) o.get("strDrinkThumb");

                Bebida b = new Bebida(idB, nomB, urlIB);
                this.bebidas.add(b);
            }
            tblBebidas.setItems(bebidas);
            btBorrar.setDisable(false);
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setContentText("No hay ninguna categoria seleccionada");
            alert.showAndWait();
        }
    }

    @FXML
    void bebidaAleatoria(ActionEvent event) throws MalformedURLException, IOException {
        String url = "https://www.thecocktaildb.com/api/json/v1/1/random.php";

        InputStream inputStream;
        inputStream = new URL(url).openStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String response = bufferedReader.readLine();

        JSONObject jsonObject = new JSONObject(response);

        JSONArray jsonArray = jsonObject.getJSONArray("drinks");

        String nomB;
        String urlIB;

        JSONObject o = jsonArray.getJSONObject(0);

        nomB = (String) o.get("strDrink");
        urlIB = (String) o.get("strDrinkThumb");

        Image imagen = new Image(urlIB);
        imagenBebida.setImage(imagen);

        labelNom.setVisible(true);

        labelBebida.setText(nomB);
        labelBebida.setVisible(true);
        
        imagenPrincipal.setVisible(false);
        imagenLetras.setVisible(false);
        imagenBebida.setVisible(true);
    }

    @FXML
    void borrar(ActionEvent event) {
        if(tblBebidas != null) {
            Alert alertConfirmation = new Alert(Alert.AlertType.CONFIRMATION);
        alertConfirmation.setHeaderText(null);
        alertConfirmation.setTitle("Confirmacion");
        alertConfirmation.setContentText("¿Estas seguro/a de que quieres borrar?");
        alertConfirmation.showAndWait();

        if (alertConfirmation.getResult().equals(ButtonType.OK)) {
            tblBebidas.getItems().removeAll(bebidas);

            labelNom.setVisible(false);
            labelBebida.setVisible(false);
            imagenBebida.setVisible(false);
            imagenPrincipal.setVisible(true);
            imagenLetras.setVisible(true);

            Alert alertInformation = new Alert(Alert.AlertType.INFORMATION);
            alertInformation.setTitle("Informacion");
            alertInformation.setContentText("Bebidas eliminadas correctamente");
            alertInformation.showAndWait();
            btBorrar.setDisable(true);
        }
        }
        

    }

    @FXML
    void cambiarImagen(MouseEvent event) {

        String drinkImagen = tblBebidas.getSelectionModel().getSelectedItem().getUrlImagenBebida();

        Image imagen = new Image(drinkImagen);
        imagenBebida.setImage(imagen);

        imagenPrincipal.setVisible(false);
        imagenLetras.setVisible(false);
        imagenBebida.setVisible(true);

        String nombreBebida = tblBebidas.getSelectionModel().getSelectedItem().getNomBebida();
        labelBebida.setText(nombreBebida);

        labelNom.setVisible(true);
        labelBebida.setVisible(true);
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        instancias();
        try {
            jsonCategorias();
        } catch (IOException ex) {
            Logger.getLogger(CocktailController.class.getName()).log(Level.SEVERE, null, ex);
        }
        asociarElementos();
        labelNom.setVisible(false);
        btBorrar.setDisable(true);
    }

}
