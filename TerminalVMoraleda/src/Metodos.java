import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Stream;

public class Metodos {

	String rutaActual = System.getProperty("user.dir");

	public void help() {
		System.out.println("* * *MENU DE LOS COMANDOS* * *");
		System.out.println("\n" + "1. help: lista todos comandos.");
		System.out.println("2. cd: la ruta del directorio actual.");
		System.out.println("3. mkdir: crea un directorio de la ruta actual.");
		System.out.println("4. info <nombre>: Muestra la informacion del elemento deseado.");
		System.out.println("5. cat <nombreFichero>: Muestra el contenido de un fichero.");
		System.out.println("6. top <numeroLineas><NombreFichero>: Muestra las lineas especificadas de un fichero.");
		System.out
				.println("7. mkfile <nombreFichero> <texto>: Crea un fichero con ese nombre y el contenido de texto.");
		System.out.println("8. write <nombreFichero> <texto>: Anyade 'texto' al final del fichero especificado.");
		System.out.println("9. dir: Lista los archivos o directorios de la ruta actual.");
		System.out.println(
				"10. readpoint <nombreFichero1> <posicion>: Lee un archivo desde una determinada posicion del puntero.");
		System.out.println(
				"11. delete <nombre>: Borra el fichero, si es un directorio borra todo su contenido y a si mismo.");
		System.out.println("12. close: Cierra el programa.");
		System.out.println("13. clear: Vacia la lista.");
	}

	public void cd(String[] array) {
		if (array.length <= 2) {
			if (array.length == 2) {
				Path path = Path.of(array[1]);
				if (array[1].equals("..")) {
					try {
						File f = new File(rutaActual);
						File p = f.getParentFile();
						rutaActual = p.getPath();
						System.out.println(p.getPath());
					} catch (Exception e) {
						System.err.println(e.getMessage());
					}

				} else if (path.isAbsolute()) {
					rutaActual = path.toString();
					System.out.println(rutaActual);
				} else {
					try {
						File f = new File(rutaActual + "\\" + array[1]);
						if (f.exists()) {
							rutaActual += "\\" + array[1];
							System.out.println(rutaActual);
						} else {
							File p = f.getParentFile();
							rutaActual = p.getPath();
							System.out.println("El sistema no puede encontrar la ruta especificada");
						}
					} catch (SecurityException e) {
						System.err.println(e.getMessage());
					}
				}

			} else {
				System.out.println(rutaActual);
			}
		} else
			System.out.println("Introduce bien el comando");

	}

	public void mkdir(String[] array) {
		if (array.length == 2) {
			File dir = new File(rutaActual + "\\" + array[1]);

			if (!dir.mkdir()) {
				System.out.println("Ya existe el subdirectorio o el archivo " + array[1]);
			} else {
				System.out.println("Directorio creado");
			}
		} else
			System.out.println("Introduce bien el comando");

	}

	public void info(String[] array) {

		if (array.length == 2) {
			Path path = Path.of(rutaActual + "\\" + array[1]);
			File elemento = new File(rutaActual + "\\" + array[1]);

			if (elemento.isDirectory()) {
				String[] listF;
				listF = elemento.list();
				System.out.println("Numero de elementos: " + listF.length);

				espaciosElemento(elemento);

			} else if (elemento.isFile()) {
				espaciosElemento(elemento);
				System.out.println("Sistema de archivos: " + path.getFileSystem());
			} else {
				System.out.println("No existe el directorio o el archivo " + array[1]);
			}
		} else
			System.out.println("Introduce bien el comando");

	}

	public void cat(String[] array) {
		if (array.length == 2) {
			File elemento = new File(rutaActual + "\\" + array[1]);

			if (elemento.isFile()) {

				FileReader fileReader = null;

				try {
					fileReader = new FileReader(elemento);
				} catch (FileNotFoundException e) {
					System.out.println("Archivo no encontrado");
					e.printStackTrace();
				}

				int read;

				try {
					while ((read = fileReader.read()) != -1) {
						System.out.print((char) read);
					}
				} catch (IOException e) {
					System.out.println("Error al leer desde el FileReader");
					e.printStackTrace();
				}

				try {
					fileReader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				System.out.println("Es un directorio o no existe el archivo " + array[1]);
			}

		} else
			System.out.println("Introduce bien el comando");
	}

	public void top(String[] array) {
		if (array.length == 3) {
			Path path = Path.of(rutaActual + "\\" + array[2]);
			File elemento = new File(rutaActual + "\\" + array[2]);
			if (elemento.isFile()) {
				try {
					List<String> textos = Files.readAllLines(path);
					int numLineas = Integer.parseInt(array[1]);
					int cont = 0;

					for (String texto : textos) {
						if (cont < numLineas && texto != null) {
							System.out.println(texto);
						} else
							break;
						cont++;
					}
				} catch (IOException e) {

					e.printStackTrace();
				}
				System.out.println("Texto leido correctamente");
			} else
				System.out.println("Es un directorio o no existe el archivo " + array[2]);

		} else
			System.out.println("Introduce bien el comando");
	}

	public void mkfile(String[] array) {
		if (array.length >= 3) {
			File elemento = new File(rutaActual + "\\" + array[1]);

			if (elemento.exists()) {
				System.out.println("Ya existe el directorio o el archivo " + array[1]);
			} else {
				try {
					elemento.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
				}
				FileWriter fileWriter = null;

				try {
					fileWriter = new FileWriter(elemento);
				} catch (IOException e1) {
					e1.printStackTrace();
				}

				try {
					for (int i = 2; i < array.length; i++) {
						fileWriter.write(array[i] + " ");
					}
				} catch (IOException e) {
					e.printStackTrace();
				}

				try {
					fileWriter.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				System.out.println("Texto creado correctamente");
			}
		} else
			System.out.println("Introduce bien el comando");
	}

	public void write(String[] array) {
		if (array.length >= 3) {
			File elemento = new File(rutaActual + "\\" + array[1]);
			if (elemento.isFile()) {

				FileWriter fileWriter = null;

				try {
					fileWriter = new FileWriter(elemento, true);
				} catch (IOException e1) {
					e1.printStackTrace();
				}

				try {
					for (int i = 2; i < array.length; i++) {
						fileWriter.write(" " + array[i]);
					}
				} catch (IOException e) {
					e.printStackTrace();
				}

				try {
					fileWriter.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				System.out.println("Texto añadido correctamente");
			} else
				System.out.println("Es un directorio o no existe el archivo " + array[1]);

		} else
			System.out.println("Introduce bien el comando");

	}

	public void dir() {
		File dir = new File(rutaActual);

		if (dir.isDirectory()) {
			String[] listF;
			listF = dir.list();
			if(listF.length == 0) {
				System.out.println("El directorio no contiene ningun subdirectorio o archivo");
			} else {
				for (String elemento : listF) {
					System.out.println(elemento);
				}
			}
			
		} else
			System.out.println("Es un archivo");
	}

	public void readpoint(String[] array) {
		if (array.length == 3) {
			File elemento = new File(rutaActual + "\\" + array[1]);

			if (elemento.isFile()) {

				RandomAccessFile randomAccessFile = null;

				try {
					randomAccessFile = new RandomAccessFile(elemento, "r");
				} catch (FileNotFoundException e) {
					System.out.println("No se ha encontrado el fichero: " + elemento.getName());
					e.printStackTrace();
				}

				int pos = Integer.parseInt(array[2]);

				try {
					randomAccessFile.seek(pos);
				} catch (IOException e) {
					e.printStackTrace();
				}

				String leido;

				try {
					while ((leido = randomAccessFile.readLine()) != null) {
						System.out.println(leido);
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
				try {
					randomAccessFile.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				System.out.println("Texto leido correctamente desde la posicion " + array[2]);
			} else
				System.out.println("Es un directorio o no existe el archivo " + array[1]);
		} else
			System.out.println("Introduce bien el comando");
	}

	public void delete(String[] array) {

		if (array.length == 2) {
			Path path = Path.of(rutaActual + "\\" + array[1]);
			File elemento = new File(rutaActual + "\\" + array[1]);

			if (elemento.isDirectory()) {
				Stream<Path> stream = null;
				try {
					stream = Files.list(path);
				} catch (IOException e) {

					e.printStackTrace();
				}

				if (stream.count() == 0) {
					if (elemento.delete()) {
						System.out.println("Directorio borrado correctamente");
					} else {
						System.out.println("No se ha podido borrar");
					}
				} else
					System.out.println("El directorio no esta vacio! (no se puede borrar)");

			} else if (elemento.isFile()) {

				if (elemento.delete()) {
					System.out.println("Fichero borrado correctamente");
				} else
					System.out.println("No se ha podido borrar");

			} else {
				System.out.println("No existe el directorio o el archivo " + array[1]);
			}
		} else
			System.out.println("Introduce bien el comando");
	}

	public void close() {
		System.out.println("* * *Programa Cerrado* * *");
		System.exit(0);
	}

	public void clear() {

		System.out.print("\033[H\033[2J");
		System.out.flush();
	}

	private void espaciosElemento(File element) {
		System.out.println("Espacio total: " + element.getTotalSpace() + " bytes");
		System.out.println("Espacio usado: " + element.getUsableSpace() + " bytes");
		System.out.println("Espacio libre: " + (element.getTotalSpace() - element.getUsableSpace()) + " bytes");
	}

}
