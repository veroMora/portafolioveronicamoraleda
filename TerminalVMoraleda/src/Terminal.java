import java.util.Scanner;

public class Terminal {
	public static Scanner t;
	public static Metodos m;

	public static void main(String[] args) {

		t = new Scanner(System.in);

		System.out.println("* * *Iniciando CMD* * *\n");
		System.out.println(System.getProperty("user.dir"));

		m = new Metodos();

		String comando;
		String[] array;

		do {
			comando = t.nextLine();
			array = comando.split(" ");
			elegirComando(array);

		} while (array[0] != "close");

	}

	private static void elegirComando(String[] array) {

		switch (array[0]) {
		case "help":
			if(array.length == 1) {
				m.help();
			} else System.out.println("Introduce bien el comando");
			break;
		case "cd":
			m.cd(array);
			break;
		case "mkdir":
			m.mkdir(array);
			break;
		case "info":
			m.info(array);
			break;
		case "cat":
			m.cat(array);
			break;
		case "top":
			m.top(array);
			break;
		case "mkfile":
			m.mkfile(array);
			break;
		case "write":
			m.write(array);
			break;
		case "dir":
			if(array.length == 1) {
				m.dir();
			} else System.out.println("Introduce bien el comando");
			break;
		case "readpoint":
			m.readpoint(array);
			break;
		case "delete":
			m.delete(array);
			break;
		case "close":
			if(array.length == 1) {
				m.close();
			} else System.out.println("Introduce bien el comando");
			break;
		case "clear":
			//m.clear();
			break;
		default:
			System.out.println("El comando no existe");
		}

	}

}
