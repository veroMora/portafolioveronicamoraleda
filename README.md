 **PORTAFOLIO DE VERÓNICA MORALEDA** 

Los lenguajes de programación que se han usado en estos proyectos son: JAVA, JAVASCRIPT, KOTLIN, CSS y HTML.

**Abadia**

Es un proyecto de hilos en el que hay 5 monjes en una mesa. Cada uno de los monjes tendrá que esperar a comer hasta que el que está a su derecha y a su izquierda no estén comiendo.

**Agenda**

Es un trabajo realizado con hilos y sockets (local). La agenda es el servidor que guarda los datos de los clientes que se conectan o muestra los ya registrados.

**cafeteriaFX:**

Es un proyecto que simula una cafetería. Te permite escoger café, recargar el saldo, filtrar los pedidos, que se mostrarán en una tabla y, además, permite seleccionar un pedido (de la tabla) y borrarlo.

**CalculadoraConexionTCP**

Conexión local entre el servidor que es una calculadora y cliente. El cliente solicita unas operaciones que el servidor tiene que resolver y devolver.

**Casino**

Hay 4 jugadores con el mismo presupuesto para jugar a la ruleta. La ruleta es un hilo que se ejecuta por cada partida.

**Cocktail:**

La carpeta de Cocktail contiene un proyecto que juega con una API de bebidas. Con este proyecto, se puede visualizar la categoría de las bebidas, que se muestran en una tabla, y las imágenes de las mismas. A su vez, tiene un par de botones, uno de ellos es "BORRAR" y te permite vaciar la tabla.

**ConexionBBDDTiendaInformatica**

Proyecto hecho con el conector de base de datos JDBC (en Eclipse). La base de datos está alojada en MySQL. Desde el proyecto se puede insertar, consultar, actualizar y eliminar datos de la BBDD.

**ORM_StreetFighter**

Es un proyecto que hace uso del mapeo objeto-relacional de Hibernate (Java). Con ello se consigue una orientación a objetos en la base de datos relacional que tenemos en MySQL. En este trabajo creamos la BBDD, mapeamos las tablas, insertamos las columnas con sus correspondientes datos entre otras acciones.

**Pokemon_MongoDB**

Es un trabajo que establece conexión a una base de datos de MongoDB (no relacional). Se pueden modificar las columnas de los documentos (pokemon), insertar, eliminar y consultar.

**PracticaXMLVMoraleda**

Práctica que permite insertar datos dentro de un documento XML, respetando la jerarquía, para luego poder leerlos y listarlos con la consola. También se pueden eliminar datos.

**StreetFighter:**

Es un proyecto basado en el juego de StreetFighter. Tiene múltiples funcionalidades y permite navegar por diferentes ventanas.

**TerminalVMoraleda:**

Es un proyecto que recrea algunas funciones de los comandos del CMD de Windows. Otros comandos son inventados al igual que sus funciones.

**tfg**

Página web hecha con React y CSS como trabajo final de grado. Dentro del proyecto podemos destacar el uso de styled components, props, hooks, axios o react router entre otras librerías. Además, incluye una aplicación de Node.js que está conectada a una base de datos de MongoDB.

**trabajoHTMLCSSMoraleda:**

En esta carpeta se encuentran imágenes y archivos, que se han creado con Brackets. El conjunto de archivos junto con las imágenes conforman una página Web orientada a Rosalía.

**veroMoraFinal**

App móvil desarrollada en Android Studio con Kotlin, que tiene conexión con FireBase, arquitectura MVVM y navegación entre pantallas.
