package Abadia;

public class Monje extends Thread{
	
	private boolean ocupado;
	private Mesa mesa;
	private int idMonje;
	
	public Monje(Mesa mesa, int idMonje, boolean ocupado) {
		this.mesa = mesa;
		this.idMonje = idMonje;
		this.ocupado = ocupado;
	}
	
	public int getIdMonje() {
		return idMonje;
	}
	public void setIdMonje(int idMonje) {
		this.idMonje = idMonje;
	}
	
	public boolean isOcupado() {
		return ocupado;
	}
	public void setOcupado(boolean ocupado) {
		this.ocupado = ocupado;
	}
	
	private void rezar() throws InterruptedException {
		System.out.println("Monje " + idMonje + " esta rezando");
		Thread.sleep(2000);
	} 
	
	private void comer() throws InterruptedException {
		System.out.println("Monje " + idMonje + " empieza a comer");
		Thread.sleep(8000);
		System.out.println("Monje " + idMonje + " ha finalizado de comer");
	}
	@Override
	public void run() {
		
		try {
			rezar();
			mesa.intentaComer(this, idMonje);
			comer();
			mesa.dejaComer(idMonje);
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}
	
	
	/*run() {
	 * rezar();
	 * intentaComer();
	 * comer();
	 * dejaComer(); }
	 */
}
