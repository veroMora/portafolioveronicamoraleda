package Abadia;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		
		ArrayList<Monje> monje = new ArrayList<Monje>();
	
		Mesa m = new Mesa(5, monje);
		
		for (int i = 0; i < 5; i++) {
			
			monje.add(new Monje(m,i,false));
			monje.get(i).start();
		}
		
		
		/*
		Monje m1 = new Monje(m,0,false);
		Monje m2 = new Monje(m,1,false);
		Monje m3 = new Monje(m,2,false);
		Monje m4 = new Monje(m,3,false);
		Monje m5 = new Monje(m,4,false);
		*/
		
		
		/*
		monje.add(m1);
		monje.add(m2);
		monje.add(m3);
		monje.add(m4);
		monje.add(m5);
		*/
		
		
		/*
		monje.get(0).start();
		monje.get(1).start();
		monje.get(2).start();
		monje.get(3).start();
		monje.get(4).start();
		*/
		

	}

}
