package Abadia;

import java.util.ArrayList;

public class Mesa {
	
	ArrayList<Monje> monje;
	int numMonjes;
	
	public Mesa(int numMonjes, ArrayList<Monje> monje) {
		this.numMonjes = numMonjes;
		this.monje = monje;
	}
	
	private int tenedorIzquierda(int idMonje) {
		int izquierda;
		if (idMonje == 0) {
			izquierda = monje.size() - 1;
		} else izquierda = idMonje - 1;
		return izquierda;
	}
	
	private int tenedorDerecha(int idMonje) {
		int derecha;
		if (monje.size() - 1 == idMonje) {
			derecha = 0;
		} else derecha = idMonje + 1;
		return derecha;
	}
	
	public synchronized void intentaComer(Monje m, int idMonje) throws InterruptedException {
		
		while (this.monje.get(tenedorIzquierda(idMonje)).isOcupado() || this.monje.get(tenedorDerecha(idMonje)).isOcupado()) {
			System.out.println("Monje " + m.getIdMonje() + " no puede comer. Asi que espera");
			wait();
		}
		monje.get(idMonje).setOcupado(true);
		
	}
	
	public synchronized void dejaComer(int idMonje) {
		this.monje.get(idMonje).setOcupado(false);
		notifyAll();
	}
	
	
	
	
	
	
	
	
	
	
	
}
